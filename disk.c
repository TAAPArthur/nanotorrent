#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include "bencode.h"
#include "nanotorrent.h"
#include "nanotorrent_private.h"
#include "util.h"

static long inline getFileSize(int fd) {
    struct stat fileStat;
    if(fstat(fd, &fileStat) == -1)
        FATAL_ERROR("Failed to get file size");
    return fileStat.st_size;
}

static inline int mkdirp(int dirfd, char* path) {
    int ret = 0;
    char* p = path;
    while(ret == 0) {
        p = strchr(p + 1, '/');
        if(!p)
            break;
		*p = '\0';
        if(mkdirat(dirfd, path, 0777) == -1 && errno != EEXIST) {
            ERROR("Failed to create dir");
            ret = -1;
        }
        *p = '/';
    }
    return ret;
}
static int doIoHelper(int fd, void* buffer, int buffer_len, int do_read) {
    if(do_read) {
        return read(fd, buffer, buffer_len);
    }
    return write(fd, buffer, buffer_len);
}

int doIo(int fd, void* buffer, int buffer_len, int doRead) {
    assert(buffer);
    int ret;
    int size = 0;
    while((ret = doIoHelper(fd, buffer + size, buffer_len - size, doRead))) {
        if(ret == -1 ) {
            if (errno == EINTR)
                continue;
            else {
                assert(errno != EFAULT);
                ERROR("Error reading file");
                if(size)
                    break;
                return -1;
            }
        }
        size += ret;
        if(size == buffer_len)
            break;
    }
    return size;
}

int safeRead(int fd, void* buffer, int bufferLen) {
    return doIo(fd, buffer, bufferLen, 1);
}

int safeWrite(int fd, void* buffer, int bufferLen) {
    return doIo(fd, buffer, bufferLen, 0);
}

static int readOrWriteToDisk(int dirfd, const TorrentMetadata* metadata, char*buffer, int pieceIndex, int pieceOffset, int len, int doRead, int* fds, int * pos) {
    unsigned long transferred = 0;
    int firstFile = 1;
    int pieceAlreadyStarted = 0;

    assert(len);
    int i = pos ? *pos : 0;
    for(; i < metadata->fileCount; i++) {
        if (fds && i && fds[i - 1] != -1) {
            close(fds[i - 1]);
            fds[i - 1] = -1;
        }
        int startingPieceIndex;
        int endingPieceIndex;
        unsigned long runningLen;
        unsigned long fileLength;
        char* path = getPieceRange(metadata, i, NULL, &startingPieceIndex, &endingPieceIndex, &fileLength, &runningLen);
        if(endingPieceIndex <= pieceIndex) {
            continue;
        }
        if(startingPieceIndex > pieceIndex) {
            break;
        }

        unsigned long fileOffsetInPiece = runningLen % metadata->pieceLength;

        unsigned long seek = 0;
        if(firstFile) {
            if(pieceAlreadyStarted) {
                assert(transferred == 0);
                // already in piece so seek to the start of the block
                seek = pieceOffset - fileOffsetInPiece;
            } else {
                // the start of the piece is in this file
                // seek to start of the target piece
                seek  = metadata->pieceLength - fileOffsetInPiece;
                // seek to start of target piece
                seek += metadata->pieceLength * (pieceIndex - startingPieceIndex -1);
                // seek to offset
                seek += pieceOffset;
            }
            pieceAlreadyStarted = 1;
            if(seek >= fileLength )
                continue;
            assert(seek >= 0);
        }
        int fd;
        if(fds && fds[i] != -1)
            fd = fds[i];
        else {
            fd = openat(dirfd, path, doRead ? O_RDONLY : O_WRONLY | O_CREAT, 0666);
            int err = errno;
            if(fd == -1) {
                if(err == ENOENT) {
                    if (!doRead && mkdirp(dirfd, path) != -1)
                        fd = openat(dirfd, path, doRead ? O_RDONLY : O_WRONLY | O_CREAT, 0666);
                } else {
                    ERROR("failed to open file");
                    INFO("failed to open file %s", path);
                }
            }
            if (fds)
                fds[i] = fd;
        }
        if(fd == -1) {
            break;
        }
        if(!doRead) {
            ftruncate(fd, fileLength);
        }
        if(firstFile) {
            assert(seek < fileLength);
            if(lseek(fd, seek, SEEK_SET) == -1) {
                ERROR("lseek");
                close(fd);
                return -1;
            }
            firstFile = 0;
        }
        int ret;
        int amountTo_transfer = MIN(len - transferred, fileLength - seek);
        assert(amountTo_transfer > 0);
        ret = doIo(fd, buffer + transferred, amountTo_transfer, doRead);
        if(!fds)
            close(fd);
        if(ret == -1) {
            break;
        }
        transferred += ret;
        if(transferred == len)
            break;
    }
    if (pos) {
        *pos = i;
    }
    return transferred == len;
}

int readFromDisk(int dirfd, const TorrentMetadata* metadata, char*buffer, int pieceIndex, int pieceOffset, int len) {
    return readOrWriteToDisk(dirfd, metadata, buffer, pieceIndex, pieceOffset, len, 1, NULL, NULL);
}

int writeToDisk(int dirfd, const TorrentMetadata* metadata, char*buffer, int pieceIndex, int pieceOffset, int len) {
    return readOrWriteToDisk(dirfd, metadata, buffer, pieceIndex, pieceOffset, len, 0, NULL, NULL);
}

static int getOnDiskBitMapLen(BitMap* bitmap) {
    // bitmap, starting piece, ending piece and version (0 is uninitialized)
    return getBytesNeeded(bitmap) + (HASH_SIZE + sizeof(int)*3);
}

static int mmapBitmapFromDisk(BitMap* bitmap, int dirfd, const char* filename) {
    int len = getOnDiskBitMapLen(bitmap);
    int fd = -1;
    int bitmapFileExisted = 0;
    if (filename) {
        fd = openat(dirfd, filename, O_RDWR);
        if (fd == -1) {
            fd = openat(dirfd, filename, O_RDWR | O_CREAT, 0666);
            if(fd == -1) {
                ERROR("Failed to open bitmap file");
            }
        } else {
            VERBOSE("Found bitmap file %s\n", filename);
            bitmapFileExisted = 1;
        }
        if(fd != -1) {
            ftruncate(fd, len);
        }
    }
    int mapFlags = fd == -1 ? MAP_PRIVATE | MAP_ANONYMOUS : MAP_SHARED;
    void* data = mmap(NULL, len, PROT_READ | PROT_WRITE, mapFlags, fd, 0);
    if(data == MAP_FAILED) {
        FATAL_ERROR("Failed to mmap memory");
        return -1;
    }
    bitmap->data = data;
    if (fd != -1)
        close(fd);
    return bitmapFileExisted;
}

int loadBitmapFromDisk(int dirfd, const TorrentMetadata* torrentMetadata, BitMap* bitmap) {
    assert(dirfd != -1);
    VERBOSE("Loading bitmap from disk\n");
    void* buffer = malloc(torrentMetadata->pieceLength);

    int fds[torrentMetadata->fileCount];
    memset(fds, -1, sizeof(fds));
    int pos = 0;
    for(int pieceIndex = 0; pieceIndex < torrentMetadata->numPieces; pieceIndex++) {
        int len = getPieceLen(torrentMetadata, pieceIndex);
        if(!readOrWriteToDisk(dirfd, torrentMetadata, buffer, pieceIndex, 0, len, 1, fds, &pos))
            continue;
        if(!setBitIfPieceIsValid(torrentMetadata, pieceIndex, buffer, len, bitmap))
            VERBOSE("Piece index %d is invalid\n", pieceIndex);
    }
    for(int i = 0; i < torrentMetadata->fileCount; i++) {
        if(fds[i] != -1)
            close(fds[i]);
    }
    free(buffer);
    return 0;
}

void syncChanges(TorrentInfo* torrentInfo, int block) {
    int flag = block ? MS_SYNC : MS_ASYNC;
    msync(torrentInfo->data, torrentInfo->cacheSlots * torrentInfo->metadata->pieceLength, flag);
    msync(torrentInfo->cacheBitmap.data, getOnDiskBitMapLen(&torrentInfo->cacheBitmap), flag);
}

#define BITMAP_VERSION  1
static int resumeProgress(TorrentInfo* torrentInfo, int bitmapMightBeValid) {
    void* savedHash = torrentInfo->cacheBitmap.data + getBytesNeeded(&torrentInfo->cacheBitmap);
    int* range = (int*)(savedHash + HASH_SIZE);
    unsigned long end = MIN(range[1], torrentInfo->endingPieceIndex);
    void* middleOfCache = torrentInfo->data + torrentInfo->metadata->pieceLength * abs(torrentInfo->startingPieceIndex - range[0]);
    if(!bitmapMightBeValid) {
        memcpy(savedHash, torrentInfo->metadata->infoHash, HASH_SIZE);
        goto persist_bitmap;
    }
    VERBOSE("Cache file range [%d, %d)\n", range[0], range[1]);
    int clear = 0;

    int valid = range[2];
    range[2] = 0;
    msync(torrentInfo->cacheBitmap.data, getOnDiskBitMapLen(&torrentInfo->cacheBitmap), MS_SYNC);
    if(valid != BITMAP_VERSION) {
        VERBOSE("bitmap is is invalid or corrupted or belongs to an older version %d\n", valid);
    } else if(memcmp(savedHash, torrentInfo->metadata->infoHash, HASH_SIZE)) {
        VERBOSE("bitmap is for a different torrent file\n");
    } else if (range[0] == torrentInfo->startingPieceIndex) {
        VERBOSE("Previous cache file had same starting position\n");
        clear = 1;
    } else if (range[0] < torrentInfo->startingPieceIndex && torrentInfo->startingPieceIndex < range[1]) {
        int copyLen = torrentInfo->metadata->pieceLength * ( end - 1 - torrentInfo->startingPieceIndex) + getPieceLen(torrentInfo->metadata, end - 1);
        VERBOSE("Existing cache file contained the start of the current range; copy len %d\n", copyLen);
        clear = 1;
        memmove(torrentInfo->data, middleOfCache, copyLen);
    } else if (torrentInfo->startingPieceIndex < range[0] && range[0] < torrentInfo->endingPieceIndex) {
        int copyLen = torrentInfo->metadata->pieceLength * ( end - 1 - range[0]) + getPieceLen(torrentInfo->metadata, end - 1);
        VERBOSE("Existing cache file started in the middle of the current range %d\n", copyLen/ torrentInfo->metadata->pieceLength);
        clear = 1;
        memmove(middleOfCache, torrentInfo->data, copyLen );
    } else {
        VERBOSE("Previous cache file is disjoint from current range\n");
    }
    if(clear) {
        clearRange(&torrentInfo->cacheBitmap, 0, torrentInfo->startingPieceIndex);
        clearRange(&torrentInfo->cacheBitmap, torrentInfo->endingPieceIndex, torrentInfo->cacheBitmap.numBits);
    } else {
        resetBitMap(&torrentInfo->cacheBitmap);
        bitmapMightBeValid = 0;
        memcpy(savedHash, torrentInfo->metadata->infoHash, HASH_SIZE);
    }
persist_bitmap:
    range[0] = torrentInfo->startingPieceIndex;
    range[1] = torrentInfo->endingPieceIndex;
    range[2] = BITMAP_VERSION;
    syncChanges(torrentInfo, 1);
    return bitmapMightBeValid;
}

static void allocateCacheForTorrent(TorrentInfo* torrentInfo, const TorrentSettings torrentSettings, int bitmapFileExisted) {
    int prot = PROT_READ | PROT_WRITE;
    int flags = MAP_PRIVATE | MAP_ANONYMOUS;
    int fd = -1;

    assert(torrentInfo->metadata->pieceLength);
    assert(torrentInfo->metadata->totalLength);

    torrentInfo->cacheSlots = torrentSettings.cacheFilename ?
        torrentInfo->endingPieceIndex - torrentInfo->startingPieceIndex :
        (MIN(torrentInfo->metadata->totalLength, torrentSettings.cacheSize ? torrentSettings.cacheSize : DEFAULT_CACHE_SIZE) + torrentInfo->metadata->pieceLength - 1) / torrentInfo->metadata->pieceLength;
    assert(torrentInfo->cacheSlots);

    unsigned long realCacheSize = torrentInfo->cacheSlots * torrentInfo->metadata->pieceLength;
    unsigned long len = realCacheSize;
    int cacheFileDidNotExist = !torrentSettings.cacheFilename;
    if (torrentSettings.cacheFilename) {
        TRACE("Calculating bitmap from cache file\n");
        fd = open(torrentSettings.cacheFilename, O_RDWR);
        if(fd == -1) {
            if(errno == ENOENT) {
                if((fd = open(torrentSettings.cacheFilename, O_RDWR | O_CREAT, 0666)) != -1) {
                    VERBOSE("Creating cache file %s\n", torrentSettings.cacheFilename);
                    resetBitMap(&torrentInfo->cacheBitmap);
                    cacheFileDidNotExist = 1;
                }
            }
        }
        if(fd == -1) {
            fd = open(torrentSettings.cacheFilename, O_RDONLY);
            if(fd == -1) {
                FATAL_ERROR("Cannot open cache file");
            }
            prot = PROT_READ;
            len = getFileSize(fd);
        }
        if(len > getFileSize(fd)) {
            if(getFileSize(fd) == 0)
                cacheFileDidNotExist = 1;
            VERBOSE("Enlarging cache file\n");
            ftruncate(fd, len);
        } else {
            len = getFileSize(fd);
        }

        flags = MAP_SHARED;

    }
    if(cacheFileDidNotExist || !bitmapFileExisted) {
        VERBOSE("Resetting bitmap;\n");
        resetBitMap(&torrentInfo->cacheBitmap);
        bitmapFileExisted = 0;
    }

    if((torrentInfo->data = mmap(NULL, len, prot, flags, fd, 0)) == MAP_FAILED) {
        FATAL_ERROR("Failed to mmap file");
    }
    int bitmapMightBeValid = bitmapFileExisted;
    if(fd != -1) {
        if(!torrentSettings.doNotCreateBitmapFiles && prot != PROT_READ) {
            bitmapMightBeValid = resumeProgress(torrentInfo, bitmapFileExisted);
        }

        if(len > realCacheSize) {
            VERBOSE("Shrinking cache file\n");
            ftruncate(fd, realCacheSize);
            munmap(torrentInfo->data, len);
            if((torrentInfo->data = mmap(NULL, realCacheSize, prot, flags, fd, 0)) == MAP_FAILED) {
                FATAL_ERROR("Failed to mmap file");
            }
        }
        close(fd);
    }

    VERBOSE("Allocating %ld bytes for cache metadata \n", sizeof(PieceMetadata) * torrentInfo->cacheSlots);
    torrentInfo->pieceMetadata = calloc(sizeof(PieceMetadata), torrentInfo->cacheSlots);
    char* data = torrentInfo->data;
    if (!cacheFileDidNotExist) {

        int pieceIndex = findFirstSetBit(&torrentInfo->cacheBitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex);
        if(pieceIndex != -1 && !isPieceValid(torrentInfo->metadata, pieceIndex, data, getPieceLen(torrentInfo->metadata, pieceIndex))) {
            bitmapMightBeValid = 0;
            VERBOSE("Bitmap seems to be invalid; starting full initialization\n");
            resetBitMap(&torrentInfo->cacheBitmap);
        }

        for(pieceIndex = torrentInfo->startingPieceIndex; pieceIndex < torrentInfo->endingPieceIndex; pieceIndex++, data += torrentInfo->metadata->pieceLength) {
            if(bitmapMightBeValid || !bitmapFileExisted && setBitIfPieceIsValid(torrentInfo->metadata, pieceIndex, data, getPieceLen(torrentInfo->metadata, pieceIndex), &torrentInfo->cacheBitmap)) {
                getPieceMetadata(torrentInfo, pieceIndex, 1);
            }
        }
    }
    if(prot == PROT_READ && !isBitMapOne(&torrentInfo->cacheBitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex)) {
        assert(!cacheFileDidNotExist);
        FATAL_ERROR("Readonly cache file is only supported if all data has been downloaded");
    }
}

void allocateBuffersAndLoadBitmaps(TorrentInfo* torrentInfo) {
    // Allocating bitmaps
    torrentInfo->cacheBitmap.numBits = torrentInfo->metadata->numPieces;
    torrentInfo->downloadedBitmap.numBits = torrentInfo->metadata->numPieces;
    torrentInfo->requested.numBits = torrentInfo->metadata->numPieces;
    torrentInfo->ondisk.numBits = torrentInfo->metadata->numPieces;

    int bytesNeeded = getBytesNeeded(&torrentInfo->downloadedBitmap);

    if(torrentInfo->settings.dirfd != -1) {
        char buffer[sizeof(torrentInfo->metadata->encodedInfoHash) + 16] = {'.'};
        memcpy(buffer, torrentInfo->metadata->encodedInfoHash, sizeof(torrentInfo->metadata->encodedInfoHash));
        strcat(buffer, "_bitmap");
        char* bitmapFile = torrentInfo->settings.doNotCreateBitmapFiles ? NULL : buffer;
        int mightBeValid = mmapBitmapFromDisk(&torrentInfo->ondisk, torrentInfo->settings.dirfd, bitmapFile);
        if (!mightBeValid) {
            loadBitmapFromDisk(torrentInfo->settings.dirfd, torrentInfo->metadata, &torrentInfo->ondisk);
        }
    }

    char buffer[255];
    if(torrentInfo->settings.cacheFilename) {
        snprintf(buffer, sizeof(buffer), "%s.cacheBitmap", torrentInfo->settings.cacheFilename);
        VERBOSE("loading cache file %s\n", torrentInfo->settings.cacheFilename);
    }
    char* bitmapFile = torrentInfo->settings.doNotCreateBitmapFiles || !torrentInfo->settings.cacheFilename ? NULL : buffer;
    int bitmapFileExisted = mmapBitmapFromDisk(&torrentInfo->cacheBitmap, AT_FDCWD, bitmapFile);

    allocateCacheForTorrent(torrentInfo, torrentInfo->settings, bitmapFileExisted);

    torrentInfo->downloadedBitmap.data = malloc(bytesNeeded);
    if(torrentInfo->ondisk.data) {
        orBitmap(&torrentInfo->downloadedBitmap, &torrentInfo->cacheBitmap, &torrentInfo->ondisk);
    }
    else
        copyBitMap(&torrentInfo->downloadedBitmap, &torrentInfo->cacheBitmap);
    // TODO just mmap the data
    torrentInfo->requested.data = malloc(bytesNeeded);
    copyBitMap(&torrentInfo->requested, &torrentInfo->downloadedBitmap);
    assert(torrentInfo->cacheSlots);
}

void freeBuffers(TorrentInfo* torrentInfo) {

    for(int i = 0; i < torrentInfo->cacheSlots; i++)
        free(torrentInfo->pieceMetadata[i].blockBitmap.data);
    free(torrentInfo->pieceMetadata);

    munmap(torrentInfo->data, torrentInfo->cacheSlots * torrentInfo->metadata->pieceLength);
    free(torrentInfo->downloadedBitmap.data);
    if(torrentInfo->ondisk.data)
        munmap(torrentInfo->ondisk.data, getOnDiskBitMapLen(&torrentInfo->downloadedBitmap));
    if(torrentInfo->cacheBitmap.data)
        munmap(torrentInfo->cacheBitmap.data, getOnDiskBitMapLen(&torrentInfo->downloadedBitmap));
    munmap((void*)torrentInfo->metadata->data, torrentInfo->metadata->end - torrentInfo->metadata->data);
}
