#ifndef HEURISTICS_H
#define HEURISTICS_H

#include "config.h"
#include "nanotorrent.h"
#include "util.h"

static int getMaxNumberOfOutstandingBlocksForPeer(const Peer* peer) {
    return MIN(MAX_PEER_PEER_OUTSTANDING_BLOCKS, MAX(1, 2 * getPeerTransferRate(peer, DOWNLOAD) / MIN_BLOCK_SIZE));
}

static unsigned int isPeerSlow(const Peer* peer, unsigned blockSize) {
    if (!peer->firstRequestTimeMS || peer->statBucket == 0) {
        return 0;
    }
    return getPeerTransferRate(peer, DOWNLOAD) < blockSize;
}

static unsigned int isPeerReallySlow(const Peer* peer) {
    return getPeerTransferRate(peer, DOWNLOAD) < SLOW_DOWNLOAD_RATE;
}

#endif

