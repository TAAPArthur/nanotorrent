#ifndef NANOTORRENT_PRIVATE_H
#define NANOTORRENT_PRIVATE_H

#include "nanotorrent.h"

// nanoTorrent.c

#define NOT_CHOKING_PEER      (1<<0)
#define PEER_NOT_CHOKING_ME   (1<<1)

#define INTERESTED_IN_PEER    (1<<2)
#define PEER_INTERESTED_IN_ME (1<<3)

#define SENT_HANDSHAKE        (1<<4)
#define RECEIVED_HANDSHAKE    (1<<5)

#define COMPLETED_HANDSHAKE   (RECEIVED_HANDSHAKE|SENT_HANDSHAKE)

#define SEEDING_FROM_PEER     (1<<6)
#define PEER_SEEDING_TO_ME    (1<<7)

#define THROTTING_PEER        (1<<8)
#define PEER_IS_THROTTLING_ME (1<<9)

#define NUM_FLAGS 10

int initTorrentInfo(TorrentInfo* torrentInfo, const TorrentMetadata* torrentMetadata, TorrentSettings torrentSettings);
/**
 * Process events until we've gone pollTimeout ms without an event (-1 to disable timeout) or a flag is set
 */
int handleEvents(TorrentInfo* torrentInfo, int pollTimeout);
/**
 * Seeds and/or reaches until a termination condition is met
 */
int eventLoop(TorrentInfo* torrentInfo);
void cleanupTorentInfo(TorrentInfo* torrentInfo);
int parseArgsAndRun(const char *argv[]);

void unclaimAll(TorrentInfo* torrentInfo, Peer*peer);

int queueMessageToSend(Peer* peer, MessageType type, const BlockInfo* blockInfo);

int validatePiece(const TorrentMetadata* torrentMetadata, int pieceIndex, char hash[HASH_SIZE]);
PieceMetadata* getPieceMetadata(const TorrentInfo* torrentInfo, int index, int new);
int setBitIfPieceIsValid(const TorrentMetadata* torrentMetadata, int pieceIndex, const void* data, int len, BitMap* bitmap);

void setupSignals();

int getNumOutstandingBlocks(const Peer* peer);


static inline int isSendQueueFull(const Peer* peer) {
    return peer->sendIndexStart % SEND_QUEUE_LEN == ((peer->sendIndexEnd + 1) % SEND_QUEUE_LEN);
}

static inline int isSendQueueEmpty(const Peer* peer) {
    return peer->sendIndexStart == peer->sendIndexEnd;
}

static inline int canReceiveBlocks(const Peer* peer) {
    return ((peer->sendIndexEnd - peer->sendIndexStart ) % SEND_QUEUE_LEN + SEND_QUEUE_LEN) % SEND_QUEUE_LEN < SEND_QUEUE_LEN * 7 / 8;
}

#endif
