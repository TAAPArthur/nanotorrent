#ifndef BENCODE_H
#define BENCODE_H
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static inline int bencodeWriteStringN(int fd, const char*str, int N) {
    return dprintf(fd, "%d:%.*s", N, N, str);
}

static inline int bencodeWriteString(int fd, const char*str) {
    return bencodeWriteStringN(fd, str, strlen(str));
}

static inline int bencodeWriteInt(int fd, long value) {
    return dprintf(fd, "i%de", value);
}

static inline int writeDictStart(int fd) { return write(fd, "d", 1); }
static inline int writeListStart(int fd) { return write(fd, "l", 1); }
static inline int writeDictEnd(int fd) { return write(fd, "e", 1); }
static inline int writeListEnd(int fd) { return write(fd, "e", 1); }

static inline char* bencodeWriteStringMemN(char* dest, const char*str, int N) {
    return dest + sprintf(dest, "%d:%.*s", N, N, str);
}

static inline char* bencodeWriteStringMem(char* dest, const char*str) {
    return bencodeWriteStringMemN(dest, str, strlen(str));
}

static inline char* bencodeWriteIntMem(char* dest, long value) {
    return dest + sprintf(dest, "i%de", value);
}

static inline char* writeDictStartMem(char* dest) { *dest = 'd'; return dest + 1; }
static inline char* writeListStartMem(char* dest) { *dest = 'l'; return dest + 1; }
static inline char* writeDictEndMem(char* dest) { *dest = 'e'; return dest + 1; }
static inline char* writeListEndMem(char* dest) { return writeDictEndMem(dest);}

typedef struct {
    const char *str;
    const char *end;
} bencodeState;

#define invalidateAndFailIfFalse(state, A) do { if(hasBencodeError(state) || !(A)) { state->str = state->end; return -1; } } while(0)

static inline int hasBencodeError(bencodeState* state) {
    return !state->str || state->str >= state->end;
}

static inline int bencodeReadString(bencodeState* state , const char** strStart, int* strLen) {
    if(hasBencodeError(state)) return -1;
    *strStart = NULL;
    *strLen = strtol(state->str, (char**)strStart, 10);
    invalidateAndFailIfFalse(state, *strLen || *strStart);
    (*strStart)++; // skip ':'
    state->str = *strStart + *strLen;
    return 0;
}

static inline int bencodeReadInt(bencodeState* state, unsigned long* value) {
    invalidateAndFailIfFalse(state, state->str[0] == 'i');
    *value = strtol(state->str + 1, (char**)&state->str, 10);
    invalidateAndFailIfFalse(state, *state->str == 'e');
    state->str++;
    return 0;
}

static inline int bencodeDictEnter(bencodeState* state) {
    invalidateAndFailIfFalse(state, state->str[0] == 'd');
    state->str++;
    return 0;
}

static inline int bencodeListEnter(bencodeState* state) {
    invalidateAndFailIfFalse(state, state->str[0] == 'l');
    state->str++;
    return 0;
}

static inline int bencodeHasNext(bencodeState* state) {
    return !hasBencodeError(state) && state->str[0] != 'e';
}

static inline void bencodeExit(bencodeState* state) {
    assert(!bencodeHasNext(state));
    state->str++;
}
static inline const char* getNextElement(const char* str, const char*end, int* failure) {
    *failure |= str >= end;
    if(*failure) {
        return NULL;
    }
    const  char* result = NULL;
    if ('0' <= str[0] && str[0] <= '9' ) {
        int len = strtol(str, (char**) &result, 10);
        *failure |= len == 0 && result == NULL;
        return result + 1 + len;
    }
    switch(str[0]) {
        case 'e':
            return str + 1;
        case 'i':
            result = strchr(str, 'e');
            *failure |= !result;
            return result + 1;
        case 'd':
        case 'l':
            result = str + 1;
            do {
                result = getNextElement(result, end, failure);
            } while(result && result[0] != 'e' && !*failure);
            return result + 1;
        default:
            *failure = 1;
            return NULL;
    }
    return NULL;
}

static inline int getBencodeLen(bencodeState* state) {
    int failure = 0;
    const char * next = getNextElement(state->str, state->end, &failure);
    assert(!failure);
    return !failure ? next - state->str : -1;
}

static inline int bencodeSkipNext(bencodeState* state) {
    int failure = 0;
    const char * next = getNextElement(state->str, state->end, &failure);
    if(!failure) {
        state->str = next;
    }
    return !failure ?  next - state->str : -1;
}

#endif /* BENCODE_H_ */

