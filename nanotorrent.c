#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>


#include "bitmap.h"
#include "config.h"
#include "hash.h"
#include "heuristics.h"
#include "nanotorrent.h"
#include "nanotorrent_private.h"
#include "util.h"

enum LogLevel LOG_LEVEL = LOG_LEVEL_INFO;

static char* messageTypeStrings[NUM_MESSAGE_TYPES] = {
    [CHOKE] = "CHOKE",
    [UNCHOKE] = "UNCHOKE",
    [INTERESTED] = "INTERESTED",
    [UNINTERESTED] = "UNINTERESTED",
    [HAVE] = "HAVE",
    [BITFIELD] = "BITFIELD",
    [REQUEST] = "REQUEST",
    [PIECE] = "PIECE",
    [CANCEL] = "CANCEL",

    [SUGGEST] = "SUGGEST",
    [HAVE_ALL] = "HAVE_ALL",
    [HAVE_NONE] = "HAVE_NONE",
    [REJECT] = "REJECT",
    [ALLOWED_FAST] = "ALLOWED_FAST",

    [EXTENSION] = "EXTENSION",
    [DONT_HAVE] = "DONT_HAVE",
    [KEEP_ALIVE] = "KEEP_ALIVE",
};

#ifndef NDEBUG
#define VERIFY_LOOP_IS_NOT_SPAMMY(now) do { \
        static int iter = 0; \
        static unsigned long startTime; \
        if(!startTime) startTime = getTimeMS(); \
        assert(now - startTime + 16 >= iter++); \
    } while(0)
#else
#define VERIFY_LOOP_IS_NOT_SPAMMY(X)
#endif

int GLOBAL_FLAGS;

enum {
    ANNOUNCE_TIMER,
    STATS_TIMER,
    CONNECTION_TIMER,
    NUM_TIMERS
};

#define SIGNAL_FLAG_SHUTDOWN             (1<<(NUM_TIMERS))
#define SIGNAL_FLAG_FINISHED_DOWNLOADING (1<<(NUM_TIMERS+1))
#define SIGNAL_FLAG_FINISHED_OUTPUTTING  (1<<(NUM_TIMERS+2))
#define SIGNAL_FLAG_DUMP_PEERS           (1<<(NUM_TIMERS+3))
timer_t timers[NUM_TIMERS];

static inline int getTimeMS() {
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    return now.tv_sec * 1000 + now.tv_nsec / 1000000;
}

void timerHandler(int sig, siginfo_t *si, void*_) {
    int mask = 0;
    switch(sig) {
        case SIGALRM:
            for(int i = 0; i < NUM_TIMERS; i++)
                if((long)timers[i] == si->si_timerid) {
                    mask = 1<<i;
                }
            break;
        case SIGHUP:
            mask = 1<<ANNOUNCE_TIMER;
            break;
        case SIGUSR1:
            mask = 1<<STATS_TIMER;
            break;
        case SIGUSR2:
            mask = SIGNAL_FLAG_DUMP_PEERS;
            break;
        case SIGTERM:
        case SIGINT:
            mask = SIGNAL_FLAG_SHUTDOWN;
    }
    assert(mask);
    GLOBAL_FLAGS |= mask;
}

int createTimers() {
    for(int i = 0; i < NUM_TIMERS; i++) {
        timer_t timerid;
        if (timer_create(CLOCK_MONOTONIC, NULL, &timerid) == -1) {
            ERROR("Failed to create timers");
            return -1;
        }
        timers[i] = timerid;
    }
    return 0;
}

int armTimers(const int* times, int len) {
    assert(len <= NUM_TIMERS);
    for(int i = 0; i < len; i++) {
        struct timespec interval = {.tv_sec = times[i] / 1000, .tv_nsec = (times[i] % 1000) * 1e6} ;
        struct itimerspec timerInfo = {.it_interval = interval, .it_value = interval};
        if(timer_settime(timers[i], 0, &timerInfo, NULL) == -1) {
            ERROR("Failed to set timers");
            return -1;
        }
    }
    return 0;
}

int isPieceValid(const TorrentMetadata* torrentMetadata, int pieceIndex, const void* data, int len) {
    assert(len);
    char hashBuffer[HASH_SIZE] = {0};
    getHash(data, len, hashBuffer);
    assert( pieceIndex < torrentMetadata->numPieces );
    return memcmp(&torrentMetadata->pieces[pieceIndex * HASH_SIZE], hashBuffer, HASH_SIZE) == 0;
}

int setBitIfPieceIsValid(const TorrentMetadata* torrentMetadata, int pieceIndex, const void* data, int len, BitMap* bitmap) {
    if(isPieceValid(torrentMetadata, pieceIndex, data, len)) {
        setBit(bitmap, pieceIndex);
        return 1;
    }
    return 0;
}

PieceMetadata* getPieceMetadata(const TorrentInfo* torrentInfo, int index, int new) {
    assert(index < torrentInfo->metadata->numPieces);
    assert(torrentInfo->startingPieceIndex <= index && index < torrentInfo->endingPieceIndex);
    unsigned int effectiveIndex = (index - torrentInfo->startingPieceIndex + torrentInfo->cacheSlots) % torrentInfo->cacheSlots;
    PieceMetadata* pieceMetadata = &torrentInfo->pieceMetadata[effectiveIndex];
    if (pieceMetadata->data && pieceMetadata->pieceIndex == index)
        return pieceMetadata;
    if (!new || pieceMetadata->data && (torrentInfo->settings.daemon && !isSet(&torrentInfo->downloadedBitmap, pieceMetadata->pieceIndex) || (!torrentInfo->settings.daemon && pieceMetadata->pieceIndex >= torrentInfo->nextPieceToOutput))) {
        if(pieceMetadata->data)
            TRACE("The slot for piece %d is already in use by piece %d; nextPieceToOutput %d\n", index, pieceMetadata->pieceIndex, torrentInfo->nextPieceToOutput);
        return NULL;
    }
    assert(!pieceMetadata->data || isSet(&torrentInfo->downloadedBitmap, pieceMetadata->pieceIndex));
    pieceMetadata->oldPieceIndex = pieceMetadata->data ? pieceMetadata->pieceIndex : -1;
    pieceMetadata->pieceIndex = index;

    if(!pieceMetadata->blockBitmap.data) {
        pieceMetadata->blockBitmap.numBits = (torrentInfo->metadata->pieceLength + torrentInfo->settings.blockSize - 1) / torrentInfo->settings.blockSize;
        pieceMetadata->blockBitmap.data = malloc(getBytesNeeded(&pieceMetadata->blockBitmap));
        resetBitMap(&pieceMetadata->blockBitmap);
    }

    pieceMetadata->data = torrentInfo->data + torrentInfo->metadata->pieceLength * effectiveIndex;
    assert(pieceMetadata->pieceIndex == index);
    return pieceMetadata;
}

int writePieceData(const TorrentInfo* torrentInfo, int outfd, int pieceIndex, unsigned int pieceOffset, unsigned int len) {
    assert(isSet(&torrentInfo->downloadedBitmap, pieceIndex));
    assert(pieceOffset + len <= getPieceLen(torrentInfo->metadata, pieceIndex));
    char* data = NULL;
    if(isSet(&torrentInfo->cacheBitmap, pieceIndex)) {
        PieceMetadata* pieceMetadata = getPieceMetadata(torrentInfo, pieceIndex, 0);
        if(pieceMetadata)
            data = pieceMetadata->data + pieceOffset;
    }
    if(!data && torrentInfo->settings.dirfd != -1 && isSet(&torrentInfo->ondisk, pieceIndex)) {
        TRACE("Loading piece from disk\n");
        static char buffer[MAX_BLOCK_SIZE];
        len = MIN(MAX_BLOCK_SIZE, len);
        if(!readFromDisk(torrentInfo->settings.dirfd, torrentInfo->metadata, buffer, pieceIndex, pieceOffset, len)) {
            TRACE("Failed to load data from disk\n");
            return -1;
        }
        data = buffer;
    }
    if(!data) {
        assert(0);
        return -1;
    }
    return write(outfd, data, len);
}

static int claimPiece(Peer* peer, PieceMetadata* piece){
    assert(peer->numOutstandingPieces < MAX_PEER_PEER_OUTSTANDING_PIECES);

    piece->claimCount++;
    peer->outstandingPieces[peer->numOutstandingPieces++] = piece->pieceIndex;
    piece->requestTime = -1;

    VERBOSE_PEER_MSG_WITH_ARGS("Claimed piece %d", peer, piece->pieceIndex);
    return 0;
}

void clearSendQueueOfRequestsForPiece(Peer* peer, MessageType type, int pieceIndex, int offset, int len) {
    assert(type == PIECE || type == REQUEST);
    for(int i = peer->sendIndexStart % SEND_QUEUE_LEN; i != peer->sendIndexEnd % SEND_QUEUE_LEN; i = (i+1) % SEND_QUEUE_LEN ) {
        if(peer->blocksToSend[i].type == type &&
                (pieceIndex == -1 ||
                    (peer->blocksToSend[i].blockInfo.pieceIndex == pieceIndex
                    && offset <= peer->blocksToSend[i].blockInfo.pieceOffset &&
                    peer->blocksToSend[i].blockInfo.pieceOffset <= offset + len)
                    )) {
            peer->blocksToSend[i].type = KEEP_ALIVE;
        }
    }
}

static void unclaimPiece(TorrentInfo* torrentInfo, Peer* peer, int pieceIndex, int complete) {
    assert(peer->numOutstandingPieces);
    if (!complete) {
        complete = isSet(&torrentInfo->downloadedBitmap, pieceIndex);
    }
    for(int n = 0; n < peer->numOutstandingPieces; n++) {
        if(peer->outstandingPieces[n] == pieceIndex) {
            PieceMetadata* piece = getPieceMetadata(torrentInfo, pieceIndex, 0);
            assert(piece);
            assert(piece->claimCount);
            --piece->claimCount;
            VERBOSE_PEER_MSG_WITH_ARGS("Unclaiming piece %d; complete %d", peer, peer->outstandingPieces[n], complete);
            if(!complete) {
                if(!piece->claimCount) {
                    VERBOSE("Marking piece index %d as unrequested\n", peer->outstandingPieces[n]);
                    clearBit(&torrentInfo->requested, pieceIndex);
                } else {
                    VERBOSE_PEER_MSG_WITH_ARGS("Finished portion of work for piece %d; unclaiming", peer, pieceIndex);
                }
                clearSendQueueOfRequestsForPiece(peer, REQUEST, pieceIndex, 0, torrentInfo->metadata->pieceLength);
            } else {
                piece->wasShared = 0;
                piece->invalidDownload = 0;
            }

            if(n != --peer->numOutstandingPieces) {
                peer->outstandingPieces[n] = peer->outstandingPieces[peer->numOutstandingPieces];
                copyBitMap(&peer->requestedBlocksBitmap[n], &peer->requestedBlocksBitmap[peer->numOutstandingPieces]);
            }
            resetBitMap(&peer->requestedBlocksBitmap[peer->numOutstandingPieces]);
            assert(isBitMapZero(&peer->requestedBlocksBitmap[peer->numOutstandingPieces],0, peer->requestedBlocksBitmap[peer->numOutstandingPieces].numBits));
            return;
        }
    }
    assert(0);
}

void unclaimAll(TorrentInfo* torrentInfo, Peer*peer) {
    VERBOSE_PEER_MSG("Unclaiming all pieces", peer);
    for(int i = peer->numOutstandingPieces - 1; i >=0; i--) {
        unclaimPiece(torrentInfo, peer, peer->outstandingPieces[i], 0);
    }
    assert(peer->numOutstandingPieces == 0);
    assert(getNumOutstandingBlocks(peer) == 0);
}

void unclaimAllPiecesAndRemovePeer(TorrentInfo* torrentInfo, int i) {
    unclaimAll(torrentInfo, &torrentInfo->peers[i]);
    removePeer(torrentInfo, i);
}

static void updateThrottleFlag(Peer* peer, const TorrentSettings* settings) {
    if(settings->maxUploadRate) {
        if(getPeerTransferRate(peer, UPLOAD) > settings->maxUploadRate) {
            if(!(peer->flags & THROTTING_PEER)) {
                VERBOSE_PEER_MSG("Throttling peer", peer);
                peer->flags |= THROTTING_PEER;
            }
        } else if ((peer->flags & THROTTING_PEER)) {
            VERBOSE_PEER_MSG("Unthrottling peer", peer);
            peer->flags &= ~THROTTING_PEER;
        }
    }
}

static void incrementPeerTransferCounts(TorrentInfo*torrentInfo, Peer* peer, TransferType type, int numBytes) {
    assert(numBytes <= torrentInfo->metadata->pieceLength);
    assert(type < NUM_TRANSFER_TYPES);
    incrementTransferCounts(&peer->stats[type], peer->statBucket, numBytes);
    torrentInfo->trackerInfo.transfers[type] += numBytes;
}

static inline void updateFlags(MessageType type, unsigned short* flags, int peer) {
    char bit;
    if(type == CHOKE || type == UNCHOKE) {
        bit = NOT_CHOKING_PEER;
    } else  {
        assert(type == INTERESTED || type == UNINTERESTED);
        bit = INTERESTED_IN_PEER;
    }
    if(peer)
        bit <<= 1;

    if(type == CHOKE || type == UNINTERESTED ) {
        *flags &= ~bit;
    } else if(type == UNCHOKE || type == INTERESTED) {
        *flags |= bit;
    }
}

int getNumOutstandingBlocks(const Peer* peer) {
    int count = 0;
    for(int i = 0; i < peer->numOutstandingPieces; i++) {
        count += popcount(&peer->requestedBlocksBitmap[i], 0, peer->requestedBlocksBitmap[i].numBits);
    }
    assert(count <= MAX_PEER_PEER_OUTSTANDING_BLOCKS);
    return count;
}

int getNumOutstandingBlocksForPiece(const Peer* peer, int pieceIndex) {
    int count = 0;
    for(int i = 0; i < peer->numOutstandingPieces; i++) {
        if(pieceIndex != peer->outstandingPieces[i])
            continue;
        for(int n = 0; n < peer->requestedBlocksBitmap[i].numBits; n++)
            count += isSet(&peer->requestedBlocksBitmap[i], n);
    }
    return count;
}

static int hasMaxOutstandingBlocks(const Peer* peer) {
    // Don't send if we are throttling uploads
    if (peer->flags & THROTTING_PEER) {
        return 0;
    }
    return getNumOutstandingBlocks(peer) >= getMaxNumberOfOutstandingBlocksForPeer(peer);
}

static int canSendBlocks(const Peer* peer) {
    // Finish sending what we are currently sending
    if (peer->currentSendMsg.sent) {
        return 1;
    }
    // Send handshake if we haven't already
    if (!(peer->flags & SENT_HANDSHAKE)) {
        return 1;
    }
    // Do we have something to send? If we do, do we have room to receive the response
    return (!isSendQueueEmpty(peer) &&
            (peer->blocksToSend[peer->sendIndexStart % SEND_QUEUE_LEN].type != REQUEST ||
            (!hasMaxOutstandingBlocks(peer))));
}

int queueMessageToSend(Peer* peer,MessageType type, const BlockInfo* blockInfo) {
    assert(doesPeerSupportExtension(peer, type));
    if(CHOKE <= type && type <= UNINTERESTED)
        updateFlags(type, &peer->flags, 0);
    if(type == CANCEL) {
        VERBOSE_PEER_MSG_WITH_ARGS("Clearing send queue of requests for piece %d", peer, blockInfo->pieceIndex);
        clearSendQueueOfRequestsForPiece(peer, REQUEST, blockInfo->pieceIndex, blockInfo->pieceOffset, blockInfo->len);
    } else if(type == CHOKE) {
        clearSendQueueOfRequestsForPiece(peer, PIECE, -1, 0, 0);
    }
    if(isSendQueueFull(peer)) {
        WARN("Can't queue event because queue is full %d %d\n", peer->sendIndexStart, peer->sendIndexEnd);
        for(int i = peer->sendIndexStart; i % SEND_QUEUE_LEN != peer->sendIndexEnd % SEND_QUEUE_LEN; i++) {
            WARN("%d %d; new type %d\n", i, peer->blocksToSend[i% SEND_QUEUE_LEN].type, type);
        }
        assert(0);
        return -1;
    }

    int empty = isSendQueueEmpty(peer);
    if(blockInfo)
        peer->blocksToSend[peer->sendIndexEnd % SEND_QUEUE_LEN].blockInfo = *blockInfo;
    peer->blocksToSend[peer->sendIndexEnd % SEND_QUEUE_LEN].type = type;
    peer->sendIndexEnd++;

    if (hasMaxOutstandingBlocks(peer))
        assert(type != REQUEST);
    if (type != REQUEST && !empty && hasMaxOutstandingBlocks(peer)) {
        while (peer->blocksToSend[peer->sendIndexStart % SEND_QUEUE_LEN].type == REQUEST ) {
            TRACE_PEER_MSG_WITH_ARGS("Queued message %d while having max outstanding blocks. Moving current head of queue to end", peer, type);
            memcpy(&peer->blocksToSend[peer->sendIndexEnd % SEND_QUEUE_LEN], &peer->blocksToSend[peer->sendIndexStart % SEND_QUEUE_LEN], sizeof(peer->blocksToSend[0]));
            peer->sendIndexStart++;
            peer->sendIndexEnd++;
        }
    }
    return 0;
}

static int prepareMesgToSend(const TorrentInfo* torrentInfo, Peer* peer, MessageType type, BlockInfo* blockInfo) {
    peer->currentSendMsg.type = type;
    assert(messageTypeStrings[type]);
    TRACE_PEER_MSG_WITH_ARGS("Preparing message type %d (%s) to peer:", peer, type, messageTypeStrings[type]);
    switch (type) {
        case KEEP_ALIVE:
            peer->currentSendMsg.len = 0;
            break;
        case CHOKE:
        case UNCHOKE:
        case INTERESTED:
        case UNINTERESTED:
        case HAVE_ALL:
        case HAVE_NONE:
            peer->currentSendMsg.len = htonl(1);
            break;
        case HAVE:
            peer->currentSendMsg.len = htonl(5);
            peer->currentSendMsg.blockInfo.pieceIndex = htonl(blockInfo->pieceIndex);
            peer->currentSendMsg.payload = &peer->currentSendMsg.blockInfo;
            break;
        case BITFIELD:
            if(isBitMapZero(&torrentInfo->downloadedBitmap, 0, torrentInfo->downloadedBitmap.numBits)) {
                TRACE("Not sending bitmap because it is empty\n");
                return -1;
            }
            peer->currentSendMsg.len = htonl(1 + getNumBytes(&torrentInfo->downloadedBitmap));
            peer->currentSendMsg.payload = torrentInfo->downloadedBitmap.data;
            break;
        case SUGGEST:
            peer->currentSendMsg.len = htonl(5);
            peer->currentSendMsg.blockInfo = (BlockInfo){htonl(blockInfo->pieceIndex)};
            peer->currentSendMsg.payload = &peer->currentSendMsg.blockInfo;
            break;
        case REJECT:
            peer->currentSendMsg.len = htonl(13);
            peer->currentSendMsg.blockInfo = (BlockInfo){htonl(blockInfo->pieceIndex), htonl(blockInfo->pieceOffset), htonl(blockInfo->len)};
            peer->currentSendMsg.payload = &peer->currentSendMsg.blockInfo;
            break;
        case REQUEST:
            assert(blockInfo->pieceIndex >= torrentInfo->startingPieceIndex);
            assert(blockInfo->pieceIndex < torrentInfo->endingPieceIndex);
            assert(blockInfo->len <= torrentInfo->metadata->totalLength);
            if(blockInfo->len > MAX_BLOCK_SIZE) {
                LOG_PEER_MSG_WITH_ARGS(LOG_LEVEL_INFO, "Peer requested block size that is too large 0x%X; Max is 0x%X", peer, blockInfo->len, MAX_BLOCK_SIZE);
                return -1;
            }
            assert (blockInfo->len <= torrentInfo->settings.blockSize);

            assert(!isSet(&torrentInfo->downloadedBitmap, blockInfo->pieceIndex));
            assert(isSet(&torrentInfo->requested, blockInfo->pieceIndex));
            assert(isSet(&peer->bitmap, blockInfo->pieceIndex));

            peer->currentSendMsg.len = htonl(13);
            peer->currentSendMsg.blockInfo = (BlockInfo){htonl(blockInfo->pieceIndex), htonl(blockInfo->pieceOffset), htonl(blockInfo->len)};
            peer->currentSendMsg.payload = &peer->currentSendMsg.blockInfo;
            break;
        case ALLOWED_FAST:
#ifndef NDEBUG
            for (int i = 0; i < LEN(peer->fastPieceSetOutgoing); i++)
                if (peer->fastPieceSetOutgoing[i] == -1 || peer->fastPieceSetOutgoing[i] == blockInfo->pieceIndex) {
                    peer->fastPieceSetOutgoing[i] = blockInfo->pieceIndex;
                    break;
                }
#endif
            peer->currentSendMsg.len = htonl(5);
            peer->currentSendMsg.blockInfo = (BlockInfo){htonl(blockInfo->pieceIndex)};
            peer->currentSendMsg.payload = &peer->currentSendMsg.blockInfo;
            break;
        case PIECE:
            assert(!torrentInfo->pedantic || isSet(&torrentInfo->downloadedBitmap, blockInfo->pieceIndex));

            assert(blockInfo && blockInfo->len);

            peer->currentSendMsg.len = htonl(9 + blockInfo->len);
            peer->currentSendMsg.blockInfo = (BlockInfo){htonl(blockInfo->pieceIndex), htonl(blockInfo->pieceOffset), htonl(blockInfo->len)};
            peer->currentSendMsg.payload = &peer->currentSendMsg.blockInfo;
            break;
        case CANCEL:
            assert(blockInfo->pieceOffset % torrentInfo->settings.blockSize == 0);
            for(int offset = blockInfo->pieceOffset; offset < blockInfo->pieceOffset + blockInfo->len; offset += torrentInfo->settings.blockSize ) {
                int blockIndex  = offset / torrentInfo->settings.blockSize;
                for(int n = 0; n < peer->numOutstandingPieces; n++)
                    if(peer->outstandingPieces[n] == blockInfo->pieceIndex) {
                        assert(isSet(&peer->requestedBlocksBitmap[n], blockIndex));
                        clearBit(&peer->requestedBlocksBitmap[n], blockIndex);
                        break;
                    }
            }
            peer->currentSendMsg.len = htonl(13);
            peer->currentSendMsg.blockInfo = (BlockInfo){htonl(blockInfo->pieceIndex), htonl(blockInfo->pieceOffset), htonl(blockInfo->len)};
            peer->currentSendMsg.payload = &peer->currentSendMsg.blockInfo;
            break;
        case EXTENSION:
            peer->currentSendMsg.len = htonl(strlen(torrentInfo->extensionHandshakeMsg) + 2);
            peer->currentSendMsg.subType = 0;
            peer->currentSendMsg.payload = torrentInfo->extensionHandshakeMsg;
            break;
        case DONT_HAVE:
            peer->currentSendMsg.type = EXTENSION;
            peer->currentSendMsg.len = htonl(6);
            assert(doesPeerSupportExtension(peer, DONT_HAVE));
            peer->currentSendMsg.subType = translateExtensionToPeerFormat(peer, DONT_HAVE);
            *((int*)peer->currentSendMsg.extensionPayload) = htonl(blockInfo->pieceIndex);
            peer->currentSendMsg.payload = peer->currentSendMsg.extensionPayload;
            break;
        default:
            VERBOSE("Unknown message type %d\n", type);
            return -1;
    }
    TRACE_PEER_MSG_WITH_ARGS("Sending message type %d (%s) len 0x%X to peer:", peer, type, messageTypeStrings[type], ntohl(peer->currentSendMsg.len));
    return 0;
}

static int updateOutstandingBlockCount(Peer* peer, int pieceIndex, int blockIndex, int delta) {
    for(int n = peer->numOutstandingPieces; n < LEN( peer->outstandingPieces); n++) {
        assert(isBitMapZero(&peer->requestedBlocksBitmap[n], 0, peer->requestedBlocksBitmap[n].numBits));
    }
    for(int n = 0; n < peer->numOutstandingPieces; n++) {
        if(peer->outstandingPieces[n] == pieceIndex) {
            assert(peer->requestedBlocksBitmap[1].data != peer->requestedBlocksBitmap[0].data);
            if(delta > 0) {
                assert(!isSet(&peer->requestedBlocksBitmap[n], blockIndex));
                setBit(&peer->requestedBlocksBitmap[n], blockIndex);
            } else {
                clearBit(&peer->requestedBlocksBitmap[n], blockIndex);
            }
            return 0;
        }
    }
    return -1;
}

static int isBlockAlreadyBeingRequested(const TorrentInfo* torrentInfo, int pieceIndex, int blockIndex) {
    for(int n = torrentInfo->numPeers - 1; n >= 0; n--)
        for(int i = 0; i < torrentInfo->peers[n].numOutstandingPieces; i++)
            if(torrentInfo->peers[n].outstandingPieces[i] == pieceIndex) {
                if(isSet(&torrentInfo->peers[n].requestedBlocksBitmap[i], blockIndex))
                    return 1;
                break;
            }
    return 0;
}

static int sendQueuedMsg(TorrentInfo* torrentInfo, Peer* peer) {
    int ret;
    if(peer->currentSendMsg.sent == 0) {
        if (!canSendBlocks(peer)) {
            return 0;
        }
        peer->currentSendMsg.sent = 0;
        peer->currentSendMsg.len = 0;

        int currentIndex = peer->sendIndexStart % SEND_QUEUE_LEN;
        BlockInfo* blockInfo = &peer->blocksToSend[currentIndex].blockInfo;
        MessageType type = peer->blocksToSend[currentIndex].type;
        if (type == REQUEST) {
            if(isSet(&torrentInfo->downloadedBitmap, blockInfo->pieceIndex)) {
                VERBOSE("We already downloaded piece %d; Not requesting it again\n", blockInfo->pieceIndex);
                peer->sendIndexStart++;
                return sendQueuedMsg(torrentInfo, peer);
            }
            BlockInfo realBlockInfo = {blockInfo->pieceIndex, blockInfo->pieceOffset, MIN(blockInfo->len, torrentInfo->settings.blockSize)};
            int requestingLastBlock = 0;
            peer->sendIndexStart++;
            if (blockInfo->len > torrentInfo->settings.blockSize) {
                blockInfo->len         -= torrentInfo->settings.blockSize;
                blockInfo->pieceOffset += torrentInfo->settings.blockSize;
                queueMessageToSend(peer, REQUEST, blockInfo);
            } else {
                requestingLastBlock = 1;
            }

            PieceMetadata* pieceMetadata = getPieceMetadata(torrentInfo, realBlockInfo.pieceIndex, 0);
            assert(pieceMetadata);
            unsigned int blockIndex = realBlockInfo.pieceOffset / torrentInfo->settings.blockSize;
            int skipBlock = 0;
            if (pieceMetadata->wasShared) {
                if(isBlockAlreadyBeingRequested(torrentInfo, realBlockInfo.pieceIndex, blockIndex)) {
                    TRACE("Another peer is already requested block %d of piece %d; Not requesting it again\n", blockIndex, realBlockInfo.pieceIndex);
                    skipBlock = 1;
                }
            }

            if (isSet(&pieceMetadata->blockBitmap, blockIndex)) {
                TRACE("We already have block %d of piece %d; Not requesting it again\n", realBlockInfo.pieceOffset / MIN_BLOCK_SIZE, realBlockInfo.pieceIndex);
                skipBlock = 1;
            }
            if(skipBlock) {
                if(requestingLastBlock && getNumOutstandingBlocksForPiece(peer, realBlockInfo.pieceIndex) == 0) {
                    unclaimPiece(torrentInfo, peer, realBlockInfo.pieceIndex, 0);
                }
                return sendQueuedMsg(torrentInfo, peer);
            }
            updateOutstandingBlockCount(peer, realBlockInfo.pieceIndex, blockIndex, 1);
            if(pieceMetadata->requestTime == -1) {
                pieceMetadata->requestTime = getTimeMS();
            }
            TRACE_PEER_MSG_WITH_ARGS("Requesting block %d of piece %d;", peer, realBlockInfo.pieceOffset / MIN_BLOCK_SIZE, realBlockInfo.pieceIndex);
            ret = prepareMesgToSend(torrentInfo, peer, REQUEST, &realBlockInfo);
            assert(ret != -1);
        } else {
            ret = prepareMesgToSend(torrentInfo, peer,
                peer->blocksToSend[peer->sendIndexStart % SEND_QUEUE_LEN].type,
                &peer->blocksToSend[peer->sendIndexStart % SEND_QUEUE_LEN].blockInfo);
            peer->sendIndexStart++;
            if (type == KEEP_ALIVE && !isSendQueueEmpty(peer)) {
                TRACE("Skipping sending KEEP_ALIVE because send queue isn't empty\n");
                return sendQueuedMsg(torrentInfo, peer);
            }
        }
        if(ret == -1)
            return 0;
    }
    int len = ntohl(peer->currentSendMsg.len);

    int isExtension = isExtensionProtocalMessage(peer->currentSendMsg.type);
    if(peer->currentSendMsg.sent < 4) {
        ret = write(peer->fd, &peer->currentSendMsg.len, 4);
    } else if(peer->currentSendMsg.sent < 5) {
        ret = write(peer->fd, &peer->currentSendMsg.type, 1);
    } else if(isExtension && peer->currentSendMsg.sent == 5) {
        ret = write(peer->fd, &peer->currentSendMsg.subType, 1);
    } else if(peer->currentSendMsg.type == PIECE) {
        if(peer->currentSendMsg.sent < 13) {
            int offset = peer->currentSendMsg.sent - 5;
            assert(peer->currentSendMsg.payload);
            ret = write(peer->fd, peer->currentSendMsg.payload + offset, 8 - offset);
        } else {
            int offset = peer->currentSendMsg.sent - 13;
            int pieceIndex = ntohl(peer->currentSendMsg.blockInfo.pieceIndex);
            int pieceOffset = ntohl(peer->currentSendMsg.blockInfo.pieceOffset);
            assert(!torrentInfo->pedantic || isSet(&torrentInfo->downloadedBitmap, pieceIndex));
            ret = writePieceData(torrentInfo, peer->fd, pieceIndex, pieceOffset + offset, len - 9 - offset);
            if(ret != -1) {
                incrementPeerTransferCounts(torrentInfo, peer, UPLOAD, ret);
                updateThrottleFlag(peer, &torrentInfo->settings);
            }
        }
    } else {
        assert(peer->currentSendMsg.payload);
        int offset = peer->currentSendMsg.sent - 5 - isExtension;
        ret = write(peer->fd, peer->currentSendMsg.payload + offset, len - 1 - offset  - isExtension);
    }
    if(ret == -1) {
        ERROR("Failed to write");
        return -1;
    }

    peer->currentSendMsg.sent += ret;
    assert(4 + ntohl(peer->currentSendMsg.len) >= peer->currentSendMsg.sent);

    if(peer->currentSendMsg.sent == 4 + ntohl(peer->currentSendMsg.len)) {
        peer->currentSendMsg.sent = 0;
    }
    return ret;
}

static int isPieceClaimedByPeer(const Peer* peer, int pieceIndex) {
    for(int n = 0; n < peer->numOutstandingPieces; n++)
        if(peer->outstandingPieces[n] == pieceIndex)
            return 1;
    return 0;
}

int receiveMessage(TorrentInfo* torrentInfo, Peer* peer) {
    assert(peer->flags & RECEIVED_HANDSHAKE);
    int len = ntohl(peer->currentReceiveMsg.len);
    if(len == 0) {
        TRACE("Received KEEP_ALIVE\n");
        if(getTimeMS() - peer->lastSentMessageTimeMS > MIN_KEEP_ALIVE_TIME && isSendQueueEmpty(peer))
            queueMessageToSend(peer, KEEP_ALIVE, NULL);

        return 0;
    }
    MessageType type = peer->currentReceiveMsg.header[0];
    assert(type < NUM_MESSAGE_TYPES);
    int * intData = (int*)&peer->currentReceiveMsg.header[1];
    TRACE_PEER_MSG_WITH_ARGS("Received message type %d (%s) len 0x%X from peer",peer, type, messageTypeStrings[type], len);

    switch(type) {
        case CHOKE:
            if (!doesPeerSupportExtension(peer, REJECT)) {
                unclaimAll(torrentInfo, peer);
            }
        case UNCHOKE:
        case INTERESTED:
        case UNINTERESTED:
            updateFlags(type, &peer->flags, 1);
            return 0;
        case HAVE_ALL:
            setAllBits(&peer->bitmap);
            return 0;
        case HAVE_NONE:
            resetBitMap(&peer->bitmap);
            return 0;
        case BITFIELD:
            if(len - 1 == getNumBytes(&torrentInfo->downloadedBitmap)) {
                peer->currentReceiveMsg.destBuffer = peer->bitmap.data;
                return 1;
            }
            VERBOSE("Received invalid len %d", len);
            return -1;
        case EXTENSION:
            peer->scratchBuffer = peer->currentReceiveMsg.destBuffer = malloc(len);
            return 2;
    }

    unsigned int pieceIndex = ntohl(intData[0]), pieceOffset = ntohl(intData[1]), blockLen = ntohl(intData[2]);
    if(pieceIndex > torrentInfo->metadata->numPieces) {
        VERBOSE("Reported piece index is greater than number of pieces %d %d\n", pieceIndex, torrentInfo->metadata->numPieces);
        assert(!torrentInfo->pedantic);
        return -1;
    }

    switch(type) {
        case HAVE:
            // If the peer obtained something we want, then we are interested in them
            // TODO also check that piece isn't in flight
            if (!isSet(&torrentInfo->downloadedBitmap, pieceIndex) && !(peer->flags & INTERESTED_IN_PEER)) {
                queueMessageToSend(peer, INTERESTED, NULL);
            }
            setBit(&peer->bitmap, pieceIndex);
            break;
        case REJECT:
            if (isPieceClaimedByPeer(peer, pieceIndex))
                unclaimPiece(torrentInfo, peer, pieceIndex, 0);
            break;
        case SUGGEST:
            if (peer->suggestedPiece == -1 && isSet(&torrentInfo->downloadedBitmap, peer->suggestedPiece))
                peer->suggestedPiece = pieceIndex;
            break;
        case ALLOWED_FAST:
            if (!isSet(&torrentInfo->downloadedBitmap, pieceIndex) && torrentInfo->startingPieceIndex <= pieceIndex && pieceIndex < torrentInfo->endingPieceIndex) {
                for (int i = 0; i < LEN(peer->fastPieceSet); i++) {
                    if (peer->fastPieceSet[i] == -1) {
                        peer->fastPieceSet[i] = pieceIndex;
                        break;
                    }
                }
            }
            break;
        case REQUEST:
            if(pieceIndex == -1 || pieceOffset == -1 || blockLen == -1)
                return -1;
            if(blockLen > MAX_BLOCK_SIZE) {
                VERBOSE_PEER_MSG_WITH_ARGS("Peer requested a block size of 0x%X; max is 0x%X", peer, blockLen, MAX_BLOCK_SIZE);
                return -1;
            }
            if(pieceIndex * torrentInfo->metadata->pieceLength + pieceOffset + blockLen > torrentInfo->metadata->totalLength) {
                VERBOSE_PEER_MSG("Peer requested out of bounds data", peer);
                return -1;
            }
            assert(blockLen);
            BlockInfo blockInfo = {pieceIndex, pieceOffset, blockLen};
#ifndef NDEBUG
            int ignoreChoke = 0;
            for (int i = 0; i < LEN(peer->fastPieceSetOutgoing); i++) {
                if (peer->fastPieceSetOutgoing[i] == pieceIndex) {
                    ignoreChoke = 1;
                    break;
                }
            }
            if (!ignoreChoke)
#endif
            if (!(peer->flags & NOT_CHOKING_PEER)) {
                TRACE_PEER_MSG_WITH_ARGS("Choking peer so not queuing block ; index: %d offset: %d len: %d;", peer, pieceIndex, pieceOffset, blockLen);
                if (doesPeerSupportExtension(peer, REJECT)) {
                    queueMessageToSend(peer, REJECT, &blockInfo);
                }
                break;
            }
            TRACE_PEER_MSG_WITH_ARGS("Queuing block to send for peer; index: %d offset: %d len: %d;", peer, pieceIndex, pieceOffset, blockLen);
            queueMessageToSend(peer, PIECE, &blockInfo);
            break;
        case PIECE:
            if(pieceIndex == -1 || pieceOffset == -1 || len - 9 > torrentInfo->settings.blockSize || len - 9 > torrentInfo->metadata->totalLength) {
                TRACE("Received bad values %d %d 0x%X\n", pieceIndex, pieceOffset, len - 9);
                return -1;
            }
            PieceMetadata* piece = getPieceMetadata(torrentInfo, pieceIndex, 0);
            if(isSet(&torrentInfo->downloadedBitmap, pieceIndex) || !piece || !isPieceClaimedByPeer(peer, pieceIndex)) {
                peer->currentReceiveMsg.destBuffer = NULL;
                return 0;
            } else {
                peer->currentReceiveMsg.destBuffer = piece->data + pieceOffset;
            }
            return 9;
        case CANCEL:
            if (doesPeerSupportExtension(peer, REJECT)) {
                queueMessageToSend(peer, REJECT, &blockInfo);
            }
            clearSendQueueOfRequestsForPiece(peer, PIECE, pieceIndex, pieceOffset, blockLen);
            break;
        default:
            return -1;
    }
    return 0;
}

int handleSendingToPeer(TorrentInfo* torrentInfo, int peerIndex) {
    Peer* peer = &torrentInfo->peers[peerIndex];
    peer->lastSentMessageTimeMS = getTimeMS();
    if (!(peer->flags & SENT_HANDSHAKE)) {
        int i = 0, ret = 0;
        const char pstrlen = strlen(PSTR);
        assert(sizeof(torrentInfo->settings.extensions) == 8);
        int lens[] = {1, pstrlen, 8, HASH_SIZE, CLIENT_ID_LEN};
        const void* srcs[] = {&pstrlen, PSTR, &torrentInfo->settings.extensions, torrentInfo->metadata->infoHash, torrentInfo->clientId};
        for(int pos = 0; i < LEN(lens); i++) {
            if(peer->currentSendMsg.sent <= pos) {
                int offset = peer->currentSendMsg.sent - pos;
                int amount = write(peer->fd, srcs[i] + offset , lens[i] - offset);
                if (amount == -1)
                    break;
                ret += amount;
                peer->currentSendMsg.sent += amount;
            }
            pos += lens[i];
        }
        if(i == LEN(lens)) {
            peer->flags |= SENT_HANDSHAKE;
            VERBOSE_PEER_MSG("Sent handshake to", peer);
            peer->currentSendMsg.sent = 0;
        }
        return ret;
    } else {
        return sendQueuedMsg(torrentInfo, peer);
    }
}


static inline int hasRequestPending(const Peer*peer, int pieceIndex) {
    for(int i = peer->sendIndexStart % SEND_QUEUE_LEN; i != peer->sendIndexEnd % SEND_QUEUE_LEN; i = (i+1) % SEND_QUEUE_LEN) {
        if(peer->blocksToSend[i].type == REQUEST && (pieceIndex == -1 || peer->blocksToSend[i].blockInfo.pieceIndex == pieceIndex)) {
            return 1;
        }
    }
    return 0;
}

static inline void sendMessageToAllRelevantPeers(TorrentInfo* torrentInfo, MessageType type, int pieceIndex) {
    const BlockInfo blockInfo = {pieceIndex};
    for(int n = 0; n < torrentInfo->numPeers; n++) {
        if((torrentInfo->peers[n].flags & (RECEIVED_HANDSHAKE|SENT_HANDSHAKE)) == (RECEIVED_HANDSHAKE|SENT_HANDSHAKE))
            if(type == HAVE || doesPeerSupportExtension(&torrentInfo->peers[n], type)) {
                queueMessageToSend(&torrentInfo->peers[n], type, &blockInfo);
            }
    }
}

static inline void receiveBlock(TorrentInfo* torrentInfo, Peer* peer, int pieceIndex, int pieceOffset, int len) {
    if(isSet(&torrentInfo->downloadedBitmap, pieceIndex)) {
        assert(torrentInfo->pedantic < 2);
        VERBOSE_PEER_MSG_WITH_ARGS("ignoring block; pieceIndex %d offset %d\n", peer, pieceIndex, pieceOffset);
        return;
    }

    assert(pieceOffset % torrentInfo->settings.blockSize == 0);

    PieceMetadata* piece = getPieceMetadata(torrentInfo, pieceIndex, 0);
    assert(piece);

    int pieceLen = getPieceLen(torrentInfo->metadata, pieceIndex);
    assert(len <= pieceLen);

    assert(pieceOffset % torrentInfo->settings.blockSize == 0);
    int blockIndex  = pieceOffset / torrentInfo->settings.blockSize;
    TRACE_PEER_MSG_WITH_ARGS("Received block piece %d block %d", peer, pieceIndex, blockIndex);
    if(isSet(&piece->blockBitmap, blockIndex)) {
        VERBOSE_PEER_MSG("Received block we already have", peer);
        assert(torrentInfo->pedantic < 2);
        return;
    }

    if(updateOutstandingBlockCount(peer, pieceIndex, blockIndex, -1) == -1) {
        VERBOSE_PEER_MSG_WITH_ARGS("Peer was not expected to send piece %d block %d", peer, pieceIndex, blockIndex);
        assert(torrentInfo->pedantic < 2);
        return;
    }

    setBit(&piece->blockBitmap, blockIndex);

    piece->requestTime = getTimeMS();

    incrementPeerTransferCounts(torrentInfo, peer, DOWNLOAD, len);

    if (isBitMapOne(&piece->blockBitmap, 0, ((pieceLen + torrentInfo->settings.blockSize - 1 )/ torrentInfo->settings.blockSize))) {
        TRACE("Downloaded all blocks of piece %d\n", pieceIndex);
        resetBitMap(&piece->blockBitmap);
        if(setBitIfPieceIsValid(torrentInfo->metadata, pieceIndex, piece->data, pieceLen, &torrentInfo->downloadedBitmap)) {
            VERBOSE("Completed piece %d of range [%d, %d); %d%% download\n",
                    pieceIndex, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex,
                    100 * popcount(&torrentInfo->downloadedBitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex) / (torrentInfo->endingPieceIndex - torrentInfo->startingPieceIndex));
            unclaimPiece(torrentInfo, peer, pieceIndex, 1);
            setBit(&torrentInfo->cacheBitmap, pieceIndex);
        } else {
            VERBOSE_PEER_MSG_WITH_ARGS("Data for piece %d doesn't match hash; Last block given by peer\n", peer, pieceIndex);
            unclaimPiece(torrentInfo, peer, pieceIndex, 0);
            piece->invalidDownload = 1;
            peer->errCount++;
            assert(!torrentInfo->pedantic);
            return;
        }
        torrentInfo->trackerInfo.left -= pieceLen;
        peer->errCount = 0;
        sendMessageToAllRelevantPeers(torrentInfo, HAVE, pieceIndex);

        if(torrentInfo->settings.dirfd != -1) {
            if(writeToDisk(torrentInfo->settings.dirfd, torrentInfo->metadata, piece->data, pieceIndex, 0, pieceLen)) {
                setBit(&torrentInfo->ondisk, pieceIndex);
            } else {
                VERBOSE("Failed to write data to disk\n");
                assert(0);
                return;
            }
        }

        if(torrentInfo->settings.quick && isBitMapOne(&torrentInfo->downloadedBitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex)) {
            VERBOSE("Finished [%d, %d)\n", torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex);
            GLOBAL_FLAGS |= SIGNAL_FLAG_FINISHED_DOWNLOADING;
        }
    } else {
        if(getNumOutstandingBlocksForPiece(peer, pieceIndex) == 0 && !hasRequestPending(peer, pieceIndex)) {
            assert(piece->wasShared);
            unclaimPiece(torrentInfo, peer, pieceIndex, 0);
        }
    }
}

static int hasPieceClaimed(const Peer* peer, int pieceIndex) {
    for(int i = 0; i < peer->numOutstandingPieces; i++) {
        if (peer->outstandingPieces[i] == pieceIndex)
            return 1;
    }
    return 0;
}

int handlePeerMessage(TorrentInfo* torrentInfo, int peerIndex) {
    Peer* peer = &torrentInfo->peers[peerIndex];
    peer->lastReceiveMessageTimeMS = getTimeMS();
    int ret = 0;
    if (!(peer->flags & RECEIVED_HANDSHAKE)) {
        const char pstrlen = strlen(PSTR);
        const int lens[] = {1, pstrlen, 8, HASH_SIZE, CLIENT_ID_LEN};
        void* dests[] = {&peer->currentReceiveMsg.len, peer->handshakeBuffer, &peer->extensionBytes, peer->handshakeBuffer, peer->clientId};
        const void* targets[] = {&pstrlen, PSTR, NULL, torrentInfo->metadata->infoHash, NULL};
        int i = 0;
        for(int pos = 0; i < LEN(lens); i++) {
            if(peer->currentReceiveMsg.read <= pos) {
                int offset = peer->currentReceiveMsg.read - pos;
                int amount = read(peer->fd, dests[i] + offset , lens[i] - offset);
                if (amount == -1)
                    break;
                ret += amount;
                peer->currentReceiveMsg.read += amount;
                if (lens[i] - offset != amount)
                    break;
                if(ret + offset == lens[i] && targets[i] && memcmp(targets[i], dests[i], lens[i])) {
                    LOG_PEER_MSG_WITH_ARGS(LOG_LEVEL_WARN, "Failed handshake on step %d", peer, i);
                    return -1;
                }
            }
            pos += lens[i];
        }
        if(ret == -1)
            return -1;
        if(i == LEN(lens)) {
            peer->flags |= RECEIVED_HANDSHAKE;
            VERBOSE_PEER_MSG("Received handshake from", peer);
            peer->currentReceiveMsg.read = 0;
            if (memcmp(peer->clientId, torrentInfo->clientId, CLIENT_ID_LEN) == 0) {
                    VERBOSE_PEER_MSG("Peer has the same id as us so this is probably a self connection. Dropping", peer);
                    return 0;
            }
            for(int i = torrentInfo->numPeers - 1; i >= 0; i--) {
                if(peer != &torrentInfo->peers[i] && peer->ip == torrentInfo->peers[i].ip && memcmp(peer->clientId, torrentInfo->peers[i].clientId, CLIENT_ID_LEN) == 0) {
                    VERBOSE_PEER_MSG_WITH_ARGS("Was already connected to peer; port %d (%d) vs %d (%d)", peer, peer->port,peer->localPort, torrentInfo->peers[i].port, torrentInfo->peers[i].localPort);
                    assert(peer->localPort ^ torrentInfo->peers[i].localPort);
                    if(peer->localPort) {
                        torrentInfo->peers[i].localPort = peer->localPort;
                        VERBOSE_PEER_MSG("We initiated the duplicate connection so dropping", peer);
                        return -1;
                    }
                    else
                        peer->localPort = torrentInfo->peers[i].localPort;
                }
            }
            if (torrentInfo->extensionHandshakeMsg && doesPeerSupportExtension(peer, EXTENSION))
                queueMessageToSend(peer, EXTENSION, NULL);
            queueMessageToSend(peer, BITFIELD, NULL);
            queueMessageToSend(peer, UNCHOKE, NULL);
        }
    } else {
        int amountRead = peer->currentReceiveMsg.read;
        int headerLen = MIN(ntohl(peer->currentReceiveMsg.len), sizeof(peer->currentReceiveMsg.header));
        if (peer->currentReceiveMsg.read < 4) {
            ret = read(peer->fd, &peer->currentReceiveMsg.len + amountRead, 4);
        } else if (amountRead < headerLen + 4) {
            int offset = amountRead - 4;
            ret = read(peer->fd, peer->currentReceiveMsg.header + offset, headerLen - offset);
            if(offset + ret == headerLen) {
                int skip = receiveMessage(torrentInfo, peer);
                peer->currentReceiveMsg.skip = skip;
                if(skip == -1) {
                    return -1;
                }
                if(skip) {
                    assert(peer->currentReceiveMsg.destBuffer);
                    memcpy(peer->currentReceiveMsg.destBuffer, peer->currentReceiveMsg.header + skip, headerLen - skip);
                }
            }
        } else {
            int offset = amountRead - 4;
            int maxAmountToRead = ntohl(peer->currentReceiveMsg.len) - offset;
            void* dest = peer->currentReceiveMsg.destBuffer + offset - peer->currentReceiveMsg.skip;
            if(peer->currentReceiveMsg.destBuffer == NULL) {
                static char buffer[1024];
                maxAmountToRead = MIN(1024, maxAmountToRead);
                dest = buffer;
            }
            ret = read(peer->fd, dest, maxAmountToRead);
        }
        peer->currentReceiveMsg.read += ret;
        if(peer->currentReceiveMsg.read == ntohl(peer->currentReceiveMsg.len) + 4) {
            char type = peer->currentReceiveMsg.header[0];
            if(peer->currentReceiveMsg.len == 0)  {
                receiveMessage(torrentInfo, peer);
            } else if(type == BITFIELD) {
                if (!(peer->flags & INTERESTED_IN_PEER) && findFirstMissingBit(&torrentInfo->downloadedBitmap, &peer->bitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex) != -1)
                    queueMessageToSend(peer, INTERESTED, NULL);
                if(isBitMapOne(&peer->bitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex)) {
                    peer->flags |= PEER_SEEDING_TO_ME;
                }
            } else if(type == PIECE) {
                int * intData = (int*)&peer->currentReceiveMsg.header[1];
                unsigned int pieceIndex = ntohl(intData[0]), pieceOffset = ntohl(intData[1]);
                if(peer->currentReceiveMsg.destBuffer)
                    receiveBlock(torrentInfo, peer, pieceIndex, pieceOffset, ntohl(peer->currentReceiveMsg.len) - 9);
                else {
                    VERBOSE_PEER_MSG_WITH_ARGS("ignoring block; pieceIndex %d offset %d because we weren't expecting this peer to send it\n", peer, pieceIndex, pieceOffset);
                }
            } else if(type == EXTENSION) {
                switch(peer->currentReceiveMsg.header[1]) {
                    default:
                        return -1;
                    case 0:
                        receiveExtensionHandshake(peer->currentReceiveMsg.destBuffer, ntohl(peer->currentReceiveMsg.len) - 2, peer);
                        break;
                    case DONT_HAVE: {
                        int pieceIndex = htonl(*(int*)peer->currentReceiveMsg.destBuffer);
                        if (hasPieceClaimed(peer, pieceIndex))
                            unclaimPiece(torrentInfo, peer, pieceIndex, 0);
                        clearBit(&peer->bitmap, pieceIndex);
                        break;
                    }
                }
                free(peer->scratchBuffer);
                peer->scratchBuffer = NULL;
            }
            peer->currentReceiveMsg.read = 0;
        }
    }
    return ret;
}

static int canOutputNextPiece(TorrentInfo* torrentInfo) {
    return torrentInfo->nextPieceToOutput < torrentInfo->endingPieceIndex && isSet(&torrentInfo->downloadedBitmap, torrentInfo->nextPieceToOutput);
}
static void outputNextPiece(TorrentInfo* torrentInfo) {
    int pieceLen = getPieceLen(torrentInfo->metadata, torrentInfo->nextPieceToOutput);
    int len = pieceLen, offset = 0;
    if (torrentInfo->nextPieceToOutput == torrentInfo->firstPieceToOutput) {
        offset = torrentInfo->offsetIntoStartingPiece;
        len -= offset;
    }
    if(torrentInfo->nextPieceToOutput == torrentInfo->lastPieceToOutput - 1) {
        len = torrentInfo->lenOfLastPieceToOutput - offset;
    }
    TRACE("Writing out piece %d offset %d len %d (of %d)\n", torrentInfo->nextPieceToOutput, offset, len, pieceLen);
    int ret = writePieceData(torrentInfo, torrentInfo->outputFd, torrentInfo->nextPieceToOutput, offset + torrentInfo->nextPieceToOutputOffset, len - torrentInfo->nextPieceToOutputOffset);
    if ( ret != -1) {
        torrentInfo->nextPieceToOutputOffset += ret;
        incrementTransferCounts(&torrentInfo->outputStats, torrentInfo->statBucket, ret);
        if(torrentInfo->nextPieceToOutputOffset == len) {
            torrentInfo->nextPieceToOutputOffset = 0;
            if(++torrentInfo->nextPieceToOutput == torrentInfo->lastPieceToOutput) {
                VERBOSE("Finished Outputting\n");
                GLOBAL_FLAGS |= SIGNAL_FLAG_FINISHED_OUTPUTTING;
            }
        }
    }
}

static int inline validate(const TorrentInfo* torrentInfo) {
    VERBOSE("Validating\n");
    int errors = 0;
    if (!torrentInfo->settings.daemon) {
        assert(torrentInfo->nextPieceToOutput == torrentInfo->lastPieceToOutput);
    }

    BitMap bitmap;
    initBitMap(&bitmap, torrentInfo->metadata->numPieces);

    for(int pieceIndex = torrentInfo->startingPieceIndex; pieceIndex < torrentInfo->endingPieceIndex; pieceIndex++) {
        PieceMetadata* pieceMetadata = getPieceMetadata(torrentInfo, pieceIndex, 0);
        if(!pieceMetadata )
            continue;
        if(!setBitIfPieceIsValid(torrentInfo->metadata, pieceIndex, pieceMetadata->data, getPieceLen(torrentInfo->metadata, pieceIndex), &bitmap)) {
            VERBOSE("Piece index %d is invalid\n", pieceIndex);
            errors++;
        }
    }
    assert(errors == 0);
    if(torrentInfo->settings.dirfd != -1) {
        resetBitMap(&bitmap);
        loadBitmapFromDisk(torrentInfo->settings.dirfd, torrentInfo->metadata, &bitmap);
        assert(isBitMapOne(&bitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex));
    } else {
        assert(isBitMapZero(&torrentInfo->cacheBitmap, 0, torrentInfo->startingPieceIndex));
        assert(isBitMapZero(&torrentInfo->cacheBitmap, torrentInfo->endingPieceIndex, torrentInfo->metadata->numPieces));
    }
    free(bitmap.data);
    return !errors;
}

static void requestPiece(TorrentInfo* torrentInfo, Peer* peer, PieceMetadata* pieceMetadata, int pieceIndex) {
    claimPiece(peer, pieceMetadata);

    if (pieceMetadata->oldPieceIndex != -1) {
        VERBOSE("Evicted piece %d from cache\n", pieceMetadata->oldPieceIndex);
        clearBit(&torrentInfo->cacheBitmap, pieceMetadata->oldPieceIndex);
        if(!torrentInfo->ondisk.data || !isSet(&torrentInfo->ondisk, pieceMetadata->oldPieceIndex)) {
            clearBit(&torrentInfo->downloadedBitmap, pieceMetadata->oldPieceIndex);
            sendMessageToAllRelevantPeers(torrentInfo, DONT_HAVE, pieceMetadata->oldPieceIndex);
        }
    }

    setBit(&torrentInfo->requested, pieceIndex);

    TRACE("Requesting piece index: %d of %d\n", pieceIndex, torrentInfo->metadata->numPieces);
    if (!peer->firstRequestTimeMS) {
        peer->firstRequestTimeMS = getTimeMS();
    }

    BlockInfo blockInfo = {pieceIndex, 0, getPieceLen(torrentInfo->metadata, pieceIndex)};
    queueMessageToSend(peer, REQUEST, &blockInfo);
}

static int requestNextBlockFromPeer(TorrentInfo*torrentInfo, Peer* peer, int start, int end) {
    int pieceIndex = -1;
    if (peer->suggestedPiece != -1 && start <= peer->suggestedPiece && peer->suggestedPiece < end && !isSet(&torrentInfo->downloadedBitmap, peer->suggestedPiece) && !isSet(&torrentInfo->requested, peer->suggestedPiece)) {
        pieceIndex = peer->suggestedPiece;
    }
    if (pieceIndex != -1) {
        pieceIndex = findFirstMissingBit(&torrentInfo->requested, &peer->bitmap, start, end);
    }
    assert(pieceIndex != end);
    if(pieceIndex == -1) {
        pieceIndex = findFirstMissingBit(&torrentInfo->requested, &peer->bitmap, MAX(torrentInfo->nextPieceToOutput, torrentInfo->startingPieceIndex), start);
        assert(pieceIndex < start);
    }
    if(pieceIndex == -1) {
        pieceIndex = findFirstMissingBit(&torrentInfo->requested, &peer->bitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex);
    }
    if(pieceIndex == -1) {
        assert(peer->flags & INTERESTED_IN_PEER);
        if(findFirstMissingBit(&torrentInfo->downloadedBitmap, &peer->bitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex) == -1) {
            queueMessageToSend(peer, UNINTERESTED, NULL);
            VERBOSE_PEER_MSG("No longer interested in peer", peer);
        }
        return -1;
    }
    assert(!isSet(&torrentInfo->downloadedBitmap, pieceIndex));
    PieceMetadata* pieceMetadata = getPieceMetadata(torrentInfo, pieceIndex, 1);
    if(!pieceMetadata) {
        TRACE("No room in cache for piece %d\n", pieceIndex);
        return -1;
    }
    assert(!isSet(&torrentInfo->requested, pieceIndex));
    requestPiece(torrentInfo, peer, pieceMetadata, pieceIndex);
    return 0;
}
static int requestPieceThatMightBeClaimed(TorrentInfo*torrentInfo, Peer* peer, PieceMetadata* piece, int stealPiece, int sharePiece, Peer* currentOwner) {
    int pieceIndex = piece->pieceIndex;
    if (hasPieceClaimed(peer, pieceIndex))
        return -1;

    assert(piece);

    if (!isSet(&peer->bitmap, pieceIndex)) {
        // peer doesn't have this piece
        return -1;
    }
    if (piece->claimCount) {
        if (piece->invalidDownload) {
            // If we failed to get a valid piece, we'll retry with a single peer so we can tell which peer sent corrupted data
            return -1;
        }

        if (stealPiece && currentOwner) {
            VERBOSE_PEER_MSG_WITH_ARGS("Stealing next piece to output (%d) from peer because it is slow", currentOwner, pieceIndex);
            unclaimPiece(torrentInfo, currentOwner, pieceIndex, 0);
        } else if (piece->claimCount == 1 && sharePiece) {
            piece->wasShared = 1;
            VERBOSE_PEER_MSG_WITH_ARGS("Sharing piece %d", peer, pieceIndex);
        } else {
            // piece can only be shared with between 2 peer
            return -1;
        }
    }

    requestPiece(torrentInfo, peer, piece, pieceIndex);
    return 0;
}

static int requestClaimed(TorrentInfo*torrentInfo, Peer* peer) {
    for(int n = 0; n < torrentInfo->numPeers; n++) {
        Peer* other = &torrentInfo->peers[n];
        if(peer == other || !(other->flags & PEER_IS_THROTTLING_ME))
            continue;
        for(int i = 0; i < other->numOutstandingPieces; i++) {
            int pieceIndex = other->outstandingPieces[i];

            PieceMetadata* piece = getPieceMetadata(torrentInfo, pieceIndex, 0);
            int haveAllRemainingBlocksBeenRequested = popcount(&other->requestedBlocksBitmap[i], 0, other->requestedBlocksBitmap[i].numBits) + popcount(&piece->blockBitmap, 0, piece->blockBitmap.numBits) == piece->blockBitmap.numBits;
            int stealPiece = isPeerReallySlow(peer) && haveAllRemainingBlocksBeenRequested &&
                (torrentInfo->nextPieceToOutput == pieceIndex || pieceIndex == findFirstClearBit(&torrentInfo->downloadedBitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex));
            if (requestPieceThatMightBeClaimed(torrentInfo, peer, piece, stealPiece, !haveAllRemainingBlocksBeenRequested, other) == 0)
                return 0;
        }
    }
    return -1;
}

static int requestFastPieceWhileChoked(TorrentInfo*torrentInfo, Peer* peer) {
    for (int i = 0; i < LEN(peer->fastPieceSet); i++) {
        int pieceIndex = peer->fastPieceSet[i];
        if (pieceIndex != -1 && !isSet(&torrentInfo->downloadedBitmap, pieceIndex) && !isSet(&torrentInfo->requested, pieceIndex)) {
            PieceMetadata* piece = getPieceMetadata(torrentInfo, pieceIndex, 1);
            if (piece && requestPieceThatMightBeClaimed(torrentInfo, peer, piece, 0, 0, NULL) == 0)
                return 0;
        }
    }
    return -1;
}
static int requestNextBlock(TorrentInfo*torrentInfo) {
    if(torrentInfo->permFlags & SIGNAL_FLAG_FINISHED_DOWNLOADING )
        return -1;

    int start;
    int end;
    int offset = torrentInfo->settings.disableRandomization ? 0 : rand();
    if(torrentInfo->settings.daemon) {
        start = torrentInfo->startingPieceIndex + (offset % torrentInfo->cacheSlots);
        end = torrentInfo->endingPieceIndex;
    } else {
        start = torrentInfo->nextPieceToOutput + offset % RANDOM_PIECE_WINDOW;
        end = torrentInfo->nextPieceToOutput + torrentInfo->cacheSlots;
    }
    start = MIN(start, torrentInfo->endingPieceIndex);
    end = MIN(end, torrentInfo->endingPieceIndex);
    int requested = 0;
    uint8_t masks[][2] = {
        {PEER_NOT_CHOKING_ME | PEER_INTERESTED_IN_ME, PEER_SEEDING_TO_ME},
        {PEER_NOT_CHOKING_ME                        , PEER_INTERESTED_IN_ME | PEER_SEEDING_TO_ME},
        {PEER_NOT_CHOKING_ME | PEER_SEEDING_TO_ME   , 0},
        {-1                                         , PEER_NOT_CHOKING_ME},
    };

    for(int n = 0; n < LEN(masks); n++) {
        for(int i = torrentInfo->numPeers - 1; i >= 0; i--) {
            Peer* peer = &torrentInfo->peers[i];
            if(!(peer->flags & INTERESTED_IN_PEER) || !(peer->flags & masks[n][0]) || (peer->flags & masks[n][1]))
                continue;
            if(peer->numOutstandingPieces == MAX_PEER_PEER_OUTSTANDING_PIECES || hasMaxOutstandingBlocks(peer)) {
                continue;
            }

            if(hasRequestPending(peer, -1))
                continue;
            if (!(peer->flags & PEER_NOT_CHOKING_ME)) {
                requested += requestFastPieceWhileChoked(torrentInfo, peer);
                continue;
            }

            if ((!(peer->flags & PEER_IS_THROTTLING_ME) && requestClaimed(torrentInfo, peer) == 0) ||
                    requestNextBlockFromPeer(torrentInfo, peer, start, end) == 0) {
                requested++;
            }
        }
    }

    return requested;
}

int handlePeerEvent(TorrentInfo* torrentInfo, int i, struct pollfd* pollfds) {
    int ret = 0;
    errno = 0;
    if(pollfds[i].revents & POLLOUT) {
        ret = handleSendingToPeer(torrentInfo, i);
        if(!canSendBlocks(&torrentInfo->peers[i])) {
            pollfds[i].events &= ~POLLOUT;
        }
        if(!ret)
            requestNextBlock(torrentInfo);
    }
    if(ret != -1 && pollfds[i].revents & POLLIN) {
        ret = handlePeerMessage(torrentInfo, i);
    }
    if((pollfds[i].events & POLLIN) && !canReceiveBlocks(&torrentInfo->peers[i])) {
        pollfds[i].events &= ~POLLIN;
    } else if(!(pollfds[i].events & POLLIN) && canReceiveBlocks(&torrentInfo->peers[i])) {
        pollfds[i].events |= POLLIN;
    }
    if(ret == -1 || pollfds[i].revents & ~(POLLOUT|POLLIN)) {
        if(ret == -1) {
            if(errno && errno != EPIPE && errno != ECONNRESET) {
                ERROR("Error processing event");
                VERBOSE("Error processing index %d event %d %d\n", i, pollfds[i].revents, errno);
                VERBOSE_PEER_MSG("Error processing event", &torrentInfo->peers[i]);
            } else {
                VERBOSE_PEER_MSG("Peer closed connection", &torrentInfo->peers[i]);
            }
        }
        ret = -1;
    }
    return ret;
}

int handleEvents(TorrentInfo* torrentInfo, int pollTimeout) {
    int trackerFdIndex = MAX_NUM_PEERS + 0;
    int listenFdIndex  = MAX_NUM_PEERS + 1;
    int outputFdIndex  = MAX_NUM_PEERS + 2;
    int inputFdIndex  = MAX_NUM_PEERS + 3;
    struct pollfd pollfds[inputFdIndex + 1];
    for(int i = 0; i < torrentInfo->numPeers; i++) {
        pollfds[i].fd = torrentInfo->peers[i].fd;
        pollfds[i].events  = canReceiveBlocks(&torrentInfo->peers[i]) ? POLLIN : 0;
        pollfds[i].events |= canSendBlocks(&torrentInfo->peers[i]) ? POLLOUT : 0;
    }
    for(int i = torrentInfo->numPeers; i < MAX_NUM_PEERS; i++) {
        pollfds[i].fd = -1;
    }

    pollfds[trackerFdIndex].fd = torrentInfo->trackerInfo.fd;
    pollfds[trackerFdIndex].events = POLLIN | (torrentInfo->trackerInfo.sending ? POLLOUT : 0);

    pollfds[listenFdIndex].fd = torrentInfo->listenFd;
    pollfds[listenFdIndex].events = POLLIN;

    pollfds[outputFdIndex].fd = torrentInfo->outputFd;
    pollfds[outputFdIndex].events = canOutputNextPiece(torrentInfo) ? POLLOUT : 0;

    pollfds[inputFdIndex].fd = torrentInfo->settings.childCmd ? STDIN_FILENO: -1;
    pollfds[inputFdIndex].events = POLLIN;

    int ret, peerRemoved = 0;
    int end = 0;
    while( !peerRemoved && !end && !GLOBAL_FLAGS && (ret = poll(pollfds, LEN(pollfds), pollTimeout))) {
        if(ret == -1) {
            if(errno == EINTR)
                continue;
            ERROR("poll");
            return -1;
        }
        for(int i = LEN(pollfds) - 1; i >= 0; i--) {
            if(!pollfds[i].revents)
                continue;
            if(i == inputFdIndex) {
                char n;
                if (read(pollfds[i].fd, &n, 1) == 0) {
                    VERBOSE("Child process finished; exiting\n");
                    GLOBAL_FLAGS |= SIGNAL_FLAG_SHUTDOWN;
                    break;
                }
            } else if(i == outputFdIndex) {
                if(pollfds[i].revents & POLLOUT)  {
                    if(canOutputNextPiece(torrentInfo)) {
                        outputNextPiece(torrentInfo);
                        if(!canOutputNextPiece(torrentInfo))
                            requestNextBlock(torrentInfo);
                        break;
                    } else {
                        TRACE("Can't write any more pieces\n");
                        pollfds[i].events = 0;
                    }
                    if(torrentInfo->nextPieceToOutput == torrentInfo->endingPieceIndex) {
                        end = 1;
                        break;
                    }
                } else {
                    VERBOSE("STDOUT has error %d\n", pollfds[i].revents);
                    GLOBAL_FLAGS |= SIGNAL_FLAG_SHUTDOWN;
                    break;
                }
            } else if(i == listenFdIndex) {
                if(pollfds[i].revents & POLLIN)  {
                    TRACE("Detected new connection\n");
                    int n = acceptPeerConnection(torrentInfo, pollfds[i].fd);
                    if(n != -1) {
                        pollfds[n].fd = torrentInfo->peers[n].fd;
                        pollfds[n].events = POLLIN | POLLOUT;
                        TRACE("Added peer %d %d\n", n, pollfds[n].events);
                    }
                } else if(pollfds[i].revents) {
                    WARN("Listen socket unexpectedly closed\n");
                    assert(!torrentInfo->pedantic);
                    close(pollfds[i].fd);
                    GLOBAL_FLAGS |= SIGNAL_FLAG_SHUTDOWN;
                    break;
                }
            } else if(i == trackerFdIndex) {
                int ret = 0;
                int finish = 0;
                if(pollfds[i].revents & POLLOUT)  {
                    if(torrentInfo->trackerInfo.buffer[0] == 0) {
                        prepareMessageToSendToTracker(torrentInfo, torrentInfo->trackerInfo.lastAnnounceTimeMS ? IN_PROGRESS : STARTED);
                    }
                    int msgLen = strlen(torrentInfo->trackerInfo.buffer);
                    int sent = torrentInfo->trackerInfo.sent;
                    ret = write(torrentInfo->trackerInfo.fd, torrentInfo->trackerInfo.buffer + sent, msgLen - sent);
                    torrentInfo->trackerInfo.sent += ret;

                    if(ret == -1) {
                        ERROR("Failed to write to tracker");
                    } else if (torrentInfo->trackerInfo.sent == msgLen) {
                        TRACE("Finished sending message to tracker\n");

                        shutdown(torrentInfo->trackerInfo.fd, SHUT_WR);
                        if(torrentInfo->trackerInfo.nextStatus == COMPLETED) {
                            finish = 1;
                        }
                        pollfds[i].events &= ~POLLOUT;
                        torrentInfo->trackerInfo.sending = 0;
                        torrentInfo->trackerInfo.read = 0;
                    }
                } else if(pollfds[i].revents & POLLIN)  {
                    ret = read(torrentInfo->trackerInfo.fd, torrentInfo->trackerInfo.buffer + torrentInfo->trackerInfo.read, sizeof(torrentInfo->trackerInfo.buffer) - torrentInfo->trackerInfo.read);
                    if(ret == -1) {
                        ERROR("Failed to read from tracker");
                    } else {
                        torrentInfo->trackerInfo.read += ret;
                        if(ret == 0 || torrentInfo->trackerInfo.read == sizeof(torrentInfo->trackerInfo.buffer)) {
                            torrentInfo->trackerInfo.buffer[torrentInfo->trackerInfo.read] = 0;
                            int addPeers = !torrentInfo->trackerInfo.lastAnnounceTimeMS || !(torrentInfo->permFlags & SIGNAL_FLAG_FINISHED_DOWNLOADING);
                            VERBOSE("Processing tracker response; add peers %d\n", addPeers );
                            int numNewPeers = processAnnounceResponse(torrentInfo, torrentInfo->trackerInfo.buffer, torrentInfo->trackerInfo.read, addPeers);
                            if(numNewPeers == -1) {
                                ret = -1;
                            } else {
                                if(numNewPeers) {
                                    peerRemoved = 1;
                                }
                                torrentInfo->trackerInfo.lastAnnounceTimeMS = getTimeMS();
                            }
                            finish = 1;
                        }
                    }
                }
                if(ret == -1 || finish) {
                    announceSucceeded(torrentInfo->metadata, &torrentInfo->trackerInfo.announceUrlIndex, ret != -1);
                    close(torrentInfo->trackerInfo.fd);
                    torrentInfo->trackerInfo.fd = -1;
                    if (ret == -1) {
                        connectToTracker(torrentInfo);
                    }
                    pollfds[i].fd = torrentInfo->trackerInfo.fd;
                }
            } else {
                if(handlePeerEvent(torrentInfo, i, pollfds) == -1) {
                    unclaimAllPiecesAndRemovePeer(torrentInfo, i);
                    peerRemoved = 1;
                } else if(torrentInfo->peers[i].currentReceiveMsg.read == 0 && (torrentInfo->peers[i].flags & COMPLETED_HANDSHAKE) == COMPLETED_HANDSHAKE) {
                    requestNextBlock(torrentInfo);
                    if(!(pollfds[outputFdIndex].events & POLLOUT) && canOutputNextPiece(torrentInfo)){
                        TRACE("Can output next piece\n");
                        pollfds[outputFdIndex].events |= POLLOUT;
                    }
                }
            }
        }

        for(int i = torrentInfo->numPeers -1; i >=0; i--) {
            if(!(pollfds[i].events & POLLOUT) && canSendBlocks(&torrentInfo->peers[i])) {
                pollfds[i].events |= POLLOUT;
            }
        }
#ifdef DEBUG
        if(getenv("NT_STEP"))
            break;
#endif
    }
    if(end)
        assert(validate(torrentInfo));
    return ret;
}

static int createSigAction(int sig, void(*func)(int, siginfo_t *, void *)) {
    struct sigaction sa;
    sa.sa_sigaction = func;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_NODEFER | SA_RESTART | SA_SIGINFO;
    return sigaction(sig, &sa, NULL);
}

void setupSignals() {
    signal(SIGPIPE, SIG_IGN);
    createSigAction(SIGALRM, timerHandler);
    createSigAction(SIGHUP,  timerHandler);
    createSigAction(SIGUSR1, timerHandler);
    createSigAction(SIGUSR2, timerHandler);
    createSigAction(SIGTERM, timerHandler);
    createSigAction(SIGINT, timerHandler);
}

static void syncCacheAndDisk(TorrentInfo* torrentInfo) {
    if(torrentInfo->cacheBitmap.data && torrentInfo->ondisk.data) {
        int pieceIndex;
        while((pieceIndex = findFirstMissingBit(&torrentInfo->ondisk, &torrentInfo->cacheBitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex)) != -1) {
            PieceMetadata* piece = getPieceMetadata(torrentInfo, pieceIndex, 0);
            assert(piece);
            if(writeToDisk(torrentInfo->settings.dirfd, torrentInfo->metadata, piece->data, pieceIndex, 0, getPieceLen(torrentInfo->metadata, pieceIndex))) {
                setBit(&torrentInfo->ondisk, pieceIndex);
            }
        }
    }
}

static int getNumUsefulPeers(TorrentInfo* torrentInfo) {
    int count = 0;
    for(int i = torrentInfo->numPeers - 1; i >= 0; i--) {
        Peer* peer = &torrentInfo->peers[i];
        count += (peer->flags & INTERESTED_IN_PEER) && (peer->flags & PEER_NOT_CHOKING_ME);
    }
    return count;
}

static void chokeAndRemovePeers(TorrentInfo* torrentInfo) {
    for(int i = torrentInfo->numPeers -1; i >=0; i--) {
        Peer* peer = &torrentInfo->peers[i];
        if(!(peer->flags & RECEIVED_HANDSHAKE)) {
            removePeer(torrentInfo, i);
            continue;
        }
        if(!isSendQueueEmpty(peer)) {
            VERBOSE_PEER_MSG("Dropping queued messages for peer", peer);
            peer->sendIndexEnd = peer->sendIndexStart;
        }
        queueMessageToSend(peer, CHOKE, NULL);
        assert(!isSendQueueEmpty(&torrentInfo->peers[i]));
    }
    while(torrentInfo->numPeers) {
        VERIFY_LOOP_IS_NOT_SPAMMY(getTimeMS());
        struct pollfd pollfds[torrentInfo->numPeers];
        for(int i = 0; i < torrentInfo->numPeers; i++) {
            pollfds[i].fd = torrentInfo->peers[i].fd;
            pollfds[i].events = POLLOUT;
        }
        if(poll(pollfds, LEN(pollfds), -1) == -1) {
            assert(errno == EAGAIN);
            continue;
        }
        for(int i = LEN(pollfds) - 1; i >= 0; i--) {
            assert(canSendBlocks(&torrentInfo->peers[i]));
            if(pollfds[i].revents && handlePeerEvent(torrentInfo, i, pollfds) == -1 ||
                    torrentInfo->peers[i].currentSendMsg.sent == 0 && isSendQueueEmpty(&torrentInfo->peers[i])) {
                unclaimAllPiecesAndRemovePeer(torrentInfo, i);
            }
        }
    }
    assert(torrentInfo->numPeers == 0);
}

void cleanupTorentInfo(TorrentInfo* torrentInfo) {
    if(torrentInfo->listenFd != -1)
        close(torrentInfo->listenFd);

    chokeAndRemovePeers(torrentInfo);
    VERBOSE("Removed all peers\n");

    if(torrentInfo->trackerInfo.fd != -1)
        close(torrentInfo->trackerInfo.fd);
}

int eventLoop(TorrentInfo* torrentInfo) {
    setupSignals();

#ifndef NDEBUG
        if(getenv("NT_SIGNAL_PARENT")) {
            assert(kill(getppid(), SIGUSR1) == 0);
        }
#endif

    syncCacheAndDisk(torrentInfo);

    int useTracker = !torrentInfo->settings.peerList || torrentInfo->settings.peerList[0] == '+';

    if(torrentInfo->trackerInfo.announceInterval == 0)
        torrentInfo->trackerInfo.announceInterval = 1;
    if(torrentInfo->trackerInfo.announceMinInterval == 0)
        torrentInfo->trackerInfo.announceMinInterval = 1;

    int times[NUM_TIMERS] = {
        [ANNOUNCE_TIMER] = torrentInfo->trackerInfo.announceInterval * 1000,
        [STATS_TIMER] = STATS_WINDOW_MS,
        [CONNECTION_TIMER] = torrentInfo->settings.keepAlive / 2
    };
    createTimers();
    armTimers(times, LEN(times));

    if(isBitMapOne(&torrentInfo->downloadedBitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex)) {
        torrentInfo->permFlags |= SIGNAL_FLAG_FINISHED_DOWNLOADING;
        VERBOSE("Had already finished downloading\n");
        if(torrentInfo->settings.daemon) {
            VERBOSE("We've already downloaded the desired range and won't output anything\n");
            GLOBAL_FLAGS |= SIGNAL_FLAG_FINISHED_OUTPUTTING | (1<<ANNOUNCE_TIMER);
        }
    }

    handleEvents(torrentInfo, 0);
    GLOBAL_FLAGS |= (1<<ANNOUNCE_TIMER);
    while (!(GLOBAL_FLAGS & SIGNAL_FLAG_SHUTDOWN)) {
        unsigned long now = getTimeMS();
        VERIFY_LOOP_IS_NOT_SPAMMY(now);

        if(GLOBAL_FLAGS & SIGNAL_FLAG_SHUTDOWN) {
            torrentInfo->permFlags |= SIGNAL_FLAG_SHUTDOWN;
        }

        if(GLOBAL_FLAGS & SIGNAL_FLAG_FINISHED_DOWNLOADING) {
            GLOBAL_FLAGS &= ~(SIGNAL_FLAG_FINISHED_DOWNLOADING);
            if (!(torrentInfo->permFlags & SIGNAL_FLAG_FINISHED_DOWNLOADING)) {
                torrentInfo->permFlags |= SIGNAL_FLAG_FINISHED_DOWNLOADING;
                // only send completed if we've downloaded everything as per spec
                if(useTracker && torrentInfo->trackerInfo.left == 0)
                    announceToTrackerQuick(torrentInfo, COMPLETED, torrentInfo->settings.quick);
            }
            if(torrentInfo->settings.quick && torrentInfo->outputFd == -1)
                break;
            if(torrentInfo->settings.daemon)
                torrentInfo->nextPieceToOutput = torrentInfo->lastPieceToOutput;
            syncChanges(torrentInfo, 0);
        }

        if(GLOBAL_FLAGS & SIGNAL_FLAG_FINISHED_OUTPUTTING) {
            GLOBAL_FLAGS &= ~(SIGNAL_FLAG_FINISHED_OUTPUTTING);
            VERBOSE("Finished outputting\n");
            torrentInfo->permFlags |= SIGNAL_FLAG_FINISHED_OUTPUTTING;
            if(torrentInfo->settings.quick &&
                    (torrentInfo->settings.dirfd == -1 && !torrentInfo->settings.cacheFilename ||
                    isBitMapOne(&torrentInfo->downloadedBitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex)))
                break;
            if(torrentInfo->outputFd != -1) {
                close(torrentInfo->outputFd);
                torrentInfo->outputFd = -1;
            }
        }

        if(torrentInfo->listenFd == -1 && listenForClients(torrentInfo) == -1) {
            INFO("Failed to listen for clients\n");
            return -1;
        }

        if(GLOBAL_FLAGS & (1<<STATS_TIMER)) {
            GLOBAL_FLAGS &= ~(1<<STATS_TIMER);

            moveToNextStatsWindow(torrentInfo);
            int downloadRate = 0;
            for(int n = 0; n < torrentInfo->numPeers; n++) {
                downloadRate += getPeerTransferRate(&torrentInfo->peers[n], DOWNLOAD);
                if (getNumOutstandingBlocks(&torrentInfo->peers[n]) && isPeerSlow(&torrentInfo->peers[n], torrentInfo->settings.blockSize)) {
                    torrentInfo->peers[n].flags |= PEER_IS_THROTTLING_ME;
                } else {
                    torrentInfo->peers[n].flags &= ~PEER_IS_THROTTLING_ME;
                }
                updateThrottleFlag(&torrentInfo->peers[n], &torrentInfo->settings);
            }

            requestNextBlock(torrentInfo);

            if (torrentInfo->trackerInfo.fd == -1 &&
                    !(torrentInfo->permFlags & SIGNAL_FLAG_FINISHED_DOWNLOADING) &&
                    now - torrentInfo->trackerInfo.lastAnnounceTimeMS >= torrentInfo->trackerInfo.announceMinInterval * 1000 &&
                    downloadRate < SLOW_DOWNLOAD_RATE) {
                VERBOSE("Download rate is slow\n");
                GLOBAL_FLAGS |= 1<<ANNOUNCE_TIMER;
            }
            dumpStats(torrentInfo);
            static int dumpStatsCounter;
            if(++dumpStatsCounter % STATS_WINDOWS == 0 && LOG_LEVEL <= LOG_LEVEL_VERBOSE) {
                GLOBAL_FLAGS |= SIGNAL_FLAG_DUMP_PEERS;
            }
        }
        if(GLOBAL_FLAGS & (SIGNAL_FLAG_DUMP_PEERS)) {
            GLOBAL_FLAGS &= ~(SIGNAL_FLAG_DUMP_PEERS);
            dumpPeers(torrentInfo);
        }

        if(torrentInfo->trackerInfo.fd == -1 && !(torrentInfo->permFlags & SIGNAL_FLAG_FINISHED_DOWNLOADING) &&
                getNumUsefulPeers(torrentInfo) == 0 &&
                now - torrentInfo->trackerInfo.lastAnnounceTimeMS >= torrentInfo->trackerInfo.announceMinInterval * 1000) {
            VERBOSE("Couldn't find any peers we are interested in that isn't choking us out of %d peers. Asking for more\n", torrentInfo->numPeers);
            GLOBAL_FLAGS |= (1<<ANNOUNCE_TIMER);
        }

        if(times[ANNOUNCE_TIMER] != torrentInfo->trackerInfo.announceInterval * 1000) {
            VERBOSE("Setting announce interval timer to %ds\n", torrentInfo->trackerInfo.announceInterval);
            times[ANNOUNCE_TIMER] = torrentInfo->trackerInfo.announceInterval * 1000;
            armTimers(times + ANNOUNCE_TIMER, 1);
        }
        if(GLOBAL_FLAGS & (1<<ANNOUNCE_TIMER)) {
            GLOBAL_FLAGS &= ~(1<<ANNOUNCE_TIMER);
            unsigned long delta = (now - torrentInfo->trackerInfo.lastAnnounceTimeMS);
            if (delta >= torrentInfo->trackerInfo.announceMinInterval * 1000) {
                if (addPeersFromPeerList(torrentInfo)) {
                    torrentInfo->trackerInfo.lastAnnounceTimeMS = now;
                } else if(torrentInfo->trackerInfo.fd == -1) {
                    VERBOSE("Announcing to tracker\n");
                    if(torrentInfo->trackerInfo.lastAnnounceTimeMS) {
                        VERBOSE("tracker; time since last announce %dms\n", delta);
                    }
                    connectToTracker(torrentInfo);
                }
            }
        }
        if(GLOBAL_FLAGS & (1<<CONNECTION_TIMER)) {
            GLOBAL_FLAGS &= ~(1<<CONNECTION_TIMER);
            for(int n = torrentInfo->numPeers - 1; n >= 0; n--) {
                Peer* peer = &torrentInfo->peers[n];
                if(now - peer->lastReceiveMessageTimeMS > torrentInfo->settings.keepAlive && peer->numOutstandingPieces) {
                    VERBOSE_PEER_MSG_WITH_ARGS("Connection from peer has been idle for %dms %d", peer, now - peer->lastReceiveMessageTimeMS, torrentInfo->settings.keepAlive);
                    unclaimAllPiecesAndRemovePeer(torrentInfo, n);

                    requestNextBlock(torrentInfo);
                    continue;
                }
                if (isSendQueueEmpty(peer))
                    queueMessageToSend(peer, KEEP_ALIVE, NULL);
                for(int i = peer->numOutstandingPieces - 1; i >= 0; i--) {
                    int pieceIndex = peer->outstandingPieces[i];
                    PieceMetadata* piece = getPieceMetadata(torrentInfo, pieceIndex, 0);
                    assert(piece);
                    assert(isSet(&torrentInfo->requested, pieceIndex) && !isSet(&torrentInfo->downloadedBitmap, pieceIndex));
                    unsigned delta = now - piece->requestTime;
                    if(piece->requestTime == -1 || delta < PIECE_TIMEOUT_MS){
                        continue;
                    }

                    VERBOSE_PEER_MSG_WITH_ARGS("Detected slow download (%dms) of %d (now: %ld vs %ld) last message time %d", peer, delta, pieceIndex, now, piece->requestTime, peer->lastReceiveMessageTimeMS);
                    ++peer->errCount;

                    assert(!torrentInfo->pedantic);
                    if(getPeerTransferRate(peer, DOWNLOAD) == 0 && (!torrentInfo->settings.daemon ||
                        torrentInfo->downloadedBitmap.numBits - popcount(&torrentInfo->downloadedBitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex) < RANDOM_PIECE_WINDOW)) {

                        unclaimAllPiecesAndRemovePeer(torrentInfo, n);
                        requestNextBlock(torrentInfo);
                        break;
                    }
                }
            }
        }

        handleEvents(torrentInfo, -1);
    }

    VERBOSE("Initiating cleanup\n");
    for(int i = 0; i < LEN(times); i++) {
        if(timer_delete(timers[i])==-1) {
            ERROR("Failed to do delete timer");
        }
    }

    if(useTracker && torrentInfo->trackerInfo.lastAnnounceTimeMS)
        announceToTrackerQuick(torrentInfo, STOPPED, 1);

    cleanupTorentInfo(torrentInfo);
    VERBOSE("Exiting event loop\n");
    return 0;
}

static void initClientId(char* dest) {
    // Azureus-style client id
    static char clientId[] = "-NT" VERSION "-";
    char buffer[13];
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    snprintf(buffer, sizeof(buffer), "%08lX%04d", now.tv_nsec, getpid() % 10000);
    memcpy(stpcpy(dest,clientId), buffer, sizeof(buffer) - 1);
    VERBOSE("client id: '%.*s'\n", CLIENT_ID_LEN, dest);
}

int initTorrentInfo(TorrentInfo* torrentInfo, const TorrentMetadata* torrentMetadata, const TorrentSettings torrentSettings) {
    initClientId(torrentInfo->clientId);
    srand(getpid());

    torrentInfo->metadata = torrentMetadata;
    assert(torrentSettings.blockSize);

    if(torrentSettings.paths && torrentSettings.paths[0]) {
        unsigned long earliestStartPos = -1, latestEndingPos = 0;
        torrentInfo->startingPieceIndex = torrentInfo->metadata->numPieces - 1;
        torrentInfo->endingPieceIndex = 0;
        for(int i = 0; torrentSettings.paths[i]; i++) {
            int startingPieceIndex, endingPieceIndex;
            unsigned long length, runningLen;
            char * p = getPieceRange(torrentMetadata, -1, torrentSettings.paths[i], &startingPieceIndex, &endingPieceIndex, &length, &runningLen);
            if(!p) {
                DIE(EXIT_INVALID_ARG, "Invalid path %s\n", torrentSettings.paths[i]);
                return -1;
            }
            torrentInfo->startingPieceIndex = MIN(torrentInfo->startingPieceIndex, startingPieceIndex);
            torrentInfo->endingPieceIndex = MAX(torrentInfo->endingPieceIndex, endingPieceIndex);
            earliestStartPos = MIN(earliestStartPos, runningLen);
            latestEndingPos = MAX(latestEndingPos, runningLen + length);
        }

        torrentInfo->offsetIntoStartingPiece = earliestStartPos % torrentInfo->metadata->pieceLength;
        torrentInfo->lenOfLastPieceToOutput  = latestEndingPos % torrentInfo->metadata->pieceLength;
        assert(torrentInfo->lenOfLastPieceToOutput <= torrentInfo->metadata->pieceLength);
        if(torrentInfo->lenOfLastPieceToOutput == 0) torrentInfo->lenOfLastPieceToOutput = torrentInfo->metadata->pieceLength;
        assert(torrentInfo->lenOfLastPieceToOutput <= torrentInfo->metadata->pieceLength);

        torrentInfo->nextPieceToOutput = torrentInfo->startingPieceIndex;
        torrentInfo->lastPieceToOutput = torrentInfo->endingPieceIndex;
        torrentInfo->startingPieceIndex = MAX(0, ((long)torrentInfo->startingPieceIndex + torrentSettings.extraPieces[0]));
        assert(torrentInfo->startingPieceIndex >= 0);
        assert(torrentInfo->startingPieceIndex < torrentInfo->endingPieceIndex);
        torrentInfo->endingPieceIndex = MIN(torrentInfo->metadata->numPieces, torrentInfo->endingPieceIndex + torrentSettings.extraPieces[1]);

    } else {
        torrentInfo->startingPieceIndex = 0;
        torrentInfo->endingPieceIndex = torrentInfo->metadata->numPieces - torrentInfo->startingPieceIndex;
        torrentInfo->offsetIntoStartingPiece = 0;
        torrentInfo->lenOfLastPieceToOutput = getPieceLen(torrentInfo->metadata, torrentInfo->metadata->numPieces - 1);
        torrentInfo->nextPieceToOutput = torrentInfo->startingPieceIndex;
        torrentInfo->lastPieceToOutput = torrentInfo->endingPieceIndex;
    }
    torrentInfo->firstPieceToOutput = torrentInfo->nextPieceToOutput;
    if (torrentInfo->settings.daemon) {
        VERBOSE("Output Range [%d, %d)\n", torrentInfo->nextPieceToOutput, torrentInfo->lastPieceToOutput);
    }
    VERBOSE("Download Range [%d, %d)\n", torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex);

    torrentInfo->settings = torrentSettings;

    allocateBuffersAndLoadBitmaps(torrentInfo);

    torrentInfo->outputFd = torrentInfo->settings.daemon ? -1 : STDOUT_FILENO;

    fcntl(STDOUT_FILENO, F_SETFL, O_NONBLOCK);

    torrentInfo->trackerInfo.left = torrentMetadata->totalLength;
    torrentInfo->trackerInfo.fd = -1;
    torrentInfo->listenFd = -1;

    for(int pieceIndex = 0; pieceIndex < torrentMetadata->numPieces; pieceIndex++) {
        if(isSet(&torrentInfo->downloadedBitmap, pieceIndex)) {
            torrentInfo->trackerInfo.left -= getPieceLen(torrentMetadata, pieceIndex);
        }
    }
    torrentInfo->extensionHandshakeMsg = allocExtensionMessage();
    return 0;
}

void uninitTorrentInfo(TorrentInfo* torrentInfo) {
    if(torrentInfo->trackerInfo.fd != -1) close(torrentInfo->trackerInfo.fd);
    if(torrentInfo->listenFd != -1) close(torrentInfo->listenFd);
    if(torrentInfo->settings.dirfd != -1) close(torrentInfo->settings.dirfd);
    free((void*)torrentInfo->extensionHandshakeMsg);
    freeBuffers(torrentInfo);
}

void version() {
    printf("Version %s\n", VERSION);
    exit(0);
}
const char* programName = "nanotorrent";

void usage(int exitCode) {
    dprintf(STDERR_FILENO,
"usage: %s [-BhlnqVv] [-d dir] [-F file] [-b blockSize] [-m cacheSize] [-P peerList ] [-p N[-M]] torrentFile [paths...]\n"
"usage: %s -c announceUrl:port [-b pieceSize] torrentFile paths...\n\n"
"-B              Don't create or use an bitmap/bitmap state files\n"
"-b              Specify the size of a piece when used with -c or specify the request blockSize\n"
"-c              Create torrent file pointing to announceUrl:port for paths and save to file\n"
"-e N            Download N extra pieces before/after the selected region but don't output them. This can be used to preload the part of the next episode\n"
"-d dir          Base directory to write files. This can be used to resume an interrupted torrent as by default nothing is saved.\n"
"-F file         Use file as a file-based cache instead of just an in memory cache.\n"
"-h | --help     Print this help message\n"
"-I              List info on the torrent file like the name and hash\n"
"-l              List paths in torrentFile\n"
"-m cacheSize   The size of the in memory cache in MiB. This option is ignored if -F is given\n"
"-n              Don't write to stdout and don't die if it is closed\n"
"-P              Comma separated list of peers to directly connect to instead of using the tracker; If the list is prepended with '+', the tracker will additionally be used\n"
"-pN[-M]         Use port number N. This can optionally be a range and the first available port [N,M] will be used\n"
"-q              Quit as soon as all the data has been outputted\n"
"-s prog         Spawn prog such that our stdout is prog's stdin\n"
"-S prog         Like -S but prog is is the foreground process\n"
"-V              Turn on verbose logging\n"
"-v | --version  Print version\n"
	,programName, programName);
    exit(exitCode);
}

enum Action {
    CREATE,
    LEECH,
    LIST,
    INFO,
};

typedef struct {
    enum Action action;
    const char* announceUrl;
} Mode;

#define VERIFY(X) do { if(!(X)) { usage(1); }} while(0)
#define GET_ARG_STR (argv[n][i + 1] ? &(argv[n++][i + 1]) : argv[(n += 2) - 1])
static const char** parseArgs(const char *argv[], TorrentSettings* torrentSettings, Mode* mode) {
    const char* dirname = NULL;
    int i, n;
    int temp;
    for(i = 1, n = 0; argv[n]; i++) {
        if(argv[n][0] != '-') {
            break;
        }
        switch(argv[n][i]) {
            case 'B':
                torrentSettings->doNotCreateBitmapFiles = 1;
                break;
            case 'b':
                torrentSettings->pieceSize = torrentSettings->blockSize = 1 << strtol(GET_ARG_STR, NULL, 0);
                break;
            case 'c':
                mode->action = CREATE;
                mode->announceUrl = GET_ARG_STR;
                torrentSettings->pieceSize = 1 << PIECE_SHIFT;
                break;
            case 'd':
                dirname = GET_ARG_STR;
                if(dirname) {
                    VERBOSE("Using dir %s\n", dirname);
                    // Create dir if it doesn't exist. We don't care if the command fails
                    mkdir(dirname, 0777);
                    if((torrentSettings->dirfd = open(dirname, O_DIRECTORY)) == -1) {
                        FATAL_ERROR("failed to open dir");
                    }
                }
                break;
            case 'e':
                temp = strtol(GET_ARG_STR, NULL, 0);
                torrentSettings->extraPieces[temp > 0] = temp;
                break;
            case 'F':
                torrentSettings->cacheFilename = GET_ARG_STR;
                break;
            case 'h':
                usage(0);
            case 'I':
                mode->action = INFO;
                break;
            case 'l':
                mode->action = LIST;
                break;
            case 'm':
                torrentSettings->cacheSize = strtol(GET_ARG_STR, NULL, 0);
                VERIFY(torrentSettings->cacheSize);
                break;
            case 'n':
                torrentSettings->daemon = 1;
                break;
            case 't':
                torrentSettings->keepAlive = strtol(GET_ARG_STR, NULL, 0);
                break;
            case 'P':
                torrentSettings->peerList = GET_ARG_STR;
                break;
            case 'p':
                {
                    char* temp = NULL;
                    torrentSettings->maxPort = torrentSettings->minPort = strtol(GET_ARG_STR, &temp, 0);
                    VERIFY(torrentSettings->minPort || temp);
                    if(temp && temp[0] == '-')
                        torrentSettings->maxPort = strtol(temp + 1, &temp, 0);
                }
                break;
            case 'q':
                torrentSettings->quick = 1;
                break;
            case 'R':
                torrentSettings->disableRandomization = 1;
                break;
            case 'S':
                torrentSettings->forkToBackground = 1;
            case 's':
                torrentSettings->childCmd = GET_ARG_STR;
                break;
            case 'U':
                torrentSettings->maxUploadRate = strtol(GET_ARG_STR, NULL, 0);
                break;
            case 'v':
                version();
            case 'V':
                LOG_LEVEL = LOG_LEVEL > LOG_LEVEL_VERBOSE ? LOG_LEVEL_VERBOSE : MIN(0, LOG_LEVEL - 1);
                break;
            case '-':
                if (argv[n][i + 1] == 0) {
                    return argv + n;
                }
                if (strcmp(argv[n], "--help") == 0)
                    usage(0);
                else if (strcmp(argv[n], "--version") == 0)
                    version();
            default:
                usage(EXIT_INVALID_ARG);
                break;
        }
        if(n) {
            argv += n;
            n = i = 0;
        } else if(!argv[n][i + 1]) {
            n++;
            i = 0;
        }
    }
    return argv + n;
}

static int spawn(int forkToBackground, const char* cmd) {
    int fds[2];
    int sentinelFds[2];
    if(pipe(fds) || pipe(sentinelFds))
        FATAL_ERROR("pipe failed");

    VERBOSE("Forking and spawning '%s' fork to background %d\n", cmd, forkToBackground);

    pid_t pid = fork();
    if(pid == -1)
        FATAL_ERROR("fork failed");
    if(!forkToBackground == !pid) {
        dup2(fds[0], STDIN_FILENO);
        close(fds[0]);
        close(fds[1]);
        close(sentinelFds[0]);
        execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);
        FATAL_ERROR(EXIT_SYS_CALL);
    } else {
        dup2(sentinelFds[0], STDIN_FILENO);
        close(sentinelFds[0]);
        close(sentinelFds[1]);
        dup2(fds[1], STDOUT_FILENO);
        close(fds[0]);
        close(fds[1]);
    }
    return STDIN_FILENO;
}

int parseArgsAndRun(const char **argv) {
    Mode mode = {.action = LEECH};
    TorrentSettings torrentSettings = {.dirfd = -1, .keepAlive = DEFAULT_CONNECTION_TIMEOUT_MS, .blockSize = 1 << DEFAULT_BLOCK_SHIFT, .extensions = EXTENSION_BYTES };
    programName = argv[0];
    argv++;
    argv = parseArgs(argv, &torrentSettings, &mode);
    const char* torrentFileName = argv[0];
    VERIFY(torrentFileName);
    VERBOSE("Using torrentFile: '%s'\n", torrentFileName);
    if(mode.action == CREATE) {
        VERIFY(mode.announceUrl);
        TRACE("Using announceUrl: '%s'\n", mode.announceUrl);
        int fd = open(torrentFileName, O_WRONLY | O_CREAT, 0666);
        if(fd == -1) {
            FATAL_ERROR("failed to open file");
            return -1;
        }
        createTorrentFile(fd, mode.announceUrl, argv[1], argv + 1, -1, torrentSettings.pieceSize);
        close(fd);
        return 0;
    }
    TorrentMetadata torrentMetadataInfo = {0};
    if(openAndParseTorrentFile(&torrentMetadataInfo, torrentFileName) == -1)
        DIE(EXIT_MALFORMED, "Could not parse torrent file");

    TorrentInfo torrentInfo = {0};
    torrentSettings.paths = argv + 1;
    if(mode.action == LIST) {
        unsigned long length;
        for(int i = 0; i < torrentMetadataInfo.fileCount; i++) {
            char* path = getPieceRange(&torrentMetadataInfo, i, NULL, NULL, NULL, &length, NULL);
            printf("%s\n", path);
        }
    } else if(mode.action == INFO) {
        dumpTorrentInfo(&torrentMetadataInfo);
    } else {

        if(!torrentInfo.settings.daemon && !torrentSettings.disableRandomization && torrentMetadataInfo.pieceLength > 1 << (PIECE_SHIFT + 2)) {
            VERBOSE("Disabling piece randomization since piece length is large: %d\n", torrentMetadataInfo.pieceLength);
            torrentSettings.disableRandomization = 1;
        }
        if(torrentSettings.childCmd) {
            spawn(torrentSettings.forkToBackground, torrentSettings.childCmd);
        }
#ifndef NDEBUG
        if(getenv("NT_PEDANTIC")) {
            torrentInfo.pedantic = atoi(getenv("NT_PEDANTIC"));
        }
#endif
        int ret = initTorrentInfo(&torrentInfo, &torrentMetadataInfo, torrentSettings);
        if(ret == 0) {
            ret = eventLoop(&torrentInfo);
            uninitTorrentInfo(&torrentInfo);
        }
        return ret;
    }
    return 0;
}

#ifndef NDEBUG
__attribute__((weak))
#endif
int main (int argc, const char *argv[])  {
    return parseArgsAndRun(argv);
}
