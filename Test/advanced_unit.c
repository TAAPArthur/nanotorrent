#include "../util.h"

#include "tester.h"

#include "scutest.h"

#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>


SCUTEST_SET_FIXTURE(NULL, NULL);

SCUTEST(test_peer_connection_detect_duplicates) {
    TorrentMetadata metadata = {};
    TorrentInfo torrentInfo = {0};
    TorrentInfo peer = {0};
    const char* cacheFile;
    createSimpleTorrentMetadataWithCacheFile(&metadata, &cacheFile);
    initTorrentInfo(&torrentInfo, &metadata, getDefaultLeecherSettings(NULL));
    initTorrentInfo(&peer, &metadata, getDefaultSeederSettings(cacheFile));

    assert(listenForClients(&peer) != -1);
    assert(listenForClients(&torrentInfo) != -1);

    int addPeerAndConnect(TorrentInfo* torrentInfo,int ip, short port);
    char ip[] = {127, 0, 0, 1};
    addPeerAndConnect(&torrentInfo, *(int*)ip, peer.port);
    addPeerAndConnect(&peer, *(int*)ip, torrentInfo.port);

    while(torrentInfo.nextPieceToOutput != torrentInfo.endingPieceIndex) {
        GLOBAL_FLAGS = 0;
        handleEvents(&torrentInfo, 0);
        handleEvents(&peer, 0);
    }
    assert(torrentInfo.numPeers == 1);
    assert(peer.numPeers == 1);

    addPeerAndConnect(&torrentInfo, *(int*)ip, peer.port);
    assert(torrentInfo.numPeers == 1);
    addPeerAndConnect(&peer, *(int*)ip, torrentInfo.port);
    assert(peer.numPeers == 1);

    signal(SIGPIPE, SIG_IGN);
    cleanupTorentInfo(&torrentInfo);
    cleanupTorentInfo(&peer);
}

SCUTEST_SET_FIXTURE(setupFakeHTTPServer, cleanupFakeHTTPServer);

static const char * announceLists[] = {
    test_annouce_url " " test_annouce_url,
    test_annouce_url "_bad " test_annouce_url,
    test_annouce_url "_bad\n" test_annouce_url,
    CONCAT("udp://localhost:", TEST_PORT) "/announce " test_annouce_url,
    CONCAT("ftp://localhost:", TEST_PORT) "/announce " test_annouce_url,
    "bd_url " test_annouce_url,
};
SCUTEST(test_announce_list_multi_protocol, .iter = LEN(announceLists)) {
    const char * announceList = announceLists[_i];
    const char* torrentName;
    int fd = createTestTorrentWithUrl(&torrentName, announceList);
    TorrentMetadata torrentMetadata = {0};
    assert(readAndParseTorrentFile(&torrentMetadata, fd) != -1);
    close(fd);
    TorrentSettings settings[] = {getDefaultLeecherSettings(NULL), getDefaultLeecherSettings(NULL)};
    TorrentInfo torrentInfos[2] = {};

    initAndConnect(&torrentMetadata, LEN(settings), torrentInfos, settings);
    cleanupTorrentInfos(LEN(torrentInfos), torrentInfos);
}

static void spawnPerFilePeers(const char* torrent_file, int num_files, const char**names, const char**dirs, int*children) {
    for(int i = 0; i < num_files; i++) {
        if(!(children[i] = fork())) {
            const char *argv[] = {"unit_test", "-n", "-d", dirs[i], torrent_file, names ?  names[i] : NULL, NULL};
            assert(parseArgsAndRun(argv) == 0);
            exit(0);
        }
        assert(children[i] != -1);
    }
}

#define SET_PIECE_SIZE_ARG(A, B) {CONCAT(A, B), B}
struct {
    char* arg;
    int value;
} piece_size_args[] = {
    SET_PIECE_SIZE_ARG("-b", PIECE_SHIFT),
    SET_PIECE_SIZE_ARG("-b", 21),
    SET_PIECE_SIZE_ARG("-b", DEFAULT_BLOCK_SHIFT),
    SET_PIECE_SIZE_ARG("-b", MAX_BLOCK_SHIFT)
};

SCUTEST(full_workflow, .iter = 4 * LEN(piece_size_args)) {
    int num_files = _i % 4;
    if(num_files == 0)
        num_files = 1;
    const char* names[num_files];
    int piece_size = piece_size_args[_i / 4].value;
    const char* piece_size_arg = piece_size_args[_i / 4].arg;
    createFiles((_i % 4), names, piece_size / 2);
    if(_i % 4 == 3) {
        truncate(names[0], piece_size);
    }

    const char* torrent_file;
    close(createTempFile(&torrent_file));
    {
        const char *argv[16] = {"unit_test", "-c", test_annouce_url, piece_size_arg, torrent_file, NULL};
        for(int i = 5, n= 0; n < num_files; i++, n++)
            argv[i] = names[n];

        int ret = parseArgsAndRun(argv);
        assert(ret == 0);
    }

    const char* cacheFile;
    combineFiles(num_files, names, &cacheFile);

    int children[num_files + 1];
    if(!(children[num_files] = fork())) {
        const char *argv[] = {"unit_test", "-n", "-F", cacheFile, torrent_file, NULL};
        assert(parseArgsAndRun(argv) == 0);
        exit(0);
    }

    const char* dirs[num_files];
    for(int i = 0; i < num_files; i++) {
        dirs[i] = createTempDir();
        const char *argv[] = {"unit_test", "-q", "-d", dirs[i], torrent_file, names[i], NULL};
        assert(parseArgsAndRun(argv) == 0);
    }
    killAndWaitForSuccess(1, &children[num_files]);

    spawnPerFilePeers(torrent_file, num_files, names, dirs, children);

    const char *argv[] = {"unit_test", "-q", "-d", createTempDir(), torrent_file, NULL};
    assert(parseArgsAndRun(argv) == 0);

    killAndWaitForSuccess(num_files, children);

    assert(parseArgsAndRun(argv) == 0);
}

SCUTEST(test_already_completed, .iter = 3) {
    int num_files = _i ? _i : 1;

    const char* torrent_file;
    const char* names[num_files];
    pid_t pid = setupPeerSeeder(_i ? num_files : 0, &torrent_file, names, BLOCK_SIZE);

    for(int i = 0; i < num_files; i++) {
        const char *argv[] = {"unit_test", "-q", "-d", createTempDir(), torrent_file, names[i], NULL};
        assert(parseArgsAndRun(argv) == 0);
        assert(parseArgsAndRun(argv) == 0);
    }

    kill(pid, SIGINT);
    waitForChildSuccess(pid);
}

SCUTEST(test_prefetch, .iter = 2) {
    int num_files = 2;

    const char* torrent_file;
    const char* names[num_files];
    pid_t pid = setupPeerSeeder(num_files, &torrent_file, names, PIECE_SIZE);

    const char* cacheFile;
    close(createTempFile(&cacheFile));
    const char* arg = _i ? "-e-1" : "-e2";

    const char* output;
    int fd = createTempFile(&output);
    dup2(fd, STDOUT_FILENO);
    close(fd);
    const char *argv[] = {"unit_test", "-q", arg, "-F", cacheFile, torrent_file, names[_i], NULL};
    waitForChildSuccess(forkAndRun(argv));
    diffFilesByName(output, names[_i], 0);
    killAndWaitForSuccess(1, &pid);

    fd = createTempFile(&output);
    assert(dup2(fd, STDOUT_FILENO) != -1);
    close(fd);
    const char *argv2[] = {"unit_test", "-q", arg, "-F", cacheFile, torrent_file, names[(_i + 1)%2], NULL};
    waitForChildSuccess(forkAndRun(argv2));

    lseek(STDOUT_FILENO, 0, SEEK_SET);
    int fd2 = open(names[(_i + 1)%2], O_RDONLY);
    diffFiles(STDOUT_FILENO, fd2, 0);
    close(fd2);
}

SCUTEST(test_validate_on_disk_bitmap, .iter = 4) {
    int num_files = 3;
    int useDir = _i % 2;

    const char* tempFile;
    if(useDir) {
        tempFile = createTempDir();
    } else {
        close(createTempFile(&tempFile));
    }

    const char* names[2][num_files];
    const char* torrent_file[2];
    pid_t pids[] = {
        setupPeerSeeder(num_files, &torrent_file[0], names[0], PIECE_SIZE / 4),
        setupPeerSeeder(num_files, &torrent_file[1], names[1], PIECE_SIZE / 4),
    };
    if( _i < 2) {
        for (int n = 0; n < LEN(pids); n++) {
            for(int i = 0; i < num_files; i++) {
                const char *argv[] = {"unit_test", "-q", useDir ? "-d" : "-F", tempFile, torrent_file[n], names[n][i], NULL};
                assert(parseArgsAndRun(argv) == 0);
                assert(parseArgsAndRun(argv) == 0);
            }
        }
    } else {
        for(int i = 0; i < num_files; i++) {
            for (int n = 0; n < LEN(pids); n++) {
                const char *argv[] = {"unit_test", "-q", useDir ? "-d" : "-F", tempFile, torrent_file[n], names[n][i], NULL};
                assert(parseArgsAndRun(argv) == 0);
                assert(parseArgsAndRun(argv) == 0);
            }
        }
    }
    killAndWaitForSuccess(LEN(pids), pids);
}

SCUTEST(test_reuse_downloaded_piece, .iter = 2) {
    int num_files = 3;
    int useDir = _i % 2;
    const char* tempFile;
    if(useDir) {
        tempFile = createTempDir();
    } else {
        close(createTempFile(&tempFile));
    }
    const char* names[num_files];
    const char* torrent_file;
    pid_t pid = setupPeerSeeder(num_files, &torrent_file, names, 1);

    for(int i = 0; i < num_files + 1; i++) {
        const char *argv[] = {"unit_test", "-q", useDir ? "-d" : "-F", tempFile, torrent_file, names[i % num_files], NULL};
        assert(parseArgsAndRun(argv) == 0);
        if(i == 0)
            killAndWaitForSuccess(1, &pid);
    }
}

SCUTEST(test_play, .iter = 6) {
    int num_files = 4;
    const char* tempFile;
    close(createTempFile(&tempFile));
    char bitmapFile[255];
    snprintf(bitmapFile, LEN(bitmapFile), "%s.cacheBitmap", tempFile);
    const char* names[num_files];
    const char* torrent_file;
    int startingSize = PIECE_SIZE * 4 / 3;

    pid_t pid = setupPeerSeeder(num_files, &torrent_file, names, startingSize);

    const char* output;
    for(int i = 0; i < num_files * (1+_i%2); i++) {
        int fd = createTempFile(&output);
        dup2(fd, STDOUT_FILENO);
        close(fd);
        const char *argv[] = {"unit_test", "-q", "-F", tempFile, torrent_file, names[i % num_files], NULL};
        assert(parseArgsAndRun(argv) == 0);
        int size = startingSize << (i% num_files);
        diffFilesByName(output, names[i % num_files], size);

        switch(_i / 2) {
            case 1:
                assert(unlink(tempFile) == 0);
                break;
            case 2:
                assert(unlink(bitmapFile) == 0);
                break;
        }
    }
    killAndWaitForSuccess(1, &pid);
}

SCUTEST(test_many_paths, .iter=3) {
    int num_files = 4;
    const char* names[num_files];
    const char* torrent_file;
    int startingSize = PIECE_SIZE / 4;

    pid_t pid = setupPeerSeeder(num_files, &torrent_file, names, startingSize);

    const char* output;
    int fd = createTempFile(&output);
    dup2(fd, STDOUT_FILENO);
    close(fd);

    switch(_i){
        case 0:
            assert(parseArgsAndRun((const char*[]){"unit_test", "-q", torrent_file, names[0], names[1], names[2], names[3], NULL}) == 0);
            break;
        case 1:
            assert(parseArgsAndRun((const char*[]){"unit_test", "-q", torrent_file, names[0], names[num_files - 1], NULL}) == 0);
            break;
        case 2:
            assert(parseArgsAndRun((const char*[]){"unit_test", "-q", torrent_file, "/0", "/3", NULL}) == 0);
            break;
    }

    const char* cacheFile;
    combineFiles(num_files, names, &cacheFile);
    diffFilesByName(output, cacheFile, 0);
    killAndWaitForSuccess(1, &pid);
}

SCUTEST(test_many_peers, .iter = 1) {
    int num_files = 3;
    int num_peers = MAX_NUM_PEERS - 1;
    const char* torrent_file;
    const char* names[num_files];
    int children[num_peers];
    pid_t pid = setupPeerSeeder(num_files, &torrent_file, names, 1);

    for(int i = 0; i < num_peers; i++) {
        const char *argv[] = {"unit_test", "-q", "-p6882-6981", torrent_file, names[i % num_files], NULL};
        children[i] = forkAndRun(argv);
    }
    waitForSuccesses(LEN(children), children);

    killAndWaitForSuccess(1, &pid);
}

SCUTEST(test_dead_peer, .iter = 2) {
    TorrentMetadata torrentMetadata = {};
    const char* cacheFile;
    const char* torrent_file = createTorrentMetadataWithCacheFile(&torrentMetadata, PIECE_SIZE * 2, 2, &cacheFile);

    const char *argv[] = {"unit_test", "-t200", "-q", torrent_file, NULL};
    pid_t pid = forkAndRun(argv);

    pid_t pid2 = -1;
    if(_i || !(pid2 = fork())) {
        TorrentInfo seeder = {0};
        initTorrentInfo(&seeder, &torrentMetadata, getDefaultSeederSettings(cacheFile));
        listenForClientsAndAnnounceToTracker(&seeder);
        assert(seeder.trackerInfo.uploaded == 0);
        while(seeder.trackerInfo.uploaded <= PIECE_SIZE - BLOCK_SIZE) {
            handleEvents(&seeder, 0);
        }
        cleanupTorentInfo(&seeder);
        if(pid2 != -1)
            exit(0);
    }
    if(pid2 != -1) {
        waitForChildSuccess(pid2);
    }

    const char *args[] = {"unit_test", "-n", "-p6885", "-F", cacheFile, torrent_file, NULL};
    pid_t pid3 = forkAndRun(args);
    waitForChildSuccess(pid);
    killAndWaitForSuccess(1, &pid3);
}

SCUTEST(test_no_double_write, .iter = 2) {
    int num_files = _i ? _i : 1;

    const char* torrent_file;
    const char* names[num_files];
    close(createTestTorrentWithFiles(num_files, &torrent_file, names, BLOCK_SIZE));

    const char* cacheFile;
    combineFiles(num_files, names, &cacheFile);

    const char* dirname = createTempDir();
    const char *args[] = {"unit_test", "-q", "-d", dirname, "-F", cacheFile, torrent_file, NULL};
    assert(parseArgsAndRun(args) == 0);

    for(int i = 0; i < num_files; i++) {
        char buffer[128] = {0};
        strcpy(buffer, dirname);
        strcat(buffer, names[i]);
        assert(chmod(buffer, 0400) != -1);
        const char *argv[] = {"unit_test", "-q", "-d", dirname, torrent_file, names[i], NULL};
        assert(parseArgsAndRun(argv) == 0);
    }

    const char *args_all[] = {"unit_test", "-V", "-q", "-d", dirname, torrent_file, NULL};
    assert(parseArgsAndRun(args_all) == 0);
}

SCUTEST(test_multi_path, .iter = 2) {
    int num_files = 4;
    const char* torrent_file;
    const char* names[num_files];
    int children[num_files];
    pid_t pid = setupPeerSeeder(num_files, &torrent_file, names, BLOCK_SIZE);

    const char* dirs[num_files];
    for(int i = 0; i < num_files; i++) {
        dirs[i] = createTempDir();
        const char *argv[] = {"unit_test", "-q", "-d", dirs[i], torrent_file, names[i], names[(i + num_files / 2) % num_files], NULL};
        assert(parseArgsAndRun(argv) == 0);
    }

    killAndWaitForSuccess(1, &pid);

    for(int offset = 0; offset < num_files; offset += num_files / 2) {
        spawnPerFilePeers(torrent_file, num_files - offset, _i ? names + offset : NULL, dirs + offset, children);
        const char *argv[] = {"unit_test", "-q", "-d", createTempDir(), torrent_file, NULL};
        assert(parseArgsAndRun(argv) == 0);
        killAndWaitForSuccess(num_files - offset, children);
    }
}

SCUTEST(test_split) {
    int num_files = 4;
    const char* torrent_file;
    const char* names[num_files];
    close(createTestTorrentWithFiles(num_files, &torrent_file, names, BLOCK_SIZE));

    const char* cacheFile;
    combineFiles(num_files, names, &cacheFile);

    const char* dirname = createTempDir();
    const char *split_args[] = {"unit_test", "-q", "-d", dirname, "-F", cacheFile, torrent_file, NULL};
    assert(parseArgsAndRun(split_args) == 0);

    const char *args[] = {"unit_test", "-q", "-d", dirname, torrent_file, NULL};
    assert(parseArgsAndRun(args) == 0);
}

static const char* getNewTempFile() {
    const char* name;
    close(createTempFile(&name));
    return name;
}
const char* flags[] = {"-q", "-qn"};
SCUTEST(test_download, .iter=LEN(flags)) {
    int num_files = 2;
    const char* torrent_file;
    const char* names[num_files];

    pid_t pid = setupPeerSeeder(num_files, &torrent_file, names, PIECE_SIZE);

    const char* cacheFile;
    combineFiles(num_files, names, &cacheFile);

    const char* dirname = createTempDir();
    const char *args[][16] = {
        {"unit_test", flags[_i], torrent_file, NULL}, // default
        {"unit_test", flags[_i], "-m1048576", torrent_file, NULL}, //small cache
        {"unit_test", flags[_i], "-F", getNewTempFile(), torrent_file, NULL}, // empty cache file
        {"unit_test", flags[_i], "-F", cacheFile, torrent_file, NULL}, // complete cache file
        {"unit_test", flags[_i], "-F", cacheFile, torrent_file, NULL}, // complete cache file
        {"unit_test", flags[_i], "-F", cacheFile, "-d", dirname, torrent_file, NULL}, // complete cache file and empty dir
        {"unit_test", flags[_i], "-F", cacheFile, "-d", dirname, torrent_file, NULL}, // complete cache file and complete dir
        {"unit_test", flags[_i], "-d", createTempDir(), torrent_file, NULL}, // directory
        {"unit_test", flags[_i], "-d", createTempDir(), "-m1048576", torrent_file, NULL}, // directory small cache
        {"unit_test", flags[_i], "-F", getNewTempFile(), "-d", createTempDir(), torrent_file, NULL}, // directory with empty cache file
        {"unit_test", flags[_i], "-F", cacheFile, "-d", createTempDir(), torrent_file, NULL}, // directory with complete cache file
    };

    for(int i = 0; i < LEN(args); i++) {
        assert(parseArgsAndRun(args[i]) == 0);
    }
    killAndWaitForSuccess(1, &pid);
}

SCUTEST(test_swarm, .iter=4) {
    int children[5];
    int size = 32 << PIECE_SHIFT;
    const char* blockSizeArgs = _i % 2 ? CONCAT("-b", MAX_BLOCK_SHIFT) : CONCAT("-b", DEFAULT_BLOCK_SHIFT);

    TorrentMetadata torrentMetadata = {};
    const char* cacheFile;
    const char* torrent_file = createTorrentMetadataWithCacheFile(&torrentMetadata, size, 1, &cacheFile);

    // TODO verify we excessively making the seeder reupload data
    children[0] = spawnTorrentClientFromMetadata(&torrentMetadata, getDefaultSeederSettings(cacheFile), NULL);

    for(int i = 1; i < LEN(children); i++) {
        const char *args[] = {"unit_test", "-B", blockSizeArgs, _i / 2 ? "-q" : "-qn", torrent_file, NULL};
        children[i] = forkAndRun(args);
    }
    waitForSuccesses(LEN(children) - 1, children + 1);
    killAndWaitForSuccess(1, children);
}

// Error handling and edge cases
SCUTEST(test_receive_invalid_data) {
    int num_files = 10;
    const char* torrent_file;
    const char* names[num_files];
    int size = 2 * MIN_BLOCK_SIZE;

    close(createTestTorrentWithFilesWithPieceSize(num_files, &torrent_file, names, -size, 2 * MIN_BLOCK_SIZE));

    const char* cacheFile;
    combineFiles(num_files, names, &cacheFile);
    const char* cacheFile2;
    combineFiles(num_files, names, &cacheFile2);

    // Cause bitmap file to be created
    assert(parseArgsAndRun((const char*[]){"unit_test", "-F", cacheFile2, "-qn", torrent_file, NULL}) == 0);
    int fd = open(cacheFile2, O_RDWR);
    ftruncate(fd, num_files * size / 2); // we only sanity check the first few pieces for validity
    ftruncate(fd, num_files * size);
    close(fd);
    pid_t pids[] = {
        startPeerSeederWithCacheFile(torrent_file, cacheFile),
        startPeerSeederWithCacheFile(torrent_file, cacheFile2),
    };

    disablePedantic();
    const char *args[] = {"unit_test", "-qn", torrent_file, NULL};
    assert(parseArgsAndRun(args) == 0);

    killAndWaitForSuccess(LEN(pids), pids);
}

SCUTEST(test_peer_connection_slow, .iter=2) {
    int size = 4 << PIECE_SHIFT;

    TorrentMetadata torrentMetadata = {};
    const char* cacheFile;
    const char* torrent_file = createTorrentMetadataWithCacheFile(&torrentMetadata, size, 1, &cacheFile);

    TorrentInfo torrentInfo = {};
    TorrentSettings settings = getDefaultLeecherSettings(NULL);
    settings.cacheSize = size;
    initTorrentInfo(&torrentInfo, &torrentMetadata, settings);
    listenForClientsAndAnnounceToTracker(&torrentInfo);

    int tempFd = createTempFile(NULL);
    dup2(tempFd, STDOUT_FILENO);
    close(tempFd);
    pid_t slowPeerPid;
    for(int i = 0; i <=_i; i++) {
        if(!(slowPeerPid = fork())) {
            const char *already_complete_args[] = {"unit_test", "-B", "-U32768", "-n", "-F", cacheFile, torrent_file, NULL};
            assert(parseArgsAndRun(already_complete_args) == 0);
            exit(0);
        }
        setenv("NT_STEP", "1", 1);
        while(getNumOutstandingBlocks(&torrentInfo.peers[0]) < 1) {
            GLOBAL_FLAGS = 0;
            handleEvents(&torrentInfo, 0);
        }
        unsetenv("NT_STEP");
        torrentInfo.peers[0].statBucket = STATS_WINDOWS;
        assert(getNumOutstandingBlocks(&torrentInfo.peers[0]) == 1);
        if(_i && i == 0) {
            signal(SIGPIPE, SIG_IGN);
            killAndWaitForSuccess(1, &slowPeerPid);

            while(torrentInfo.numPeers) {
                GLOBAL_FLAGS = 0;
                handleEvents(&torrentInfo, 0);
            }
        }
    }

    pid_t pid = startPeerSeederWithCacheFile(torrent_file, cacheFile);

    while(torrentInfo.numPeers == 1) {
        GLOBAL_FLAGS = 0;
        handleEvents(&torrentInfo, 0);
    }
    torrentInfo.peers[1].stats[DOWNLOAD].transferCounts[1] = GOOD_DOWNLOAD_RATE * STATS_WINDOWS;
    torrentInfo.peers[1].statBucket = STATS_WINDOWS;

    assert(eventLoop(&torrentInfo) == 0);

    assert(torrentInfo.peers[0].stats[DOWNLOAD].transferCounts[0] > 0);
    assert(torrentInfo.peers[1].stats[DOWNLOAD].transferCounts[0] < size);

    killAndWaitForSuccess(1, &pid);
    killAndWaitForSuccess(1, &slowPeerPid);
}

SCUTEST(test_many_slow_peers) {
    int num_files = 1;
    const char* torrent_file;
    const char* names[num_files];
    int fd = createTestTorrentWithFiles(num_files, &torrent_file, names, PIECE_SIZE * 8);
    close(fd);
    const char* cacheFile;
    combineFiles(num_files, names, &cacheFile);
    assert(chmod(cacheFile, 0400) == 0);

    disablePedanticWarnings();
    pid_t slowPeers[4];
    for(int i = 0; i < LEN(slowPeers); i++) {
        if(!(slowPeers[i] = fork())) {
            const char *already_complete_args[] = {"unit_test", "-U1", "-B", "-n", "-F", cacheFile, torrent_file, NULL};
            assert(parseArgsAndRun(already_complete_args) == 0);
            exit(0);
        }
    }
    pid_t pid = startPeerSeederWithCacheFile(torrent_file, cacheFile);
    assert(parseArgsAndRun((const char*[]){"unit_test", "-qn", torrent_file, NULL}) == 0);
    killAndWaitForSuccess(1, &pid);
    killAndWaitForSuccess(LEN(slowPeers), slowPeers);
}

SCUTEST(test_steal_piece, .iter = 2) {
    TorrentMetadata torrentMetadata = {};
    const char* cacheFile;
    const char* torrent_file = createTorrentMetadataWithCacheFile(&torrentMetadata, PIECE_SIZE * 2, 2, &cacheFile);

    const char *args[] = {"unit_test", "-B", _i ? "-q" : "-qn", torrent_file, NULL};
    pid_t downloader = forkAndRun(args);

    TorrentInfo seeder = {};
    initTorrentInfo(&seeder, &torrentMetadata, getDefaultSeederSettings(cacheFile));
    listenForClientsAndAnnounceToTracker(&seeder);
    setenv("NT_STEP", "1", 1);
    while (seeder.peers[0].stats[UPLOAD].transferCounts[0] == 0) {
        GLOBAL_FLAGS = 0;
        handleEvents(&seeder, -1);
    }
    unsetenv("NT_STEP");
    pid_t pid = startPeerSeederWithCacheFile(torrent_file, cacheFile);
    waitForChildSuccess(downloader);
    killAndWaitForSuccess(1, &pid);
    cleanupTorentInfo(&seeder);
}

SCUTEST(test_large_file, .iter = 3, .timeout=20) {
    int num_files = 1 << 8;
    int fileSize = 1 << 24;
    int pieceSize = 1 << 20;

    const char* torrent_file = NULL;
    const char* names[num_files];
    const char* ref_name;
    int fd = createTempFile(&ref_name);
    ftruncate(fd, fileSize);
    close(fd);
    const char* test_dir = createTempDir();

    fd = createTempFile(&torrent_file);
    assert(chdir(test_dir) != -1);

    for(int i = 0; i < num_files; i++) {
        char nameBuffer[16];
        snprintf(nameBuffer, 255, "%04d.file", i);
        names[i] = strdup(nameBuffer);
        int ret = link(ref_name, names[i]);
        assert(ret != -1);
    }

    int ret = createTorrentFile(fd, test_annouce_url, names[0], names, num_files, pieceSize);
    assert(ret != -1);
    close(fd);
    int type = _i;

    if (type == 0) {
        const char *argv[] = {"unit_test", "-Vqn", "-d", test_dir, torrent_file, NULL};
        assert(parseArgsAndRun(argv) == 0);

        const char *argv2[] = {"unit_test", "-qn", "-d", test_dir, torrent_file, names[0], NULL};
        assert(parseArgsAndRun(argv2) == 0);

        const char *argv3[] = {"unit_test", "-qn", "-d", test_dir, torrent_file, names[num_files - 1], NULL};
        assert(parseArgsAndRun(argv3) == 0);
    } else if (type == 1) {

        const char* cacheFile;
        fd = createTempFile(&cacheFile);
        assert(fd != -1);
        close(fd);

        const char *argv4[] = {"unit_test", "-qn", "-F", cacheFile, "-d", test_dir, torrent_file, names[num_files - 1], NULL};
        assert(parseArgsAndRun(argv4) == 0);
    } else {
        pid_t pid = fork();
        if (!pid) {
            const char *argv[] = {"unit_test", "-n", "-d", test_dir, torrent_file, NULL};
            assert(parseArgsAndRun(argv) == 0);
            exit(0);
        }

        const char *argv[] = {"unit_test", "-Vnq", torrent_file, names[num_files - 1], NULL};
        assert(parseArgsAndRun(argv) == 0);

        killAndWaitForSuccess(1, &pid);
    }

}
