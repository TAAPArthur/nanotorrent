#include "../util.h"

#include "tester.h"

#include "scutest.h"

SCUTEST(test_list_files, .iter=3) {
    int num_files = _i ? _i : 1;
    const char* names[num_files];
    const char* torrent_file;
    close(createTestTorrentWithFiles(_i, &torrent_file, names, 0));

    int fds[2];
    pipe(fds);
    if(!fork()) {
        dup2(fds[1], STDOUT_FILENO);
        close(fds[0]);
        const char *argv[] = {"unit_test", "-l", torrent_file, NULL};
        assert(parseArgsAndRun(argv) == 0);
        exit(0);
    }
    close(fds[1]);
    char buffer[1024];
    safeRead(fds[0], buffer, sizeof(buffer));
    close(fds[0]);
    char*bp = buffer;
    for(int i = 0; i < num_files; i++) {
        int len = strlen(names[i] + 1);
        assert(memcmp(bp, names[i] + 1, len) == 0);
        bp = strchr(bp, '\n') + 1;
    }
    waitForChildSuccess(-1);
}

SCUTEST(test_info_files, .iter=3) {
    int num_files = _i ? _i : 1;
    const char* names[num_files];
    const char* torrent_file;
    close(createTestTorrentWithFiles(_i, &torrent_file, names, 0));

    const char *argv[] = {"unit_test", "-I", torrent_file, NULL};
    waitForChildSuccess(forkAndRun(argv));
}

SCUTEST(test_specific_port) {
    const char*torrentFileName;
    int fd = createTestTorrentWithFiles(0, &torrentFileName, NULL, PIECE_SIZE);
    close(fd);
    const char *argv[] = {"unit_test", CONCAT("-p", TEST_PORT), torrentFileName, NULL};
    int port = listenOnPort(TEST_PORT);
    assert(parseArgsAndRun(argv));
    close(port);
}

SCUTEST(test_help_version_args) {
    char * flags[] = {"-h", "--help", "-v", "--version"};
    for(int i = 0; i < LEN(flags); i++) {
        if(!fork()) {
            const char *argv[] = {"unit_test", flags[i], NULL};
            parseArgsAndRun(argv);
            exit(1);
        }
        waitForChildSuccess(-1);
    }
}

SCUTEST(test_missing_torrent_file) {
    if(!fork()) {
        const char *argv[16] = {"unit_test", "-c", test_annouce_url, NULL, NULL};
        assert(parseArgsAndRun(argv));
        exit(0);
    }
    waitForChildFailure(-1);
    if(!fork()) {
        const char *argv[16] = {"unit_test", NULL};
        assert(parseArgsAndRun(argv));
        exit(0);
    }
    waitForChildFailure(-1);
}

SCUTEST(test_bad_file_path, .iter = 3) {
    const char* torrent_file_name;
    int fd = createTestTorrentWithFiles(_i, &torrent_file_name, NULL, 1);
    assert(fd != -1);
    close(fd);

    if(!fork()) {
        const char *argv[16] = {"unit_test", "-n", torrent_file_name, "/dev/null/bad_path"};
        assert(parseArgsAndRun(argv) == 1);
        exit(0);
    }
    waitForChildFailure(-1);
}
