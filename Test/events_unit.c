#include <assert.h>
#include <fcntl.h>
#include <libgen.h>
#include <netinet/in.h>
#include <poll.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

#include "scutest.h"

#include "../bencode.h"
#include "../hash.h"
#include "../nanotorrent.h"
#include "../util.h"
#include "tester.h"

SCUTEST(test_die_on_signal, .iter=2) {
    int sig[] = {SIGTERM, SIGINT};
    const char* cacheFile;
    TorrentMetadata metadata = {};
    createSimpleTorrentMetadataWithCacheFile(&metadata, &cacheFile);

    pid_t pid = spawnTorrentClientFromMetadata(&metadata, getDefaultLeecherSettings(NULL), NULL);

    kill(pid, sig[_i]);
    waitForChildSuccess(pid);
}

SCUTEST(test_die_with_stdout) {
    // in non-daemon mode, we should die with stdout
    TorrentMetadata metadata = {};
    createSimpleTorrentMetadata(&metadata);

    int fds[2];
    pipe(fds);
    pid_t pid = spawnTorrentClientFromMetadata(&metadata, getDefaultLeecherSettings(NULL), fds);
    close(fds[0]);
    waitForChildSuccess(pid);
    close(fds[1]);
}

SCUTEST(test_live_past_stdout) {
    // in daemon mode, we should not die with stdout
    int fds[2];
    pipe(fds);
    const char* cacheFile;
    TorrentMetadata metadata = {};
    createSimpleTorrentMetadataWithCacheFile(&metadata, &cacheFile);

    pid_t pid = spawnTorrentClientFromMetadata(&metadata, getDefaultSeederSettings(cacheFile), fds);
    close(fds[0]);
    killAndWaitForSuccess(1, &pid);
    close(fds[1]);
}

SCUTEST(test_output_before_anything_else) {
    TorrentMetadata metadata = {};
    const char* cacheFile;
    createSimpleTorrentMetadataWithCacheFile(&metadata, &cacheFile);

    int fds[2];
    pipe(fds);
    pid_t pid = spawnTorrentClientFromMetadata(&metadata, getDefaultLeecherSettings(cacheFile), fds);
    close(fds[1]);
    int cacheFd = open(cacheFile, O_RDONLY);
    diffFiles(cacheFd, fds[0], 0);
    waitForChildSuccess(pid);
    close(cacheFd);
    close(fds[0]);
}

SCUTEST(test_spawn_die_with_cmd, .iter = 2) {
    TorrentMetadata metadata = {};
    const char* torrentFile = createSimpleTorrentMetadata(&metadata);
    const char *argv[] = {"unit_test", _i ? "-s" : "-S", "exit 0", torrentFile , NULL};
    int fds[2];
    assert(pipe(fds) == 0);
    waitForChildSuccess(forkAndRun(argv));
    close(fds[1]);
    char b;
    assert(read(fds[0], &b, 1) == 0);
    close(fds[0]);
}

SCUTEST(test_spawn_verify_output, .iter = 2) {
    TorrentMetadata metadata = {};
    const char* cacheFile;
    const char* torrentFile = createSimpleTorrentMetadataWithCacheFile(&metadata, &cacheFile);
    pid_t peerPid = spawnTorrentClientFromMetadata(&metadata, getDefaultSeederSettings(cacheFile), NULL);

    const char* name;
    int fd1 = createTempFile(&name);

    setenv("FILE", name, 1);
    const char *argv[] = {"unit_test", "-F", cacheFile, _i ? "-s" : "-S", "cat - > $FILE", torrentFile, NULL};
    waitForChildSuccess(forkAndRun(argv));

    int fd2 = createTempFile(NULL);
    int fds[] = {fd1, fd2};
    waitForChildSuccess(spawnTorrentClientFromMetadata(&metadata, getDefaultLeecherSettings(cacheFile), fds));

    killAndWaitForSuccess(1, &peerPid);
    lseek(fd1, 0, SEEK_SET);
    lseek(fd2, 0, SEEK_SET);

    diffFiles(fd1, fd2, 0);
    close(fd1);
    close(fd2);
}

SCUTEST(test_peer_list, .iter = 2) {
    const char* cacheFile;
    TorrentMetadata metadata = {};
    const char* torrentFile = createSimpleTorrentMetadataWithCacheFile(&metadata, &cacheFile);

    TorrentSettings settings = getDefaultSeederSettings(cacheFile);
    settings.minPort = TEST_PORT;
    settings.maxPort = TEST_PORT;
    settings.peerList = "";

    const char *argv[] = {"unit_test", "-q", "-P", CONCAT("127.0.0.1:", TEST_PORT), torrentFile, NULL};

    pid_t child = _i ? forkAndRun(argv) : -1;
    pid_t pid = spawnTorrentClientFromMetadata(&metadata, settings, NULL);

    waitForChildSuccess(forkAndRun(argv));
    if (child != -1)
        waitForChildSuccess(child);

    killAndWaitForSuccess(1, &pid);
}

SCUTEST_SET_FIXTURE(setupFakeHTTPServer, cleanupFakeHTTPServer);

SCUTEST(test_choke, .iter = 2) {
    TorrentMetadata torrentMetadata;
    const char* cacheFile;
    const char* torrentFileName = createSimpleTorrentMetadataWithCacheFile(&torrentMetadata, &cacheFile);
    TorrentInfo torrentInfos[3] = {};
    TorrentSettings settings[] = {getDefaultLeecherSettings(NULL), getDefaultSeederSettings(cacheFile), getDefaultSeederSettings(cacheFile)};
    if (_i) {
        enableExtensions(LEN(settings), settings, FAST_EXTENSION_BIT);
    }

    initAndConnect(&torrentMetadata, LEN(settings), torrentInfos, settings);
    queueMessageToSend(&torrentInfos[1].peers[0], CHOKE, NULL);

    while (!handleEventsForMultipleClients(LEN(torrentInfos), torrentInfos)) {
        assert(torrentInfos[1].peers[0].stats[UPLOAD].totals == 0);
        assert(torrentInfos[1].peers[0].stats[DOWNLOAD].totals == 0);
    }
    cleanupTorrentInfos(LEN(torrentInfos), torrentInfos);
}

SCUTEST(test_cancel) {
    TorrentMetadata metadata = {};
    const char* cacheFile;
    const char* torrentFile = createSimpleTorrentMetadataWithCacheFile(&metadata, &cacheFile);
    pid_t pid = spawnTorrentClientFromMetadata(&metadata, getDefaultSeederSettings(cacheFile), NULL);

    TorrentInfo leecher = {};
    TorrentSettings settings = getDefaultLeecherSettings(NULL);
    settings.disableRandomization = 1;
    initTorrentInfo(&leecher, &metadata, settings);

    listenForClientsAndAnnounceToTracker(&leecher);
    while(leecher.numPeers != 1 || getNumOutstandingBlocks(&leecher.peers[0]) < 2) {
        GLOBAL_FLAGS = 0;
        handleEvents(&leecher, 0);
    }
    BlockInfo blockInfo = {0, .pieceOffset = MIN_BLOCK_SIZE, .len = MIN_BLOCK_SIZE};
    queueMessageToSend(&leecher.peers[0], CANCEL, &blockInfo);
    assert(eventLoop(&leecher) == 0);
    killAndWaitForSuccess(1, &pid);
}

SCUTEST(test_download_order, .iter=4) {
    int numFiles = 3;
    TorrentMetadata torrentMetadata = {};
    const char* cacheFile;
    createTorrentMetadataWithCacheFile(&torrentMetadata, -PIECE_SIZE, numFiles, &cacheFile) ;

    const char* paths[] = {"/1", NULL};

    TorrentSettings settings = getDefaultLeecherSettings(NULL);
    settings.disableRandomization = 1;
    int totalPieces = 1;
    if(_i & 1) {
        settings.extraPieces[0] = -1;
        totalPieces++;
    }
    if(_i & 2) {
        settings.extraPieces[1] = 1;
        totalPieces++;
    }
    int totalSize = PIECE_SIZE * totalPieces;
    settings.paths = paths;

    TorrentInfo leecher = {};
    initTorrentInfo(&leecher, &torrentMetadata, settings);

    TorrentInfo seeder = {0};
    initTorrentInfo(&seeder, &torrentMetadata, getDefaultSeederSettings(cacheFile));

    listenForClientsAndAnnounceToTracker(&seeder);
    listenForClientsAndAnnounceToTracker(&leecher);
    assert(leecher.numPeers == 1);

    setenv("NT_STEP", "1", 1);
    int nextPieceToOutput = leecher.nextPieceToOutput;
    while(!isBitMapOne(&leecher.downloadedBitmap, leecher.startingPieceIndex, leecher.endingPieceIndex)) {
        GLOBAL_FLAGS = 0;
        handleEvents(&leecher, 0);
        handleEvents(&seeder, 0);
        if(!isBitMapOne(&leecher.downloadedBitmap, leecher.startingPieceIndex, leecher.endingPieceIndex)) {
            assert(isBitMapZero(&leecher.downloadedBitmap, 0, leecher.startingPieceIndex));
            assert(isBitMapZero(&leecher.downloadedBitmap, leecher.endingPieceIndex, leecher.metadata->numPieces));
        }
        if(nextPieceToOutput != leecher.nextPieceToOutput) {
            assert(nextPieceToOutput + 1 == leecher.nextPieceToOutput);
            nextPieceToOutput++;
        }
    }
    assert(leecher.trackerInfo.downloaded == totalSize);

    signal(SIGPIPE, SIG_IGN);
    cleanupTorentInfo(&leecher);
    cleanupTorentInfo(&seeder);
}

SCUTEST(test_output_only_desired_pieces, .iter = 8) {
    int num_files = 4;
    const char* torrent_file;
    const char* _names[num_files];
    const char** names = _names;
    int startingSize = _i > 3 ? PIECE_SIZE / 4 : 1;
    pid_t pid = setupPeerSeeder(num_files, &torrent_file, names, startingSize);

    if(_i  & 1) {
        names++;
        --num_files;
    }
    if(_i & 2) {
        names[--num_files] = NULL;
    }
    const char* arg = "-q";
    if(_i == 5) {
        arg="-qe1";
    } else if(_i == 5) {
        arg="-qe-1";
    }
    int dest = createTempFile(NULL);
    pid_t child;
    if(!(child = fork())) {
        dup2(dest, STDOUT_FILENO);
        close(dest);
        for(int i = 0; i < num_files; i++) {
            const char *args[] = {"unit_test", arg, torrent_file, names[i], NULL};
            assert(parseArgsAndRun(args) == 0);
        }
        exit(0);
    }
    int targetFD = combineFiles(num_files, names, NULL);

    waitForChildSuccess(child);
    lseek(dest, 0, SEEK_SET);

    struct stat fileStat[2];
    assert(fstat(dest, &fileStat[0]) != -1 && fstat(targetFD, &fileStat[1]) != -1);
    assert(fileStat[0].st_size == fileStat[1].st_size);
    char buffer[2][fileStat[0].st_size];
    assert(safeRead(targetFD, buffer[1], fileStat[1].st_size) == fileStat[1].st_size);
    assert(safeRead(dest, buffer[0], fileStat[0].st_size) == fileStat[0].st_size);
    assert(memcmp(buffer[0], buffer[1], fileStat[1].st_size) == 0);

    killAndWaitForSuccess(1, &pid);
    close(targetFD);
    close(dest);
}

SCUTEST(test_stalled_output, .iter = 4) {
    int pieceShift = PIECE_SHIFT + _i;
    int size = 1U << (PIECE_SHIFT + 4);

    const char* cacheFile;
    TorrentMetadata metadata = {};
    const char* torrent_file = createTorrentMetadataWithCacheFileAndPieceSize(&metadata, size, 1, &cacheFile, 1 << pieceShift);

    int fds[2];
    pipe(fds);
    pid_t pid = spawnTorrentClientFromMetadata(&metadata, getDefaultLeecherSettings(cacheFile), fds);
    close(fds[1]);

    const char *args[] = {"unit_test", "-nq", torrent_file, NULL};
    assert(parseArgsAndRun(args) == 0);
    int cacheFD = open(cacheFile, O_RDONLY);
    diffFiles(fds[0], cacheFD, size);
    close(fds[0]);
    close(cacheFD);
    waitForChildSuccess(pid);
}

SCUTEST(test_stalled_seeder,  .iter = 2 ) {
    int num_files = 1;
    const char* torrent_file;
    const char* _names[num_files];
    const char** names = _names;
    pid_t pids[] = {
        setupPeerSeeder(num_files, &torrent_file, names, PIECE_SIZE * 4),
        startPeerSeeder(num_files, torrent_file, names),
        startPeerSeeder(num_files, torrent_file, names),
    };
    int usePipe = _i;
    for(int  i = 0; i < LEN(pids) - 1; i++) {
        pid_t child;
        int fds[2];
        if(usePipe)
            pipe(fds);
        if(!(child = fork())) {
            if(usePipe) {
                dup2(fds[1], STDOUT_FILENO);
                close(fds[1]);
                close(fds[0]);
            }
            const char *args[] = {"unit_test", usePipe ? "-q" :"-nq", "-t100", torrent_file, NULL};
            assert(parseArgsAndRun(args) == 0);
            exit(0);
        }
        if(usePipe) {
            close(fds[1]);
            char buffer[512];
            assert(safeRead(fds[0], buffer, 1) == 1);
            assert(kill(pids[i], SIGSTOP) == 0);
            while(safeRead(fds[0], buffer, LEN(buffer)));
            close(fds[0]);
        }
        waitForChildSuccess(child);
        if(!usePipe) {
            assert(kill(pids[i], SIGTSTP) == 0);
        }
    }
    for(int  i = 0; i < LEN(pids); i++)
        assert(kill(pids[i], SIGCONT) == 0);
    killAndWaitForSuccess(LEN(pids), pids);
}

static void runUntilFlags(TorrentInfo* clients, int numClients, TorrentInfo* client, int targetFlags) {
    unsigned flagMask = (1 << NUM_FLAGS) - 1;
    assert((targetFlags & flagMask) == targetFlags);
    while (client->peers[0].flags != targetFlags) {
        assert((client->peers[0].flags & flagMask) == client->peers[0].flags);
        for (int i = numClients - 1; i >= 0; i--) {
            handleEvents(clients + i, 0);
        }
    }
}

SCUTEST(test_peer_flags) {
    TorrentMetadata metadata = {};
    const char* torrentFile = createSimpleTorrentMetadataWithCacheFile(&metadata, NULL);
    TorrentInfo clients[2] = {};
    for(int i = LEN(clients) - 1; i >= 0; i--) {
        initTorrentInfo(&clients[i], &metadata, getDefaultLeecherSettings(NULL));
        listenForClientsAndAnnounceToTracker(&clients[i]);
    }

    int baseFlags = SENT_HANDSHAKE | RECEIVED_HANDSHAKE;
    runUntilFlags(clients, LEN(clients), &clients[0], NOT_CHOKING_PEER | PEER_NOT_CHOKING_ME | baseFlags);
    runUntilFlags(clients, LEN(clients), &clients[1], NOT_CHOKING_PEER | PEER_NOT_CHOKING_ME | baseFlags);

    queueMessageToSend(&clients[0].peers[0], CHOKE, NULL);

    runUntilFlags(clients, LEN(clients), &clients[0], PEER_NOT_CHOKING_ME | baseFlags);
    runUntilFlags(clients, LEN(clients), &clients[1], NOT_CHOKING_PEER | baseFlags);

    queueMessageToSend(&clients[0].peers[0], UNCHOKE, NULL);
    runUntilFlags(clients, LEN(clients), &clients[0], NOT_CHOKING_PEER | PEER_NOT_CHOKING_ME | baseFlags);
    runUntilFlags(clients, LEN(clients), &clients[1], NOT_CHOKING_PEER | PEER_NOT_CHOKING_ME | baseFlags);

    signal(SIGPIPE, SIG_IGN);
    for(int i = LEN(clients) - 1; i >= 0; i--) {
        cleanupTorentInfo(&clients[i]);
    }
}
