#include "tester.h"

#include "scutest.h"

#include <signal.h>
#include <sys/stat.h>
#include <unistd.h>

SCUTEST_SET_FIXTURE(NULL, NULL);

static TorrentInfo* getDummyTorrentInfo() {
    static TorrentMetadata metadata = {};
    createSimpleTorrentMetadata(&metadata);
    static TorrentInfo torrentInfo = {0};
    int ret = initTorrentInfo(&torrentInfo, &metadata, getDefaultLeecherSettings(NULL));
    assert(ret != -1);
    return &torrentInfo;
}

SCUTEST(test_announce_tracker_down) {
    int fd = connectToTracker(getDummyTorrentInfo());
    assert(fd != -1);
    int ret = write(fd, &fd, 1);
    assert(ret == -1);
    close(fd);
}

SCUTEST(test_connect_fail, .iter = 2) {
    static const char * announceList = "udp://localhost ftp://localhost bad_url";
    const char* torrentName;
    int fd = createTestTorrentWithUrl(&torrentName, announceList);
    TorrentMetadata torrentMetadata = {0};
    assert(readAndParseTorrentFile(&torrentMetadata, fd) != -1);
    close(fd);
    TorrentSettings settings = getDefaultLeecherSettings(NULL);
    TorrentInfo torrentInfo = {};

    initTorrentInfo(&torrentInfo, &torrentMetadata, settings);

    if (_i) {
        assert(connectToTracker(&torrentInfo) == -1);
    } else {
        announceToTrackerQuick(&torrentInfo, STARTED, 1);
        assert(torrentInfo.trackerInfo.fd == -1);
        announceToTrackerQuick(&torrentInfo, STARTED, 0);
        assert(torrentInfo.trackerInfo.fd == -1);
    }
    cleanupTorrentInfos(1, &torrentInfo);
}

SCUTEST(test_peer_connection_add_peer_ignore_duplicates) {
    TorrentInfo* torrentInfo = getDummyTorrentInfo();
    addPeer(torrentInfo, 0xDEADBEEF, -1, -1);
    assert(torrentInfo->numPeers == 1);
    addPeer(torrentInfo, 0xDEADBEEF, -1, -1);
    assert(torrentInfo->numPeers == 1);

    addPeer(torrentInfo, 0xDEADBEEF,  1,  -1);
    assert(torrentInfo->numPeers == 2);
    addPeer(torrentInfo, 0xDEADBEEF,  1,  -1);
    assert(torrentInfo->numPeers == 2);

    removePeer(torrentInfo, 0);
    assert(torrentInfo->numPeers == 1);
    addPeer(torrentInfo, 0xDEADBEEF,  1,  -1);
    assert(torrentInfo->numPeers == 1);
    removePeer(torrentInfo, 0);
    assert(torrentInfo->numPeers == 0);
}
