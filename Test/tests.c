#define SCUTEST_IMPLEMENTATION

#include <assert.h>
#include <libgen.h>
#include <netinet/in.h>
#include <poll.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

#include "scutest.h"

#include "../bencode.h"
#include "../hash.h"
#include "../nanotorrent.h"
#include "../util.h"
#include "tester.h"


int main() {
    enablePedantic();
    return runUnitTests();
}

SCUTEST_SET_DEFAULT_FIXTURE(NULL, NULL, .flags = SCUTEST_CHECK_FD_LEAK | SCUTEST_CHECK_CHILD_LEAK | SCUTEST_CREATE_TMP_DIR);

static char templates[16][64];
static int numTempFiles;
int createTempFile(const char** name) {
    assert(numTempFiles < LEN(templates));
    strcpy(templates[numTempFiles], SCUTEST_TEMP_WORKING_DIR);
    strcat(templates[numTempFiles], "/FXXXXXX");
    if(name) {
        *name = templates[numTempFiles];
    }
    int ret = mkstemp(templates[numTempFiles]);
    if(!name) {
        assert(unlink(templates[numTempFiles]) == 0);
        templates[numTempFiles][0] = 0;
    }
    numTempFiles++;
    return ret;
}

const char* createTempDir() {
    assert(numTempFiles < LEN(templates));
    strcpy(templates[numTempFiles], SCUTEST_TEMP_WORKING_DIR);
    strcat(templates[numTempFiles], "/DXXXXXX");
    char*name = mkdtemp(templates[numTempFiles++]);
    assert(name);
    return name;
}


void createFiles(int num_files, const char**names, int starting_size) {
    if(!names || !num_files)
        num_files = 1;
    for(int i = 0; !i || i < num_files; i++) {
        int fd = createTempFile(names ? &names[i] : NULL);
        int size = starting_size < 0 ? -starting_size : starting_size << i;
        ftruncate(fd, size);
        const void* arbitrary_data = names ? (void*)names[i] : (void*)&i;
        int len = names ? strlen(names[i]) : sizeof(i);
        if(len * 2 > starting_size)
            write(fd, &len, 1);
        else
            for(int n = 0; n < 2; n++) {
                write(fd, arbitrary_data, len);
                lseek(fd, size - len, SEEK_SET);
            }
        close(fd);
    }
}

int createTestTorrentWithUrlWithFilesWithPieceSize(int num, const char**torrentFileName, const char* url, const char** names, int startingSize, int pieceSize) {
    const char* temp_names[num + 1];
    createFiles(num, names ? names : temp_names, startingSize);

    int fd = createTempFile(torrentFileName);
    int ret = createTorrentFile(fd, url, names ? names[0] : temp_names[0], (names) ? names : temp_names, num, pieceSize);
    assert(ret != -1);
    assert(fd != -1);
    lseek(fd, 0, SEEK_SET);
    return fd;
}

int createTestTorrentWithFilesWithPieceSize(int num, const char**torrentFileName, const char** names, int startingSize, int pieceSize) {
    return createTestTorrentWithUrlWithFilesWithPieceSize(num, torrentFileName, test_annouce_url, names, startingSize, pieceSize);
}

int createTestTorrentWithUrl(const char**torrentFileName, const char* url) {
    return createTestTorrentWithUrlWithFilesWithPieceSize(1, torrentFileName, url, NULL, PIECE_SIZE, PIECE_SIZE);
}

int combineFiles(int num_files, const char** names, const char**cacheFile) {
    int fd = createTempFile(cacheFile);
    for(int i = 0; i < num_files || i == 0; i++) {
        int read_fd = open(names[i], O_RDONLY);
        assert(read_fd != -1);
        char buffer[4096];
        int ret;
        while((ret = safeRead(read_fd, buffer, sizeof(buffer)))) {
            assert(ret != -1);
            assert(ret == write(fd, buffer, ret));
        }
        close(read_fd);
    }
    if(cacheFile)
        close(fd);
    else
        lseek(fd, 0, SEEK_SET);
    return fd;
}

void waitForChildSuccess(int pid) {
    int status;
    while(-1 == waitpid(pid, &status, 0)) assert(errno == EAGAIN);
    assert(WIFEXITED(status) && WEXITSTATUS(status) == 0);
}

void waitForChildFailure(int pid) {
    int status;
    while(-1 == waitpid(pid, &status, 0)) assert(errno == EAGAIN);
    assert(WIFEXITED(status) && WEXITSTATUS(status));
}

void waitForSuccesses(int num, int* children) {
    for(int i = 0; i <num; i++) {
        waitForChildSuccess(children[i]);
    }
}

void killAndWaitForSuccess(int num, int* children) {
    for(int i = 0; i <num; i++) {
        assert(kill(children[i], SIGINT) == 0);
    }
    waitForSuccesses(num, children);
}


static int stop_http_client;
static void exit_success() {
    stop_http_client = 1;
}
static void fakeHTTPServer(int fdToParent, int fdToChild) {
    signal(SIGPIPE, SIG_IGN);
    struct sigaction sa = {.sa_handler = exit_success, .sa_flags = SA_NODEFER | SA_RESTART};
    sigaction(SIGINT, &sa, NULL);
    int sfd = listenOnPort(TEST_PORT);
    if(sfd == -1) {
        return;
    }

    struct sockaddr_in addr;
    unsigned int len = sizeof(addr);
    struct pollfd pollfds[] ={ {sfd, POLLIN}, {fdToChild, POLLIN}};
    // Drain any connections that may have been leftover from a previous test
    // Note this is just a best effort attempt
    while(poll(pollfds, 1, 0) != 0) {
        int fd = accept(sfd, (struct sockaddr*)&addr, &len);
        close(fd);
    }

    write(fdToParent, &sfd, 1);
    int minInterval = 1000;
    struct peer_holder {
        int ip;
        short port;
        char hash[HASH_SIZE * 3 + 1];
        long lastTime;
        char clientid[CLIENT_ID_LEN + 1];
    } peers[MAX_NUM_PEERS * 2];
    const char * magicToken = "magicToken";
    int peerCount = 0;
    while(!stop_http_client) {
        int ret = poll(pollfds, LEN(pollfds), -1);
        for(int i = 0; i < LEN(pollfds); i++)
            if (pollfds[i].revents && pollfds[i].revents != POLLIN)
                stop_http_client = 1;
        if(ret == -1 || stop_http_client)
            continue;
        int fd = accept(sfd, (struct sockaddr*)&addr, &len);
        if(fd==-1) {
            perror("Error with accept");
            assert(0);
            break;
        }
        char buffer[1024] = {0};
        safeRead(fd, buffer, sizeof(buffer) - 1);
        if(!strstr(buffer, "\r\n\r\n")) {
            close(fd);
            continue;
        }
        const char* prefix = "GET /announce?";
        if (strncmp(prefix, buffer, strlen(prefix))) {
            close(fd);
            continue;
        }

        short port;
#define extract(LABEL, FMT, ...) do {assert(strstr(buffer, LABEL)); assert(1==sscanf(strstr(buffer, LABEL), LABEL "=" FMT, __VA_ARGS__)); } while(0)

        extract("port", "%hu", &port);
        port = htons(port);
        assert(port);

        long downloaded, left, uploaded;
        extract("downloaded", "%ld", &downloaded);
        extract("left", "%ld", &left);
        extract("uploaded", "%ld", &uploaded);
        char event[16];
        extract("event", "%16s", event);
        int eventIndex;

        extern const char* announceStatusStrings[4];
        for(eventIndex = 0; eventIndex < NUM_ANNOUNCE_STATUS; eventIndex++)  {
            if(announceStatusStrings[eventIndex][0] && strncmp(announceStatusStrings[eventIndex], event, strlen(announceStatusStrings[eventIndex])) == 0) {
                if(eventIndex == STARTED) {
                    assert(uploaded == 0);
                } else if(eventIndex == COMPLETED) {
                    assert(left == 0);
                }
                break;
            }
        }
        if(eventIndex == NUM_ANNOUNCE_STATUS)
            eventIndex = IN_PROGRESS;
        if(eventIndex != STARTED) {
            char token[strlen(magicToken)];
            extract("trackerid", "%10s", token);
            assert(strncmp(magicToken, token, strlen(magicToken)) == 0);
        }


        char clientid[CLIENT_ID_LEN + 1];
        char* hash = peers[peerCount].hash;
        extract("peer_id", CONCAT("%", CLIENT_ID_LEN) "s", clientid);
        extract("info_hash", "%60s", hash);
        assert(!strchr(hash, '&'));

        struct timespec now;
        clock_gettime(CLOCK_MONOTONIC, &now);
        long timeMS = now.tv_sec * 1000 + now.tv_nsec / 1000000;

        int add = 1;
        int found = 0;
        int relevantPeers = 0;
        for(int i = peerCount - 1; i >= 0; i--) {
            if(peers[i].ip == addr.sin_addr.s_addr && peers[i].port == port) {
                if(memcmp(clientid, peers[i].clientid, CLIENT_ID_LEN) == 0) {
                    found = 1;
                    if((timeMS - peers[i].lastTime < minInterval) && (eventIndex == STARTED || eventIndex == IN_PROGRESS))
                        assert(0);
                    add = 0;
                }
                if(eventIndex == STOPPED) {
                    if(i != peerCount - 1)
                        memcpy(&peers[i], &peers[peerCount - 1], sizeof(peers[i]));
                    peerCount--;
                }
            } else if(memcmp(hash, peers[i].hash, HASH_SIZE) == 0)
                relevantPeers++;
        }

        assert(found != (eventIndex == STARTED));
        write(fd, "\r\n\r\n", 4);
        writeDictStart(fd);
            bencodeWriteString(fd, "interval");
            bencodeWriteInt(fd, 2);
            bencodeWriteString(fd, "min interval");
            bencodeWriteInt(fd, minInterval / 1000);
            bencodeWriteString(fd, "peers");
            dprintf(fd, "%d:", relevantPeers * 6);
            for(int i = 0; i < peerCount; i++)
                if(memcmp(hash, peers[i].hash, HASH_SIZE) == 0)
                    write(fd, &peers[i], 6);
            // Sometimes don't send the tracker id. Client should still remember it
            if(!found || peerCount % 2) {
                bencodeWriteString(fd, "tracker id");
                bencodeWriteString(fd, magicToken);
            }
            bencodeWriteString(fd, "unknown_key");
            bencodeWriteInt(fd, 1);
        writeDictEnd(fd);
        close(fd);
        if(add && peerCount != LEN(peers) - 1) {
            peers[peerCount].ip = addr.sin_addr.s_addr;
            peers[peerCount].port = port;
            peers[peerCount].lastTime = timeMS;
            memcpy(peers[peerCount].hash, hash, HASH_SIZE);
            memcpy(peers[peerCount].clientid, clientid, CLIENT_ID_LEN);
            peerCount++;
        }
    }
    close(sfd);
}

static pid_t fake_server_pid;
static int keepAliveFd = -1;
void setupFakeHTTPServer() {
    int childToParent[2];
    int parentToChild[2];
    pipe(childToParent);
    pipe(parentToChild);
    if(!(fake_server_pid = fork())) {
        close(childToParent[0]);
        close(parentToChild[1]);
        fakeHTTPServer(childToParent[1], parentToChild[0]);
        exit(0);
    }
    close(childToParent[1]);
    close(parentToChild[0]);

    char byte;
    assert(safeRead(childToParent[0], &byte, 1) == 1);
    close(childToParent[0]);
    keepAliveFd = parentToChild[1];

    signal(SIGPIPE, SIG_IGN);
}

void cleanupFakeHTTPServer() {
    if(fake_server_pid)
        killAndWaitForSuccess(1, &fake_server_pid);
    if (keepAliveFd  != -1)
        close(keepAliveFd);
}


void diffFiles(int fd1, int fd2, size_t size) {
    if(!size) {
        struct stat fileStat;
        if(fstat(fd1, &fileStat) == -1)
            FATAL_ERROR("Failed to get file size");
        size = fileStat.st_size;
    }
    void* buffer = malloc(size);
    void* buffer2 = malloc(size);
    assert(safeRead(fd1, buffer, size) == size);
    assert(safeRead(fd2, buffer2, size) == size);
    assert(memcmp(buffer, buffer2, size) == 0);

    assert(safeRead(fd1, buffer, 0) == 0);
    assert(safeRead(fd2, buffer2, 0) == 0);

    free(buffer);
    free(buffer2);
}

void diffFilesByName(const char* path1, const char* path2, int size) {
    int fd1 = open(path1, O_RDONLY);
    int fd2 = open(path2, O_RDONLY);
    diffFiles(fd1, fd2, size);
    close(fd2);
    close(fd1);
}

pid_t forkAndRun(const char **argv) {
    pid_t pid = fork();;
    if (!pid) {
        assert(parseArgsAndRun(argv) == 0);
        exit(0);
    }
    return pid;
}

const char* createTorrentMetadataWithCacheFileAndPieceSize(TorrentMetadata* torrentMetadata, int size, int num, const char** cacheFileName, int pieceSize) {
    assert(num < 4096);
    const char* names[num + 1];
    const char* torrent_file_name;

    int fd = createTestTorrentWithFilesWithPieceSize(num, &torrent_file_name, names, size, pieceSize);
    assert(fd != -1);
    assert(readAndParseTorrentFile(torrentMetadata, fd) != -1);
    close(fd);
    if (cacheFileName) {
        combineFiles(num, names, cacheFileName);
        if(chmod(*cacheFileName, 0400) == -1) {
            perror("Failed to make cache file readonly");
            assert(0);
        }
    }
    return torrent_file_name;
}

TorrentSettings getDefaultSeederSettings(const char* cacheFile) {
    TorrentSettings defaultSeederSettings = {.dirfd = -1, .daemon = 1, .cacheFilename = cacheFile, .keepAlive = DEFAULT_CONNECTION_TIMEOUT_MS, .blockSize = 1 << DEFAULT_BLOCK_SHIFT, .doNotCreateBitmapFiles = 1};
    return defaultSeederSettings;
}

TorrentSettings getDefaultLeecherSettings(const char* cacheFile) {
    TorrentSettings defaultLeecherSettings = {.dirfd = -1, .quick=1, .cacheFilename = cacheFile, .keepAlive = DEFAULT_CONNECTION_TIMEOUT_MS, .blockSize = 1 << DEFAULT_BLOCK_SHIFT};
    return defaultLeecherSettings;
}

void enableExtensions(int num, TorrentSettings settings[num], long long extensions) {
    for (int i = 0; i <  num; i++) {
        settings[i].extensions = extensions;
    }
}

pid_t spawnTorrentClientFromMetadata(const TorrentMetadata* torrentMetadata, TorrentSettings settings, int fds[2]) {
    pid_t pid;
    sigset_t set;
    sigset_t oldset;
    sigemptyset(&set);
    sigaddset(&set, SIGUSR1);
    sigprocmask(SIG_BLOCK, &set, &oldset);
    if(!(pid = fork())) {
        sigprocmask(SIG_SETMASK, &oldset, NULL);
        setenv("NT_SIGNAL_PARENT", "1", 1);

        if (fds) {
            dup2(fds[1], STDOUT_FILENO);
            close(fds[0]);
            close(fds[1]);
        }
        TorrentInfo torrentInfo = {0};
        int ret = initTorrentInfo(&torrentInfo, torrentMetadata, settings);
        assert(ret != -1);
        assert(eventLoop(&torrentInfo) == 0);
        exit(0);
    }
    int signal;
    assert(sigwait(&set, &signal) == 0);
    sigprocmask(SIG_SETMASK, &oldset, NULL);
    return pid;
}


int announceToTracker(TorrentInfo* torrentInfo) {
    int sfd = connectToTracker(torrentInfo);
    assert(sfd != -1);
    while (torrentInfo->trackerInfo.fd != -1) {
        handleEvents(torrentInfo, 0);
    }
    return 0;
}

void listenForClientsAndAnnounceToTracker(TorrentInfo* torrentInfo) {
    assert(listenForClients(torrentInfo) != -1);
    assert(announceToTracker(torrentInfo) != -1);
}


pid_t startPeerSeederWithCacheFile(const char* torrent_file, const char* cacheFile) {
    assert(torrent_file);
    pid_t pid;
    if(!(pid = fork())) {
        const char *already_complete_args[] = {"unit_test", "-n", "-B", "-F", cacheFile, torrent_file, NULL};
        assert(parseArgsAndRun(already_complete_args) == 0);
        exit(0);
    }
    return pid;
}

pid_t startPeerSeeder(int num_files, const char* torrent_file, const char** names) {
    const char* cacheFile;
    combineFiles(num_files, names, &cacheFile);
    return startPeerSeederWithCacheFile(torrent_file, cacheFile);
}

pid_t setupPeerSeeder(int num_files, const char** torrent_file, const char** names, int startingSize) {
    int fd = createTestTorrentWithFiles(num_files, torrent_file, names, startingSize);
    close(fd);
    return startPeerSeeder(num_files, *torrent_file, names);
}

int handleEventsForMultipleClients(int num, TorrentInfo torrentInfos[num]) {
    int flags = 0;
    for (int i = 0; i < num; i++) {
        GLOBAL_FLAGS = 0;
        handleEvents(&torrentInfos[i], 0);
        flags |= GLOBAL_FLAGS;
    }
    return flags;
}

static void runUnilHandshakeCompletes(int num, TorrentInfo torrentInfos[num]) {
    while (1) {
        int handshake = 1;
        for (int i = 0; i < num; i++) {
            GLOBAL_FLAGS = 0;
            handleEvents(&torrentInfos[i], 0);
            for(int n = 0; n < torrentInfos[i].numPeers; n++) {
                int match = 0;
                for(int j = 0; j < num; j++) {
                    if (memcmp(torrentInfos[j].clientId, torrentInfos[i].peers[n].clientId, CLIENT_ID_LEN) == 0) {
                        match = 1;
                        break;
                    }
                }
                if (!match)
                    continue;
                if ((torrentInfos[i].peers[n].flags & COMPLETED_HANDSHAKE) != COMPLETED_HANDSHAKE) {
                    handshake = 0;
                    break;
                }
            }
        }
        if (handshake)
            break;
    }
}

void initAndConnect(const TorrentMetadata* torrentMetadata, int num, TorrentInfo torrentInfos[num], TorrentSettings settings[num]) {
    for (int i = 0; i <  num; i++) {
        initTorrentInfo(&torrentInfos[i], torrentMetadata, settings[i]);
        listenForClientsAndAnnounceToTracker(&torrentInfos[i]);
        assert(torrentInfos[i].numPeers == i);
    }
    handleEventsForMultipleClients(num, torrentInfos);
    for (int i = 0; i < num - 1; i++) {
        assert(torrentInfos[i].numPeers == num - 1);
    }
    runUnilHandshakeCompletes(2, torrentInfos);
}

void cleanupTorrentInfos(int num, TorrentInfo torrentInfos[num]) {
    for (int i = 0; i < num; i++) {
        cleanupTorentInfo(&torrentInfos[i]);
    }
}
