#include <assert.h>
#include <fcntl.h>
#include <libgen.h>
#include <netinet/in.h>
#include <poll.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

#include "scutest.h"

#include "../bencode.h"
#include "../hash.h"
#include "../nanotorrent.h"
#include "../util.h"
#include "tester.h"

SCUTEST(test_peer_flags_str) {
    char chars[NUM_FLAGS + 1] = {};
    for(int i = 0; i < NUM_FLAGS; i++) {
        assert(!strchr(chars, *getFlagString(1 << i)));
        chars[i] = *getFlagString(1 << i);
        assert(chars[i]);
    }
}

SCUTEST(test_transfer_rate) {
    Statistics stats = {};
    const int avgBytesPerSec = 1024;
    const long rate = (1000 * avgBytesPerSec / STATS_WINDOW_MS);
    incrementTransferCounts(&stats, 0, avgBytesPerSec);
    for (int i = 1; i < STATS_WINDOWS; i++) {
        incrementTransferCounts(&stats, i, avgBytesPerSec);
        assert(getTransferRate(&stats, i) == rate);
    }

    for (int i = 0; i < STATS_WINDOWS; i++) {
        stats.transferCounts[i] = 0;
        assert(getTransferRate(&stats, STATS_WINDOWS + i) == rate);
        incrementTransferCounts(&stats, STATS_WINDOWS + i, avgBytesPerSec);
    }

}

SCUTEST(test_dump_stats) {
    for(int n = 0; n < LOG_LEVEL_ERROR; n++) {
        LOG_LEVEL = n;
        for(int i = 0; i < 3; i++) {
            TorrentInfo torrentInfo = {.numPeers = i};
            dumpPeers(&torrentInfo);
            dumpStats(&torrentInfo);
        }
    }
}

SCUTEST(test_forget_old_stats) {
    TorrentInfo torrentInfo = {.numPeers = 1};
    for (int i = 0; i < STATS_WINDOWS; i++) {
        for(int n = 0; n < NUM_TRANSFER_TYPES; n++) {
            incrementTransferCounts(&torrentInfo.peers[0].stats[n], i, 1<<20);
        }
        torrentInfo.outputStats.transferCounts[i] += 1<<20;
        moveToNextStatsWindow(&torrentInfo);
    }
    for (int i = 0; i < STATS_WINDOWS; i++) {
        moveToNextStatsWindow(&torrentInfo);
    }
    for (int i = 0; i < STATS_WINDOWS; i++) {
        assert(getTransferRate(&torrentInfo.outputStats, i) == 0);
        for(int n = 0; n < NUM_TRANSFER_TYPES; n++) {
            assert(getTransferRate(&torrentInfo.peers[0].stats[n], i) == 0);
        }
    }
}
