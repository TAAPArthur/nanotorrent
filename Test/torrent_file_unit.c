#include "../bencode.h"
#include "../hash.h"
#include "../util.h"

#include "tester.h"

#include "scutest.h"

SCUTEST(create_torrent_file, .iter = 5) {
    int num_files = 2;
    const char* names[num_files];
    int pieceSize = 1UL << (PIECE_SHIFT + _i);
    close(createTestTorrentWithFilesWithPieceSize(_i, NULL, names, -pieceSize, pieceSize));
}

SCUTEST(create_torrent_file_with_announce_list, .iter = 4) {
    const char * announceLists[] = {"A1", "A1 A2", "A1\nB1", "A1 A2 A3\nB1 B2\nC1"};
    const char * announceList = announceLists[_i];
    int fd = createTestTorrentWithUrl(NULL, announceList);
    TorrentMetadata torrentMetadata = {0};
    assert(readAndParseTorrentFile(&torrentMetadata, fd) != -1);
    close(fd);
    for (int i = 0; i < strlen(announceList); i+=3) {
        int announceUrlLen;
        const char* announceUrl = getAnnounceUrl(&torrentMetadata, i / 3, &announceUrlLen);
        assert(announceUrl);
        assert(announceUrlLen == 2);
        assert(strncmp(announceList + i, announceUrl, 2) == 0);
    }
    dumpTorrentInfo(&torrentMetadata);
}

SCUTEST(create_and_parse_file, .iter = 6) {
    int delta = _i % 3 - 1;
    int num_files = _i ? _i : 1;
    int single = !_i;
    int size = PIECE_SIZE + delta;
    const char* names[num_files];
    int fd = createTestTorrentWithFiles(single ? 0 : num_files, NULL, names, size);
    int total_expected_size = size * ((1<< num_files) - 1);

    TorrentMetadata torrentMetadata = {0};
    assert(readAndParseTorrentFile(&torrentMetadata, fd) != -1);
    close(fd);
    int announceUrlLen;
    const char* announceUrl = getAnnounceUrl(&torrentMetadata, 0, &announceUrlLen);
    assert(strncmp(announceUrl, test_annouce_url,  announceUrlLen) == 0);
    assert(strlen(test_annouce_url) == announceUrlLen);


    assert(torrentMetadata.name);
    assert(torrentMetadata.nameLen == strlen(names[0]));
    assert(strncmp(torrentMetadata.name, names[0], strlen(names[0])) == 0);

    assert(torrentMetadata.fileCount == num_files);

    assert(torrentMetadata.pieceLength == MIN(PIECE_SIZE, total_expected_size));
    assert(torrentMetadata.pieces);

    char buffer[PIECE_SIZE];

    int filefd = combineFiles(num_files, names, NULL);
    char hashBuffer[HASH_SIZE];
    for(int len = MIN(torrentMetadata.pieceLength,PIECE_SIZE), i = 0; len >0; len -=PIECE_SIZE, i++) {
        safeRead(filefd, buffer, len);
        getHash(buffer, len, hashBuffer);
        assert(memcmp(hashBuffer, torrentMetadata.pieces + i * HASH_SIZE, HASH_SIZE) == 0);
    }
    close(filefd);

    int running_len = 0;
    for(int i = 0; i < num_files; i++) {
        unsigned long len;
        const char* path = getPieceRange(&torrentMetadata, i, NULL, NULL, NULL, &len, NULL);
        assert(strcmp(path, names[i] + 1) == 0);
        assert(len == size << i);
        running_len += len;
    }
    assert(torrentMetadata.totalLength == total_expected_size);
    assert(running_len == total_expected_size);
    dumpTorrentInfo(&torrentMetadata);
}
