#include "../bencode.h"
#include "../bitmap.h"
#include "../hash.h"
#include "../util.h"

#include "tester.h"

#include "scutest.h"

#include <fcntl.h>

SCUTEST(load_bitmap_from_disk, .iter=2) {
    int num_files = _i ? _i : 1;
    const char* names[num_files];

    TorrentMetadata metadata= {0};
    int torrentFileFd = readAndParseTorrentFile(&metadata, createTestTorrentWithFiles(_i, NULL, names, 1));
    assert(torrentFileFd != -1);
    close(torrentFileFd);

    chdir("/");
    BitMap bitmap;
    initBitMap(&bitmap, metadata.numPieces);
    int dirfd = open(".", O_RDONLY);
    loadBitmapFromDisk(dirfd, &metadata, &bitmap);
    assert(isBitMapOne(&bitmap, 0, metadata.numPieces));

    resetBitMap(&bitmap);
    int fd = open(names[0], O_WRONLY);
    assert(write(fd, "DEADBEEF", 8) != -1);
    close(fd);
    loadBitmapFromDisk(dirfd, &metadata, &bitmap);
    assert(!isBitMapOne(&bitmap, 0, metadata.numPieces));
    assert(isBitMapOne(&bitmap, 1, metadata.numPieces));
    close(dirfd);
}

