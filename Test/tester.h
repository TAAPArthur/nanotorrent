#ifndef TESTER_H
#define TESTER_H

#include "../nanotorrent.h"
#include "../nanotorrent_private.h"
#include "../util.h"
#include <signal.h>

#define TEST_PORT        6890
#define PIECE_SIZE       (1 << MAX_BLOCK_SHIFT)
#define BLOCK_SIZE       (1 << DEFAULT_BLOCK_SHIFT)
#define TEST_CACHE_SIZE  (PIECE_SIZE * 2)



#define test_annouce_url CONCAT("http://localhost:", TEST_PORT) "/announce"

extern int GLOBAL_FLAGS;

static inline void enablePedantic() {
    setenv("NT_PEDANTIC", "2", 1);
}

static inline void disablePedanticWarnings() {
    setenv("NT_PEDANTIC", "1", 1);
}

static inline void disablePedantic() {
    unsetenv("NT_PEDANTIC");
}

void createFiles(int num_files, const char**names, int starting_size) ;

int createTestTorrentWithUrl(const char**torrentFileName, const char* url) ;
int createTestTorrentWithFilesWithPieceSize(int num, const char**torrentFileName, const char** names, int startingSize, int pieceSize) ;
int combineFiles(int num_files, const char** names, const char**cacheFile) ;
static inline int createTestTorrentWithFiles(int num, const char**torrentFileName, const char** names, int startingSize) {
    return createTestTorrentWithFilesWithPieceSize(num, torrentFileName,  names, startingSize, PIECE_SIZE);
}

int createTempFile(const char** name);
const char* createTempDir();


void waitForChildSuccess(int pid) ;

void waitForChildFailure(int pid) ;

void waitForSuccesses(int num, int* children) ;

void killAndWaitForSuccess(int num, int* children) ;

pid_t forkAndRun(const char **argv);

void cleanupFakeHTTPServer();
void setupFakeHTTPServer();

void diffFiles(int fd1, int fd2, size_t size);
void diffFilesByName(const char* path1, const char* path2, int size);


const char* createTorrentMetadataWithCacheFileAndPieceSize(TorrentMetadata* torrentMetadata, int size, int num, const char** cacheFileName, int pieceSize);
static  inline const char* createTorrentMetadataWithCacheFile(TorrentMetadata* torrentMetadata, int size, int num, const char** cacheFileName) {
    return createTorrentMetadataWithCacheFileAndPieceSize(torrentMetadata, size, num, cacheFileName, PIECE_SIZE);
}
static inline const char* createTorrentMetadata(TorrentMetadata* torrentMetadata, int size, int num) {
    return createTorrentMetadataWithCacheFile(torrentMetadata, size, num, NULL);
}

static inline const char* createSimpleTorrentMetadataWithCacheFile(TorrentMetadata* torrentMetadata, const char** cacheFileName) {
    return createTorrentMetadataWithCacheFile(torrentMetadata, 3 * PIECE_SIZE, 1, cacheFileName);
}

static inline const char* createSimpleTorrentMetadata(TorrentMetadata* torrentMetadata) {
    return createSimpleTorrentMetadataWithCacheFile(torrentMetadata, NULL);
}
TorrentSettings getDefaultSeederSettings(const char* cacheFile);
TorrentSettings getDefaultLeecherSettings(const char* cacheFile);

void enableExtensions(int num, TorrentSettings settings[num], long long extensions);

pid_t spawnTorrentClientFromMetadata(const TorrentMetadata* torrentMetadata, TorrentSettings settings, int fds[2]);

int announceToTracker(TorrentInfo* torrentInfo);
void listenForClientsAndAnnounceToTracker(TorrentInfo* torrentInfo);


pid_t startPeerSeederWithCacheFile(const char* torrent_file, const char* cacheFile);

pid_t startPeerSeeder(int num_files, const char* torrent_file, const char** names);

pid_t setupPeerSeeder(int num_files, const char** torrent_file, const char** names, int startingSize);


int handleEventsForMultipleClients(int num, TorrentInfo torrentInfos[num]) ;

void initAndConnect(const TorrentMetadata* torrentMetadata, int num, TorrentInfo torrentInfos[num], TorrentSettings settings[num]) ;

void cleanupTorrentInfos(int num, TorrentInfo torrentInfos[num]) ;

#endif
