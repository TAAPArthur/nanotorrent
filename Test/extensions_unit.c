#include <assert.h>

#include "scutest.h"

#include "tester.h"

SCUTEST_SET_FIXTURE(setupFakeHTTPServer, cleanupFakeHTTPServer);

SCUTEST(test_extension_bit) {
    TorrentMetadata torrentMetadata;
    createSimpleTorrentMetadataWithCacheFile(&torrentMetadata, NULL);
    TorrentInfo torrentInfos[2] = {};

    TorrentSettings settings[] = {getDefaultLeecherSettings(NULL), getDefaultLeecherSettings(NULL)};
    enableExtensions(2, settings, EXTENSION_PROTOCOL_BIT);
    initAndConnect(&torrentMetadata, 2, torrentInfos, settings);
    for(int i = 0; i < LEN(torrentInfos); i++) {
        assert(doesPeerSupportExtension(&torrentInfos[i].peers[0], EXTENSION));
    }
    cleanupTorrentInfos(2, torrentInfos);
}

SCUTEST(test_dont_have_extensions_send) {
    TorrentMetadata torrentMetadata;
    const char* cacheFile;
    const char* torrentFileName = createSimpleTorrentMetadataWithCacheFile(&torrentMetadata, &cacheFile);
    TorrentInfo torrentInfos[2] = {};
    TorrentSettings settings[] = {getDefaultLeecherSettings(cacheFile), getDefaultSeederSettings(cacheFile)};
    enableExtensions(2, settings, EXTENSION_PROTOCOL_BIT);

    initAndConnect(&torrentMetadata, 2, torrentInfos, settings);

    while (!isSet(&torrentInfos[1].peers[0].bitmap, 0)) {
        handleEventsForMultipleClients(2, torrentInfos);
    }

    const BlockInfo blockInfo = {0};
    queueMessageToSend(&torrentInfos[0].peers[0], DONT_HAVE, &blockInfo);
    while(isSet(&torrentInfos[1].peers[0].bitmap, 0)) {
        handleEventsForMultipleClients(2, torrentInfos);
    }
    cleanupTorrentInfos(2, torrentInfos);
}

extern int GLOBAL_FLAGS;
SCUTEST(test_dont_have_extensions) {
    TorrentMetadata torrentMetadata;
    const char* cacheFile;
    const char* torrentFileName = createSimpleTorrentMetadataWithCacheFile(&torrentMetadata, &cacheFile);
    TorrentInfo torrentInfos[2] = {};
    TorrentInfo* seeder = &torrentInfos[1];
    TorrentSettings settings[] = {getDefaultLeecherSettings(NULL), getDefaultSeederSettings(cacheFile)};
    enableExtensions(2, settings, EXTENSION_PROTOCOL_BIT);
    settings[0].cacheSize = PIECE_SIZE;
    settings[0].quick = 0;
    initAndConnect(&torrentMetadata, 2, torrentInfos, settings);

    while (!handleEventsForMultipleClients(2, torrentInfos));

    assert(doesPeerSupportExtension(&torrentInfos[0].peers[0], DONT_HAVE));
    assert(doesPeerSupportExtension(&torrentInfos[1].peers[0], DONT_HAVE));

    assert(seeder->peers[0].bitmap.numBits != 1);
    // Leecher should have forgotten all but one piece and updated its peers
    while (popcount(&seeder->peers[0].bitmap, 0, seeder->peers[0].bitmap.numBits) != 1) {
        handleEventsForMultipleClients(2, torrentInfos);
    }
    assert(popcount(&seeder->peers[0].bitmap, 0, seeder->peers[0].bitmap.numBits) == 1);
    cleanupTorrentInfos(2, torrentInfos);
}

SCUTEST(test_allowed_fast) {
    TorrentMetadata torrentMetadata;
    const char* cacheFile;
    int numPieces = 3;
    const char* torrentFileName = createTorrentMetadataWithCacheFile(&torrentMetadata, numPieces * PIECE_SIZE, 1, &cacheFile);
    TorrentInfo torrentInfos[2] = {};
    TorrentSettings settings[] = {getDefaultLeecherSettings(NULL), getDefaultSeederSettings(cacheFile)};
    enableExtensions(LEN(settings), settings, FAST_EXTENSION_BIT);

    initAndConnect(&torrentMetadata, LEN(settings), torrentInfos, settings);
    queueMessageToSend(&torrentInfos[1].peers[0], CHOKE, NULL);
    for(int i = 0; i < numPieces ; i++) {
        BlockInfo blockInfo = {.pieceIndex = i};
        queueMessageToSend(&torrentInfos[1].peers[0], ALLOWED_FAST, &blockInfo);
    }
    while (!handleEventsForMultipleClients(LEN(torrentInfos), torrentInfos));
    cleanupTorrentInfos(LEN(torrentInfos), torrentInfos);
}

SCUTEST(test_extensions, .iter = 2) {
    TorrentMetadata torrentMetadata = {};
    const char* cacheFile;
    const char* torrentFileName = createSimpleTorrentMetadataWithCacheFile(&torrentMetadata, &cacheFile);
    TorrentSettings settings[] = {getDefaultLeecherSettings(NULL), getDefaultSeederSettings(cacheFile)};
    const TorrentSettings * seederSettings = &settings[1], * leecherSettings = &settings[0] ;
    enableExtensions(2, settings, ALL_PROTOCOL_BITS);
    pid_t pid = spawnTorrentClientFromMetadata(&torrentMetadata, *seederSettings, NULL);
    if (_i) {
        waitForChildSuccess(spawnTorrentClientFromMetadata(&torrentMetadata, *leecherSettings, NULL));
    } else {
        waitForChildSuccess(spawnTorrentClientFromMetadata(&torrentMetadata, getDefaultLeecherSettings(NULL), NULL));
    }
    killAndWaitForSuccess(1, &pid);
}
