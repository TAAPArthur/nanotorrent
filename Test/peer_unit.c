#include <assert.h>
#include <fcntl.h>
#include <libgen.h>
#include <netinet/in.h>
#include <poll.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

#include "scutest.h"

#include "../bencode.h"
#include "../hash.h"
#include "../nanotorrent.h"
#include "../util.h"
#include "tester.h"

static const int sizes[] = {
    [0]  = 1, 512, 4096, BLOCK_SIZE - 1,
    [4]  = BLOCK_SIZE, BLOCK_SIZE + 1, BLOCK_SIZE * 2, PIECE_SIZE - 1,
    [8]  = PIECE_SIZE, PIECE_SIZE + 1, PIECE_SIZE + BLOCK_SIZE - 1, PIECE_SIZE + BLOCK_SIZE,
    [12] = PIECE_SIZE + BLOCK_SIZE + 1, PIECE_SIZE * 2 - 1, PIECE_SIZE * 2, PIECE_SIZE * 2 + 1,
    [16] = TEST_CACHE_SIZE, TEST_CACHE_SIZE + 1, PIECE_SIZE * 8 + 1, (8U << PIECE_SHIFT) + BLOCK_SIZE * 3 -1
};

void initAndConnectSeederAndLeecher(const TorrentMetadata* torrentMetadata, const char* cacheFile, TorrentInfo* leecher, TorrentInfo* seeder) {
    initTorrentInfo(leecher, torrentMetadata, getDefaultLeecherSettings(NULL));
    initTorrentInfo(seeder, torrentMetadata, getDefaultSeederSettings(cacheFile));

    listenForClientsAndAnnounceToTracker(seeder);
    assert(seeder->numPeers == 0);

    listenForClientsAndAnnounceToTracker(leecher);
    assert(leecher->numPeers == 1);

    handleEvents(seeder, 1);
    assert(seeder->numPeers == 1);
}


void createTorrentMetadataAndConnectSeederAndLeecher(int i, TorrentInfo* leecher, TorrentInfo* seeder) {
    static TorrentMetadata torrentMetadata;
    static const char* cacheFile;
    const char* torrentFileName = createTorrentMetadataWithCacheFile(&torrentMetadata, sizes[i], i % 3, &cacheFile);
    initAndConnectSeederAndLeecher(&torrentMetadata, cacheFile, leecher, seeder);
}

SCUTEST_SET_FIXTURE(setupFakeHTTPServer, cleanupFakeHTTPServer, .iter = LEN(sizes));

extern int GLOBAL_FLAGS;
SCUTEST(test_peer_connection) {
    TorrentInfo seeder= {0};
    TorrentInfo leecher = {0};
    createTorrentMetadataAndConnectSeederAndLeecher(_i, &leecher, &seeder);

    leecher.outputFd = createTempFile(NULL);
    int i = 0;
    while(leecher.nextPieceToOutput != leecher.endingPieceIndex) {
        GLOBAL_FLAGS = 0;
        handleEvents(&leecher, 0);
        assert(leecher.numPeers == 1);
        handleEvents(&seeder, 0);
        assert(seeder.numPeers == 1);

        if(_i % 3 == 0 && sizes[_i] < PIECE_SIZE * 4) {
            if(i % 32 == 0) {
                queueMessageToSend(&seeder.peers[0], i % 64 == 0 ? UNCHOKE : CHOKE, NULL);
            }
        }
        i++;
    }
    cleanupTorentInfo(&leecher);
    cleanupTorentInfo(&seeder);
    close(leecher.outputFd);
}

SCUTEST(test_seeder_connection_stalled, .iter=2) {
    TorrentInfo seeder = {0};
    TorrentInfo leecher = {0};
    createTorrentMetadataAndConnectSeederAndLeecher(_i, &leecher, &seeder);

    TorrentInfo leecher2 = {0};

    initTorrentInfo(&leecher2, seeder.metadata, getDefaultLeecherSettings(NULL));
    assert(listenForClients(&leecher2) != -1);
    assert(announceToTracker(&leecher2) != -1);
    assert(leecher2.numPeers == 2);

    int msg = htonl(4096);
    assert(write(leecher.peers[_i % 2].fd, &msg, sizeof(msg)) == sizeof(msg));

    while(leecher2.nextPieceToOutput != leecher.endingPieceIndex) {
        GLOBAL_FLAGS = 0;
        handleEvents(&leecher2, 0);
        handleEvents(&seeder, 0);
    }

    cleanupTorentInfo(&leecher);
    cleanupTorentInfo(&seeder);
    cleanupTorentInfo(&leecher2);
}

SCUTEST(test_leech_to_completion) {
    TorrentMetadata torrentMetadata = {};
    const char* cacheFile;
    const char* torrentFileName = createTorrentMetadataWithCacheFile(&torrentMetadata, sizes[_i], _i % 3, &cacheFile);
    int numPeers = _i % 3 + 1;
    int children[numPeers];
    for(int i = 0; i <numPeers; i++) {
        if(!(children[i] = fork())) {
            TorrentInfo peer = {0};
            initTorrentInfo(&peer, &torrentMetadata, getDefaultSeederSettings(cacheFile));
            int ret = eventLoop(&peer);
            assert(ret == 0);
            if (numPeers == 1)
                assert(peer.trackerInfo.uploaded == peer.metadata->totalLength);
            exit(0);
        }
    }
    TorrentInfo leecher = {0};
    initTorrentInfo(&leecher, &torrentMetadata, getDefaultLeecherSettings(NULL));
    int ret = eventLoop(&leecher);
    assert(ret == 0);
    killAndWaitForSuccess(numPeers, children);

    assert(leecher.trackerInfo.left == 0);
    assert(leecher.trackerInfo.downloaded == torrentMetadata.totalLength);
}
