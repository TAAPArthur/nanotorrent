#include "../bitmap.h"
#include "../util.h"

#include "scutest.h"

SCUTEST(test_bitmap_bytes) {
    BitMap bitmap = {};
    initBitMap(&bitmap, 1);
    assert(getNumBytes(&bitmap) == 1);
    assert(getBytesNeeded(&bitmap) == sizeof(bitmap.data[0]));
}

SCUTEST(test_bitmap) {
    BitMap bitmap;
    int bit_list[] = {2, 8, 16, 32, 64, 128, 256};
    for(int n = 0; n < LEN(bit_list); n++) {
        for(int delta = -1; delta < 2; delta++) {
            int bits = bit_list[n] + delta;
            initBitMap(&bitmap, bits);
            for(int i = 0; i < bits; i++) {
                setBit(&bitmap, i);
                assert(isSet(&bitmap, i));
            }
            free(bitmap.data);
        }
    }
}

SCUTEST(test_bitmap_search) {
    BitMap bitmap;
    initBitMap(&bitmap, 4096);
    assert(0 == findFirstClearBit(&bitmap, 0, bitmap.numBits));
    assert(1 == findFirstClearBit(&bitmap, 1, bitmap.numBits));
    assert(-1 == findFirstSetBit(&bitmap, 0, bitmap.numBits));

    setBit(&bitmap, 1024);
    assert(1024 == findFirstSetBit(&bitmap, 0, bitmap.numBits));
    assert(-1 == findFirstSetBit(&bitmap, 0, 1024));
    setBit(&bitmap, 1023);
    assert(1023 == findFirstSetBit(&bitmap, 0, bitmap.numBits));
    assert(-1 == findFirstSetBit(&bitmap, 0, 1023));

    setBit(&bitmap, 0);
    assert(1 == findFirstClearBit(&bitmap, 0, bitmap.numBits));
    assert(-1 == findFirstClearBit(&bitmap, 0, -1));
    assert(1025 == findFirstClearBit(&bitmap, 1023, bitmap.numBits));
    free(bitmap.data);
}

SCUTEST(test_bitmap_is_zero) {
    BitMap bitmap;
    initBitMap(&bitmap, 4096);
    assert(isBitMapZero(&bitmap, 0, bitmap.numBits));
    setBit(&bitmap, 4095);
    assert(!isBitMapZero(&bitmap, 0, bitmap.numBits));
    assert(isBitMapZero(&bitmap, 0, 4095));
    setBit(&bitmap, 1);
    assert(!isBitMapZero(&bitmap, 0, 4095));
    assert(!isBitMapZero(&bitmap, 1, 4095));
    assert(isBitMapZero(&bitmap, 2, 4095));

    free(bitmap.data);
}

SCUTEST(test_bitmap_set_all_bits) {
    BitMap bitmap;
    initBitMap(&bitmap, 1);
    setAllBits(&bitmap);
    assert(*bitmap.data == 0x80);
    free(bitmap.data);

    initBitMap(&bitmap, 7);
    setAllBits(&bitmap);
    assert(*bitmap.data == 0xFE);
    free(bitmap.data);

    initBitMap(&bitmap, 8);
    setAllBits(&bitmap);
    assert(*(short*)bitmap.data == 0xFF);
    free(bitmap.data);
}

SCUTEST(test_bitmap_is_one) {
    BitMap bitmap;
    initBitMap(&bitmap, 256);
    setAllBits(&bitmap);
    assert(!isBitMapZero(&bitmap, 0, bitmap.numBits));
    assert(isBitMapOne(&bitmap, 0, bitmap.numBits));

    free(bitmap.data);
}

int bitmapLens[] = {4, 8, 256};
SCUTEST(test_reset_bitmap, .iter=LEN(bitmapLens)) {
    BitMap bitmap;
    initBitMap(&bitmap, bitmapLens[_i]);
    setAllBits(&bitmap);
    resetBitMap(&bitmap);
    assert(isBitMapZero(&bitmap, 0, bitmap.numBits));
    setAllBits(&bitmap);
    clearRange(&bitmap, 0, bitmap.numBits);
    assert(isBitMapZero(&bitmap, 0, bitmap.numBits));

    setAllBits(&bitmap);
    clearRange(&bitmap, 1, bitmap.numBits - 1);
    assert(isBitMapZero(&bitmap, 1, bitmap.numBits - 1));
    assert(isSet(&bitmap, 0));
    assert(isSet(&bitmap, bitmap.numBits - 1));

    for(int i = 0; i < MIN(bitmap.numBits/4, 8); i++) {
        int low = bitmap.numBits / 4;
        int high = bitmap.numBits / 2;
        setAllBits(&bitmap);
        clearRange(&bitmap, low - i, high + i);
        assert(isBitMapZero(&bitmap, low - i, high + i));
        assert(isBitMapOne(&bitmap, 0, low - i));
        assert(isBitMapOne(&bitmap, high + i, bitmap.numBits));
    }
}

SCUTEST(test_bitmap_find_missing) {
    BitMap self, peer;
    initBitMap(&self, 4096);
    initBitMap(&peer, 4096);
    assert(findFirstMissingBit(&self, &peer, 0, peer.numBits) == -1);
    setBit(&peer, 0);
    assert(findFirstMissingBit(&self, &peer, 0, peer.numBits) == 0);
    setBit(&self, 0);
    setBit(&peer, 1);
    assert(findFirstMissingBit(&self, &peer, 0, peer.numBits) == 1);

    self.data[0] = 0xE8;
    peer.data[0] = 0x0F;
    // Handle the case where the first set bit in peer after the first set bit in self
    // is already set
    assert(findFirstMissingBit(&self, &peer, 0, peer.numBits) == 5);

    free(self.data);
    free(peer.data);
}

SCUTEST(test_bitmap_small) {
    BitMap bitmap;
    initBitMap(&bitmap, 1);
    assert(isBitMapZero(&bitmap, 0, 0));
    setBit(&bitmap, 0);
    assert(isBitMapOne(&bitmap, 0, 0));
    free(bitmap.data);
}

SCUTEST(test_bitmap_copy) {
    BitMap bitmap;
    initBitMap(&bitmap, 256);
    setAllBits(&bitmap);
    BitMap bitmapCopy;
    initBitMap(&bitmapCopy, 256);
    copyBitMap(&bitmapCopy, &bitmap);
    assert(isBitMapOne(&bitmap, 0, bitmap.numBits));
}

SCUTEST(test_bitmap_or) {
    BitMap bitmap1;
    BitMap bitmap2;
    initBitMap(&bitmap1, 256);
    initBitMap(&bitmap2, 256);
    setAllBits(&bitmap1);
    setAllBits(&bitmap2);
    clearRange(&bitmap1, 0, bitmap1.numBits / 2);
    clearRange(&bitmap2, bitmap2.numBits / 2, bitmap2.numBits);
    BitMap bitmapOr;
    initBitMap(&bitmapOr, 256);
    orBitmap(&bitmapOr, &bitmap1, &bitmap2);
    assert(isBitMapOne(&bitmapOr, 0, bitmapOr.numBits));
}

SCUTEST(test_bitmap_popcount) {
    BitMap bitmap;
    initBitMap(&bitmap, 256);
    for(int i = 0; i < bitmap.numBits; i++) {
        assert(popcount(&bitmap, 0, bitmap.numBits) == i);
        setBit(&bitmap, i);
        assert(popcount(&bitmap, i, i + 1) == 1);
    }
    assert(popcount(&bitmap, 0, bitmap.numBits) == bitmap.numBits);
}
