#ifndef BITMAP_H
#define BITMAP_H
#include <assert.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    long numBits;
    unsigned char* data;
} BitMap;

#define BITS_PER_ENTRY(A) (sizeof((A)->data[0]) * 8)


/**
 * Returns the number of bytes backed by this bitmap
 */
static inline int getNumBytes(const BitMap* field) {
    return (field->numBits + 7) / 8;
}

/**
 * Returns the number of bytes that need to be allocated for the BitMap
 */
static inline int getBytesNeeded(const BitMap* field) {
    assert(field->numBits);
    return (getNumBytes(field) + sizeof(field->data[0]) - 1) & ~( sizeof(field->data[0]) - 1);
}

static inline void resetBitMap(BitMap* field) {
    memset(field->data, 0x00, getBytesNeeded(field));
}

static void setAllBits(BitMap* bitmap) {
    memset(bitmap->data, 0xFF, getBytesNeeded(bitmap));
    if (bitmap->numBits % 8) {
        char mask = (1 << (8 - bitmap->numBits % 8)) - 1;
        bitmap->data[getBytesNeeded(bitmap) - 1]  &= ~mask;
    }
}

static inline void initBitMap(BitMap* field, int numBits) {
    field->numBits = numBits;
    field->data = malloc(getBytesNeeded(field));
    resetBitMap(field);
}

static inline void copyBitMap(BitMap* dest, BitMap* src) {
    memcpy(dest->data, src->data, getBytesNeeded(dest));
}

static inline void orBitmap(BitMap* dest, BitMap* field1, const BitMap* field2) {
    for(int i = 0; i < getBytesNeeded(dest); i++)
        dest->data[i] = field1->data[i] | field2->data[i];
}

static inline void setBit(BitMap* field, unsigned long index) {
    assert(index < field->numBits);
    int entry = index / BITS_PER_ENTRY(field);
    int bitIndex = BITS_PER_ENTRY(field) - 1 - index % BITS_PER_ENTRY(field);;
    field->data[entry] |= 1UL << bitIndex;
}

static inline void clearRange(BitMap* field, unsigned long start, unsigned long end) {
    if(start >= end)
        return;
    int startMask = ~((1 << (8 - start % 8)) - 1);
    int endMask   = ((1 << (8 - end % 8)) - 1);
    if(start / BITS_PER_ENTRY(field) == end / BITS_PER_ENTRY(field)) {
        field->data[start / BITS_PER_ENTRY(field)] &= startMask | endMask;
    } else {
        field->data[start / BITS_PER_ENTRY(field)] &= startMask;
        field->data[end / BITS_PER_ENTRY(field)]   &= endMask;
    }
    int startByte = (start + 7) / 8;
    int endByte = end / 8;
    if(endByte > startByte)
        memset(field->data + startByte, 0x00, endByte - startByte);
}

static inline void clearBit(BitMap* field, unsigned long index) {
    assert(index < field->numBits);
    int entry = index / BITS_PER_ENTRY(field);
    int bitIndex = BITS_PER_ENTRY(field) - 1 - index % BITS_PER_ENTRY(field);;
    field->data[entry] &= ~(1UL << bitIndex);
}

static inline int isSet(const BitMap* field, unsigned long index) {
    assert(field->numBits);
    assert(index < field->numBits);
    int entry = index / BITS_PER_ENTRY(field);
    int bitIndex = BITS_PER_ENTRY(field) - 1 - index % BITS_PER_ENTRY(field);
    return field->data[entry] & (1UL << bitIndex) ? 1 : 0;
}

static inline int _findFirstBit(const BitMap* set, const BitMap* clear, long startingIndex, long endingIndex) {
    const int bits_per_entry = BITS_PER_ENTRY(set);
    int end = (endingIndex + bits_per_entry - 1) / bits_per_entry;

    for(int entry = startingIndex / bits_per_entry; entry < end; entry++) {
        if((!set || set->data[entry]) && (!clear || (((unsigned char)clear->data[entry]) + 1 != 0 || entry + 1 == end))) {
            int index = entry * bits_per_entry;
            for(int n = bits_per_entry - 1; n >= 0 && index < endingIndex; n--, index++) {
                if(index >= startingIndex && (!set || (set->data[entry] & (1UL << n))) && (!clear || !(clear->data[entry] & (1UL << n)))) {
                    return index;
                }
            }
        }
    }
    return -1;
}

static inline int findFirstSetBit(const BitMap* field, long startingIndex, long endingIndex) {
    return _findFirstBit(field, 0, startingIndex, endingIndex);
}

static inline int findFirstClearBit(const BitMap* field, long startingIndex, long endingIndex) {
    return _findFirstBit(0, field, startingIndex, endingIndex);
}

static inline int isBitMapZero(const BitMap* field, long startingIndex, long endingIndex) {
    return findFirstSetBit(field, startingIndex, endingIndex) == -1;
}

static inline int isBitMapOne(const BitMap* field, long startingIndex, long endingIndex) {
    return findFirstClearBit(field, startingIndex, endingIndex) == -1;
}

static inline int findFirstMissingBit(const BitMap* ref, const BitMap* search, long startingIndex, long endingIndex) {
    return _findFirstBit(search, ref, startingIndex, endingIndex);
}

static inline int popcount(const BitMap* bitmap, long startingIndex, long endingIndex) {
    int count = 0;
    for(int n = startingIndex; n < endingIndex; n++)
        count += isSet(bitmap, n);
    return count;
}
#endif
