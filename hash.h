#ifndef HASH_H
#define HASH_H

#include "picohash.h"

#ifndef NDEBUG
#include "config.h"
#include "string.h"
#endif

static inline int getHash(const char* start, int len, char*hashDest) {
#ifndef NDEBUG
    static char zeroHash[HASH_SIZE];
    static char setup = 0;
    if (memcmp(start, start + 1, len -1) == 0 && start[0] == 0) {
        if (setup) {
            memcpy(hashDest, zeroHash, HASH_SIZE);
            return 0;
        } else {
            setup = 2;
        }
    }
#endif
    picohash_ctx_t ctx;
    picohash_init_sha1(&ctx);
    picohash_update(&ctx, start, len);
    picohash_final(&ctx, hashDest);
#ifndef NDEBUG
    if (setup == 2) {
        memcpy(zeroHash, hashDest, HASH_SIZE);
        setup = 1;
    }
#endif
    return 0;
}
#endif
