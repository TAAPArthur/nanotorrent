#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <time.h>
#include <unistd.h>

enum LogLevel  {
    LOG_LEVEL_ALL,
    LOG_LEVEL_TRACE,
    LOG_LEVEL_VERBOSE,
    LOG_LEVEL_INFO,
    LOG_LEVEL_WARN,
    LOG_LEVEL_ERROR,
};
extern enum LogLevel LOG_LEVEL;

#define LABEL  __FILE__ CONCAT(":", __LINE__) " "

#ifdef DEBUG
#define LOG(LEVEL, MSG, ...) do {if(LEVEL >= LOG_LEVEL) dprintf(STDERR_FILENO, "%d:" LABEL MSG, getpid(), __VA_ARGS__); } while(0)
#define TRACE(...)           LOG(LOG_LEVEL_TRACE, __VA_ARGS__, "")
#else
#define LOG(LEVEL, MSG, ...) do {if(LEVEL >= LOG_LEVEL) dprintf(STDERR_FILENO, LABEL MSG, __VA_ARGS__); } while(0)
#define TRACE(...)
#endif

#define LOG_ALL(...)         LOG(LOG_LEVEL_ALL, __VA_ARGS__, "")
#define VERBOSE(...)         LOG(LOG_LEVEL_VERBOSE, __VA_ARGS__, "")
#define INFO(...)            LOG(LOG_LEVEL_INFO, __VA_ARGS__, "")
#define STAT(...)            dprintf(STDERR_FILENO, __VA_ARGS__)
#define WARN(...)            LOG(LOG_LEVEL_WARN, __VA_ARGS__, "")
#define ERROR(MSG)           perror(LABEL " " MSG)
#define DIE(EXIT_CODE, ...)  do {dprintf(STDERR_FILENO, __VA_ARGS__); exit(EXIT_CODE);} while(0)

#define LOG_PEER_MSG_WITH_ARGS(LEVEL, PREFIX, peer, ...) LOG(LEVEL, PREFIX " %d.%d.%d.%d:%d ('%.*s')\n", __VA_ARGS__, ((unsigned char*)&(peer)->ip)[0],((unsigned char*)&(peer)->ip)[1],((unsigned char*)&(peer)->ip)[2],((unsigned char*)&(peer)->ip)[3], (peer)->port, CLIENT_ID_LEN, (peer)->clientId)

#define TRACE_PEER_MSG_WITH_ARGS(PREFIX, peer, ...) LOG_PEER_MSG_WITH_ARGS(LOG_LEVEL_TRACE, PREFIX, peer, __VA_ARGS__)
#define VERBOSE_PEER_MSG_WITH_ARGS(PREFIX, peer, ...) LOG_PEER_MSG_WITH_ARGS(LOG_LEVEL_VERBOSE, PREFIX, peer, __VA_ARGS__)
#define VERBOSE_PEER_MSG(PREFIX, peer) VERBOSE_PEER_MSG_WITH_ARGS(PREFIX "%s", peer, "")

#define EXIT_MALFORMED       2
#define EXIT_OUT_OF_RESOURCE 3
#define EXIT_INVALID_ARG     1
#define EXIT_SYS_CALL        -1
#define FATAL_ERROR(...)     do { ERROR(__VA_ARGS__); _exit(EXIT_SYS_CALL); } while(0)


#define LEN(A) (sizeof(A)/sizeof(A[0]))
#define _CONCAT(A, B) A # B
#define CONCAT(A, B) _CONCAT(A, B)

#define MIN(A,B) ((A) < (B) ? (A) : (B))
#define MAX(A,B) ((A) > (B) ? (A) : (B))

#endif
