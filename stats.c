#include "nanotorrent.h"
#include "nanotorrent_private.h"
#include "util.h"

static const char flagBitToChar[11] = "cCiIhHsStT";
static const char* units[] = {"B", "KiB", "MiB", "GiB"};
static int inline getUnitIndex(unsigned long value) { return value < 1<<10 ? 0 : value < 1<<20 ? 1 : value < 1<<30 ? 2 : 3;}
#define VALUE_WITH_UNIT(V) (V >> (getUnitIndex(V) * 10)), (units[getUnitIndex(V)])
#define RATE_FMT_STR "%04u %03s/s %04u %03s"

unsigned long getTransferRate(const Statistics * stats, int bucket) {
    unsigned long count = 0;
    unsigned numWindows;
    if (bucket == 0) {
        count = stats->transferCounts[0];
        numWindows = 1;
    } else {
        for(int j = 1; j < STATS_WINDOWS; j++)
            count += stats->transferCounts[(j + bucket) % STATS_WINDOWS];
        numWindows = MIN(bucket + 1, STATS_WINDOWS) - 1;
    }
    return 1000 * count / (numWindows * STATS_WINDOW_MS);
}

unsigned long getPeerTransferRate(const Peer* peer, TransferType type) {
    return getTransferRate(&peer->stats[type], peer->statBucket);
}

void incrementTransferCounts(Statistics * stats, int bucket, int numBytes) {
    stats->transferCounts[bucket % STATS_WINDOWS] += numBytes;
    stats->totals += numBytes;
}

void moveToNextStatsWindow(TorrentInfo* torrentInfo) {
    for(int n = 0; n < torrentInfo->numPeers; n++) {
        torrentInfo->peers[n].statBucket++;
        for(int i = 0; i < NUM_TRANSFER_TYPES; i++) {
            torrentInfo->peers[n].stats[i].transferCounts[torrentInfo->peers[n].statBucket % STATS_WINDOWS] = 0;
        }
    }
    torrentInfo->statBucket++;
    torrentInfo->outputStats.transferCounts[torrentInfo->statBucket % STATS_WINDOWS] = 0;
}

const char* getFlagString(unsigned int flags) {
    static char flagStr[LEN(flagBitToChar)] = {0};
    int c = 0;
    for(int i = 0; i < LEN(flagBitToChar) - 1; i++) {
        if (flags & (1 << i)) {
            assert(flagBitToChar[i]);
            flagStr[c++] = flagBitToChar[i];
        }
    }
    flagStr[c] = 0;
    return flagStr;
}

void dumpPeers(const TorrentInfo* torrentInfo) {
    if(!torrentInfo->numPeers)
        return;
    STAT("\n%-12s\t%4s\t%8s\t%-10s%-10s\t%-10s%-10s\t%-12s\t%-14s\t%-8s\n", "IP", "Port", "Client", "Upload", "Total Up", "Down", "Total Down", "Flags", "Outstanding", "Ext");
    for(int n = 0; n < torrentInfo->numPeers; n++) {
        const Peer* peer = &torrentInfo->peers[n];
        const char* flagStr = getFlagString(peer->flags);
        unsigned char* ip = (void*)&peer->ip;

        STAT("%d.%d.%d.%d\t%hu\t%8.*s\t" RATE_FMT_STR "\t" RATE_FMT_STR "\t%8s(%2hhX)\t%2d %2d ",
            ip[0], ip[1], ip[2], ip[3],
            peer->port,
            8, peer->clientId,
            VALUE_WITH_UNIT(getPeerTransferRate(peer, UPLOAD)), VALUE_WITH_UNIT(peer->stats[UPLOAD].totals),
            VALUE_WITH_UNIT(getPeerTransferRate(peer, DOWNLOAD)), VALUE_WITH_UNIT(peer->stats[DOWNLOAD].totals),
            flagStr, peer->flags,
            torrentInfo->peers[n].numOutstandingPieces,
            getNumOutstandingBlocks(&torrentInfo->peers[n])
        );
        for(int i = 0; i < peer->numOutstandingPieces; i++)
            STAT("%d,", peer->outstandingPieces[i]);
        for(int i = peer->numOutstandingPieces; i < MAX_PEER_PEER_OUTSTANDING_PIECES; i++)
            STAT("%4s", "");
        STAT("\t0x%lX", torrentInfo->peers[n].extensionBytes);
        STAT("\n");
    }
}

void dumpStats(const TorrentInfo* torrentInfo) {
    if(!torrentInfo->numPeers)
        return;
    unsigned long rates[NUM_TRANSFER_TYPES] = {0};
    unsigned outstandingPieces = 0;
    unsigned outstandingBlocks = 0;
    for(int n = 0; n < torrentInfo->numPeers; n++) {
        for(int i = 0; i < NUM_TRANSFER_TYPES; i++) {
            rates[i] += getPeerTransferRate(&torrentInfo->peers[n], i);
        }
        outstandingPieces += torrentInfo->peers[n].numOutstandingPieces;
        outstandingBlocks += getNumOutstandingBlocks(&torrentInfo->peers[n]);
    }
    if(isatty(STDERR_FILENO))
        STAT("\r");

    STAT("Uploaded " RATE_FMT_STR "; Download " RATE_FMT_STR ";",
            VALUE_WITH_UNIT(rates[UPLOAD]), VALUE_WITH_UNIT(torrentInfo->trackerInfo.transfers[UPLOAD]),
            VALUE_WITH_UNIT(rates[DOWNLOAD]),VALUE_WITH_UNIT(torrentInfo->trackerInfo.transfers[DOWNLOAD]));

    STAT("Output " RATE_FMT_STR ";",
            VALUE_WITH_UNIT(getTransferRate(&torrentInfo->outputStats, torrentInfo->statBucket)), VALUE_WITH_UNIT(torrentInfo->outputStats.totals));
    unsigned nextPiece = findFirstClearBit(&torrentInfo->downloadedBitmap, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex);
    if(nextPiece != -1) {
        STAT("Outstanding %d; %d; Next Piece %04d/%04d; %02d%%",
            outstandingPieces, outstandingBlocks,
            findFirstClearBit(&torrentInfo->requested, torrentInfo->startingPieceIndex, torrentInfo->endingPieceIndex),
            nextPiece,
            (nextPiece - torrentInfo->startingPieceIndex) * 100 / (torrentInfo->endingPieceIndex - torrentInfo->startingPieceIndex));
    }
    STAT(" Num Peers %d", torrentInfo->numPeers);
    if(!isatty(STDERR_FILENO) || LOG_LEVEL <= LOG_LEVEL_VERBOSE)
        STAT("\n");
}
