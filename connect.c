#include <assert.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include "bencode.h"
#include "nanotorrent.h"
#include "util.h"

static inline char* getHost(const char* url, const char* end, short* port) {
    static char host[255] = {0};
    const char* domainEnd = url;
    while (domainEnd < end && *domainEnd != ':') {
        domainEnd++;
    }
    if (*domainEnd != ':')
        domainEnd = NULL;
    *port = 80;

    if(!domainEnd) {
        domainEnd = url;
        while (domainEnd < end && *domainEnd != '/') {
            domainEnd++;
        }
    } else {
        *port = atoi(domainEnd + 1);
    }
    strncpy(host, url, MIN(sizeof(host)-1, domainEnd-url));
    return host;
}
static const char* supportedProtocols[] = {"http://"};
const char * isProtocolSupported(const char* url, int len) {
    for (int i = 0; i < LEN(supportedProtocols); i++) {
        int slen = strlen(supportedProtocols[i]);
        if (len >  slen && strncmp(url, supportedProtocols[i], slen) == 0)
            return url + slen;
    }
    return NULL;
}

static inline char* getHostAndPortFromUrl(const char* announceUrl, int announceUrlLen, short* port) {
    const char* ptr = announceUrl;
    const char* host = isProtocolSupported(announceUrl, announceUrlLen);
    if(!host) {
        WARN("announce url doesn't have supported protocol info: %.*s\n", announceUrlLen, announceUrl);
        return NULL;
    }
    return getHost(host, announceUrl + announceUrlLen, port);
}

static int async_connect(int sfd, const struct sockaddr * addr, socklen_t socklen) {
    if(sfd != -1 && connect(sfd, addr, socklen) == -1) {
        if(errno != EINPROGRESS) {
            ERROR("failed connect to socket");
            return -1;
        }
    }
    return sfd;
}

static int connectToPeer(int ip, short port) {
    int sfd = socket(AF_INET, SOCK_NONBLOCK|SOCK_STREAM, 0);
    if(sfd == -1) {
        ERROR("Could not create socket");
        return -1;
    }

    struct sockaddr_in addr = {.sin_family = AF_INET, .sin_addr.s_addr = ip, .sin_port = port};
    if(async_connect(sfd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        close(sfd);
        ERROR("failed to connect to socket");
        return -1;
    }
    return sfd;
}

int addPeerAndConnect(TorrentInfo* torrentInfo,int ip, short port) {
    int n = addPeer(torrentInfo, ip, port, -1);
    if(n == -1)
        return -1;

    torrentInfo->peers[n].fd = connectToPeer(ip, htons(port));
    if(torrentInfo->peers[n].fd == -1) {
        removePeer(torrentInfo, n);
        return -1;
    }
    return n;
}

int connectToSpecificTracker(TorrentInfo* torrentInfo, const char* host, int port) {
    assert(torrentInfo->trackerInfo.fd == -1);
    struct addrinfo *result = NULL;
    struct addrinfo hint = {0};
    hint.ai_family = AF_INET;
    hint.ai_socktype = SOCK_STREAM;
    if(getaddrinfo(host, NULL, &hint, &result)) {
        ERROR("Failed to get addr info");
        return -1;
    }
    ((struct sockaddr_in*)result->ai_addr)->sin_port = htons(port);
    int sfd = socket(AF_INET, SOCK_NONBLOCK|SOCK_STREAM, 0);

    if(sfd == -1) {
        ERROR("failed to create or connect to socket");
    } else if(async_connect(sfd, result->ai_addr, result->ai_addrlen) == -1) {
        close(sfd);
        sfd = -1;
    } else {
        VERBOSE("Connecting to tracker %s:%d\n", host, port);
        torrentInfo->trackerInfo.fd = sfd;
        torrentInfo->trackerInfo.sending = 1;
        torrentInfo->trackerInfo.read = 0;
        torrentInfo->trackerInfo.sent = 0;
        torrentInfo->trackerInfo.buffer[0] = 0;
    }
    freeaddrinfo(result);

    return sfd;
}

int connectToTracker(TorrentInfo* torrentInfo) {
    int announceUrlLen;
    const char* announceUrl = getAnnounceUrl(torrentInfo->metadata, torrentInfo->trackerInfo.announceUrlIndex, &announceUrlLen);
    if (!announceUrl) {
        VERBOSE("Could not connect to any announce urls\n");
        torrentInfo->trackerInfo.announceUrlIndex = 0;
        return -1;
    }
    VERBOSE("Trying to connect to announce url '%.*s'\n", announceUrlLen, announceUrl);
    short port;
    const char* host = getHostAndPortFromUrl(announceUrl, announceUrlLen, &port);
    int ret = -1;
    if (host) {
        ret = connectToSpecificTracker(torrentInfo, host, port);
    }
    if (ret == -1 ) {
        INFO("Failed to parse host info url %d for %.*s\n", torrentInfo->trackerInfo.announceUrlIndex, announceUrlLen, announceUrl);
        announceSucceeded(torrentInfo->metadata, &torrentInfo->trackerInfo.announceUrlIndex, 0);
        return connectToTracker(torrentInfo);
    }
    return ret;
}

int listenOnPort(int port) {
    TRACE("Attempting to listen on port %d\n", port);
    int sfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sfd == -1) {
        ERROR("Failed to create socket");
        return -1;
    }
#ifdef ENABLE_SOCKET_REUSE
    int enable = 1;
    if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1) {
        ERROR("Could not set SO_REUSEADDR; program won't be able to restart quickly");
    }
#endif

    struct sockaddr_in addr = {.sin_family = AF_INET, .sin_addr.s_addr = htonl(INADDR_ANY), .sin_port = htons(port)};
    if(bind(sfd, (struct sockaddr*)&addr, sizeof(addr)) == -1){
        VERBOSE("Failed to bind to socket\n");
        close(sfd);
        return -1;
    } else if(listen(sfd, MAX_NUM_PEERS) == -1) {
        ERROR("Failed to listen to socket");
        close(sfd);
        return -1;
    }
    return sfd;
}

#define A_OR_B(A,B) ((A) ? (A) : (B))
int listenForClients(TorrentInfo* torrentFileInfo) {
    int incomingFD = -1;
    int port;
    for(port = A_OR_B(torrentFileInfo->settings.minPort, MIN_PORT); port <= A_OR_B(torrentFileInfo->settings.maxPort, MAX_PORT); port++) {
        incomingFD = listenOnPort(port);
        if(incomingFD != -1)
            break;
    }
    if(incomingFD == -1) {
        ERROR("Failed to find port to listen on");
        return -1;
    }
    VERBOSE("Listening on port %d\n", port);
    torrentFileInfo->port = port;
    torrentFileInfo->listenFd = incomingFD;
    return incomingFD;
}

void announceToTrackerQuick(TorrentInfo* torrentInfo, AnnounceStatus announceStatus, int quick) {
    if (torrentInfo->trackerInfo.fd != -1) {
        close(torrentInfo->trackerInfo.fd);
        torrentInfo->trackerInfo.fd = -1;
    }
    int len;
    if (!getAnnounceUrl(torrentInfo->metadata, torrentInfo->trackerInfo.announceUrlIndex, &len))
        return;
    int sfd = connectToTracker(torrentInfo);
    if (sfd == -1) {
        return;
    }
    prepareMessageToSendToTracker(torrentInfo, announceStatus);
    if(quick) {
        int len = strlen(torrentInfo->trackerInfo.buffer);
        int ret, offset = 0;
        while(offset != len && (ret = safeWrite(sfd, torrentInfo->trackerInfo.buffer + offset, len - offset))) {
            if(ret == -1)
                break;
            offset += ret;
        }
        close(sfd);
        torrentInfo->trackerInfo.fd = -1;
        int success = offset == ret;
        announceSucceeded(torrentInfo->metadata, &torrentInfo->trackerInfo.announceUrlIndex, success);
        if (!success) {
            return announceToTrackerQuick(torrentInfo, announceStatus, quick);
        }
    }
}

int processAnnounceResponse(TorrentInfo* torrentInfo, const char*buffer, int ret, int addPeers) {
    VERBOSE("Read %d bytes from tracker\n", ret);
    const char* ptr = strstr(buffer, "\r\n\r\n");
    if(ptr) {
        ptr+=4;
        ret-=ptr-buffer;
    }
    if(!ptr) {
        return -1;
    }
    assert(ptr);

    bencodeState state = {ptr, ptr + ret};
    bencodeDictEnter(&state);

    TorrentTrackerInfo* trackerInfo = &torrentInfo->trackerInfo;
    int newPeers = 0;
    while(bencodeHasNext(&state)) {
        const char *key;
        int klen;
        bencodeReadString(&state, &key, &klen);
        LOG_ALL("Found key: '%.*s'\n", klen, key);
        if(strncmp("failure", key, klen) == 0 || strncmp("warning", key, klen) == 0) {
            const char* s;
            int slen;
            if(bencodeReadString(&state, &s, &slen) != -1)
                VERBOSE("%.*s: %s\n", klen, key, s, slen);
        } else if(strncmp("interval", key, klen) == 0) {
            bencodeReadInt(&state, &trackerInfo->announceInterval);
        } else if(strncmp("min interval", key, klen) == 0) {
            bencodeReadInt(&state, &trackerInfo->announceMinInterval);
        } else if(strncmp("peers", key, klen) == 0) {
            if(state.str[0] == 'd') {
                VERBOSE("Received non-compact response\n");
                bencodeSkipNext(&state);
            } else {
                const char* s;
                bencodeReadString(&state, &s, &klen);
                TRACE("Got %d peers from tracker\n", klen / 6);
                if (addPeers) {
                    for(int i = 0 ; i < klen; i+=6) {
                        if(addPeerAndConnect(torrentInfo, *((int*)(s + i)), ntohs(*((short*)(s + i + 4)))) != -1)
                            newPeers++;
                    }
                }
                TRACE("Finished adding peers\n");
            }
        } else if(strncmp("tracker id", key, klen) == 0) {
            int len;
            const char *s;
            if(bencodeReadString(&state, &s, &len) != -1) {
                strncpy(trackerInfo->trackerId, s, MIN(len, sizeof(trackerInfo->trackerId) -1));
            }
        } else {
            bencodeSkipNext(&state);
        }
    }
    VERBOSE("Added %d new peers from tracker for a total of %d\n", newPeers, torrentInfo->numPeers);
    return hasBencodeError(&state) ? -1 : newPeers;
}

int addPeersFromPeerList(TorrentInfo* torrentInfo) {
    const char* peerList = torrentInfo->settings.peerList;
    if(!peerList)
        return 0;
    int skipTracker = peerList[0] != '+';
    if(!skipTracker)
        peerList++;
    const char* end = torrentInfo->settings.peerList + strlen(peerList);
    while(peerList && peerList[0]) {
        short port;
        const char* next = strchr(peerList, ',');
        char* host = getHost(peerList, MAX(next, end), &port);
        struct addrinfo *result = NULL;
        struct addrinfo hint = {0};
        hint.ai_family = AF_INET;
        hint.ai_socktype = SOCK_STREAM;
        if(getaddrinfo(host, NULL, &hint, &result)) {
            ERROR("Failed to get addr info");
        } else {
            addPeerAndConnect(torrentInfo, ((struct sockaddr_in *)result->ai_addr)->sin_addr.s_addr, port);
            freeaddrinfo(result);
        }
        peerList = next;
    }
    return skipTracker;
}

int acceptPeerConnection(TorrentInfo* torrentInfo, int socketFd) {
    struct sockaddr_in addr;
    unsigned int len = sizeof(addr);
    int fd = accept(socketFd, (struct sockaddr*)&addr, &len);
    if (fd == -1) {
        ERROR("Failed to accept connection");
        return -1;
    }
    int n = addPeer(torrentInfo, addr.sin_addr.s_addr, ntohs(addr.sin_port), fd);
    if (n == -1) {
        close(fd);
        TRACE("Either we already have max peers or an unexpected error occurred");
        return -1;
    }
    return n;
}

const char* announceStatusStrings[] = { "started", "", "stopped", "completed"};

void prepareMessageToSendToTracker(TorrentInfo* torrentInfo, AnnounceStatus announceStatus) {
    int announceUrlLen;
    const char* announceUrl = getAnnounceUrl(torrentInfo->metadata, torrentInfo->trackerInfo.announceUrlIndex, &announceUrlLen);
    VERBOSE("Sending message to tracker %.*s; status %s\n", announceUrlLen, announceUrl, announceStatusStrings[announceStatus]);

    short port;
    const char* host = getHostAndPortFromUrl(announceUrl, announceUrlLen, &port);
    char* pathStart = strstr(announceUrl, "://") + 3;

    while (pathStart < announceUrl + announceUrlLen && *pathStart != '/') {
        pathStart++;
    }

    int msgLen = snprintf(torrentInfo->trackerInfo.buffer, sizeof(torrentInfo->trackerInfo.buffer), "GET %.*s?info_hash=%.*s&peer_id=%.*s&port=%d&uploaded=%ld&downloaded=%ld&left=%ld&compact=1&event=%s&trackerid=%s HTTP/1.1\r\nHost: %s:%d\r\nUser-Agent: nanotorrent/"VERSION"\r\nAccept: */*\r\n\r\n",
            announceUrlLen - (int)(pathStart - announceUrl), pathStart,
            3 * HASH_SIZE, torrentInfo->metadata->encodedInfoHash,
            CLIENT_ID_LEN, torrentInfo->clientId,
            torrentInfo->port,
            torrentInfo->trackerInfo.uploaded, torrentInfo->trackerInfo.downloaded, torrentInfo->trackerInfo.left,
            announceStatusStrings[announceStatus],
            torrentInfo->trackerInfo.trackerId, host, port);
    torrentInfo->trackerInfo.buffer[msgLen] = 0;

    TRACE("Sending http request to %s:%d; body: %s\n", host, port, torrentInfo->trackerInfo.buffer);
    torrentInfo->trackerInfo.sent = 0;
    torrentInfo->trackerInfo.read = 0;
}

int addPeer(TorrentInfo* torrentInfo, unsigned int ip, unsigned short port, int fd) {
    if(!ip || !port)
        return -1;

    for(int n = 0; n < torrentInfo->numPeers; n++) {
        if(ip == torrentInfo->peers[n].ip && (port == torrentInfo->peers[n].port || port == torrentInfo->peers[n].localPort)) {
            if(fd != -1) {
                TRACE("We already had a connection to this peer on fd %d", torrentInfo->peers[n].fd);
            }
            TRACE("Peer already saved at index %d\n", n);
            return -1;
        }
    }
    if(torrentInfo->numPeers >= MAX_NUM_PEERS) {
        VERBOSE("Can't add any more peers because we are already at the max\n");
        return -1;
    }
    int n = torrentInfo->numPeers;
    memset(&torrentInfo->peers[n], 0, sizeof(torrentInfo->peers[n]));
    memset(torrentInfo->peers[n].fastPieceSet, 0xFF, sizeof(torrentInfo->peers[n].fastPieceSet));
#ifndef NDEBUG
    memset(torrentInfo->peers[n].fastPieceSetOutgoing, 0xFF, sizeof(torrentInfo->peers[n].fastPieceSetOutgoing));
#endif
    torrentInfo->peers[n].suggestedPiece = -1;
    torrentInfo->peers[n].fd = fd;
    torrentInfo->peers[n].ip = ip;
    torrentInfo->peers[n].port = port;
    torrentInfo->peers[n].localPort = fd == -1 ? port : 0;
    torrentInfo->peers[n].flags = 0;
    torrentInfo->peers[n].lastReceiveMessageTimeMS = torrentInfo->peers[n].lastSentMessageTimeMS = -1;

    initBitMap(&torrentInfo->peers[n].bitmap, torrentInfo->metadata->numPieces);

    for(int i = 0; i < MAX_PEER_PEER_OUTSTANDING_PIECES; i++) {
        initBitMap(&torrentInfo->peers[n].requestedBlocksBitmap[i], (torrentInfo->metadata->pieceLength + torrentInfo->settings.blockSize - 1) / torrentInfo->settings.blockSize);
    }

    torrentInfo->numPeers++;

    VERBOSE_PEER_MSG("Added peer", (&torrentInfo->peers[n]));
    TRACE("Total num peers %d\n", torrentInfo->numPeers);
    return n;
}

void removePeer(TorrentInfo* torrentInfo, int index) {
    assert(index < torrentInfo->numPeers);

    VERBOSE_PEER_MSG("Removing peer", &torrentInfo->peers[index]);
    close(torrentInfo->peers[index].fd);
    free(torrentInfo->peers[index].bitmap.data);

    if(torrentInfo->peers[index].scratchBuffer)
        free(torrentInfo->peers[index].scratchBuffer);
    for(int i = 0; i < MAX_PEER_PEER_OUTSTANDING_PIECES; i++) {
        free(torrentInfo->peers[index].requestedBlocksBitmap[i].data);
    }
    assert(torrentInfo->numPeers);
    --torrentInfo->numPeers;
    if(index < torrentInfo->numPeers) {
        memcpy(torrentInfo->peers + index, torrentInfo->peers + torrentInfo->numPeers, sizeof(Peer));
    }
    VERBOSE("Peers left %d\n", torrentInfo->numPeers);
}
