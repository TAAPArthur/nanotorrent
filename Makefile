
DEBUG_FLAGS_1 = -DDEBUG -g
DEBUG_FLAGS_0 = -DNDEBUG

DEBUG = 0
CPPFLAGS = $(DEBUG_FLAGS_$(DEBUG))
LDFLAGS=-g -static

CFLAGS= -Wall -Werror -Wno-parentheses

HEADERS := bencode.h config.h bitmap.h util.h
SRCS := nanotorrent.c disk.c torrent_file.c stats.c connect.c extensions.c
OBJS := $(SRCS:.c=.o)

all: nanotorrent

install: nanotorrent
	install -D nanotorrent $(DESTDIR)/usr/bin/nanotorrent
	ln -f $(DESTDIR)/usr/bin/nanotorrent $(DESTDIR)/usr/bin/nt

$(OBJS) : $(HEADERS)

nanotorrent: $(OBJS)

TEST_SRCS := Test/bitmap_unit.c Test/torrent_file_unit.c Test/disk_unit.c Test/stats_unit.c Test/misc_unit.c Test/connection_unit.c Test/peer_unit.c Test/events_unit.c Test/extensions_unit.c Test/advanced_unit.c
TEST_OBJS := $(TEST_SRCS:.c=.o)

unit_test: CPPFLAGS = $(DEBUG_FLAGS_1)
unit_test: Test/tests.o $(TEST_OBJS) $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@

test: unit_test
	./$^


clean:
	rm -f Test/*.o *.o nanotorrent unit_test
