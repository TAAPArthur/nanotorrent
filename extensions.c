#include "bencode.h"
#include "extensions.h"
#include "nanotorrent.h"
#include "util.h"

const char* extToName[] = {
    [DONT_HAVE] = "lt_donthave"
};

const char* allocExtensionMessage() {
    char* mem = malloc(256);
    memset(mem, 0, 256);
    char* next = mem;

    next = writeDictStartMem(next);
        next = bencodeWriteStringMem(next, "m");
        next = writeDictStartMem(next);
            for(int i = 0; i < LEN(extToName); i++)  {
                if (extToName[i]) {
                    next = bencodeWriteStringMem(next, extToName[i]);
                    next = bencodeWriteIntMem(next, i);
                }
            }
        next = writeDictEndMem(next);
        next = bencodeWriteStringMem(next, "reqq");
        next = bencodeWriteIntMem(next, SEND_QUEUE_LEN/2);
        next = bencodeWriteStringMem(next, "v");
        next = bencodeWriteStringMem(next, NAME " " VERSION);
    next = writeDictEndMem(next);
    *next = 0;
    next++;
    return realloc(mem, next - mem);
}

void receiveExtensionHandshake(const char* start, int len, Peer* peer) {
    bencodeState state = {start, start + len};
    bencodeDictEnter(&state);

    while(bencodeHasNext(&state)) {
        const char *key;
        int klen;
        long value;
        bencodeReadString(&state, &key, &klen);
        if (strncmp("m", key, klen) == 0) {
            bencodeDictEnter(&state);
            while(bencodeHasNext(&state)) {
                bencodeReadString(&state, &key, &klen);
                bencodeReadInt(&state, &value);
                for(int i = 0; i < LEN(extToName); i++)  {
                    if (extToName[i] && strlen(extToName[i]) == klen && strncmp(extToName[i], key, klen) == 0) {
                        peer->extensions[i] = value;
                        break;
                    }
                }
            }
        } else if (strncmp("rreq", key, klen) == 0) {
            bencodeReadInt(&state, &value);
        } else {
            VERBOSE("ignoring key %.*s\n", key, klen);
            bencodeSkipNext(&state);
        }
    }

    bencodeExit(&state);
}

int translateExtensionToPeerFormat(const Peer* peer, MessageType ext){
    return peer->extensions[ext];
}


static long long messageTypeExtensions[NUM_MESSAGE_TYPES] = {
    [SUGGEST] = FAST_EXTENSION_BIT,
    [HAVE_ALL] = FAST_EXTENSION_BIT,
    [HAVE_NONE] = FAST_EXTENSION_BIT,
    [REJECT] = FAST_EXTENSION_BIT,
    [ALLOWED_FAST] = FAST_EXTENSION_BIT,

    [EXTENSION] = EXTENSION_PROTOCOL_BIT,
    [DONT_HAVE] = EXTENSION_PROTOCOL_BIT,
};

int isExtensionProtocalMessage(MessageType type) {
    return EXTENSION <= type && type < MAX_NUM_EXTENSION;
}

int doesPeerSupportExtension(const Peer* peer, MessageType type){
    if (!isExtensionProtocalMessage(type)) {
        return (peer->extensionBytes & messageTypeExtensions[type]) == messageTypeExtensions[type];
    }
    switch (type) {
        case EXTENSION:
            return peer->extensionBytes & EXTENSION_PROTOCOL_BIT;
        default:
            return translateExtensionToPeerFormat(peer, type);
    }
}
