#include "bencode.h"
#include "config.h"
#include "hash.h"
#include "nanotorrent.h"
#include "util.h"

#include <assert.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

static int computeHashes(int fd, char*buffer, unsigned int pieceSize, int rem, char* hashBuffer, int* hashBufferIndex, unsigned long fileSize, int force) {
    int size;
    assert(pieceSize != rem);
    while((size = safeRead(fd, buffer + rem, pieceSize - rem)) == pieceSize - rem || (force && size > 0)) {
        getHash(buffer, size + rem, hashBuffer + *hashBufferIndex);
        *hashBufferIndex += HASH_SIZE;
        rem = 0;
        if (size > fileSize)
            break;
        fileSize -= size;
    }
    assert(size != -1);
    return size + rem;
}

static unsigned long writeFileInfo(int fd, const char*name, const char** files, unsigned int numFiles, char **hashBufferStart, unsigned int pieceSize) {
    char* buffer = malloc(pieceSize);
    int hashBufferIndex = 0;
    unsigned long len = 0;
    int fileFd;
    unsigned long hashBufferMaxSize;
    struct stat fileStat;
    if(!files || !numFiles) {
        assert(numFiles <= 1);
        if ((fileFd = open(name, O_RDONLY)) == -1 || fstat(fileFd, &fileStat) == -1) {
            FATAL_ERROR("Failed to open file/get file info");
            return -1;
        }
        len = fileStat.st_size;
        hashBufferMaxSize = (len + pieceSize - 1 )/ pieceSize * HASH_SIZE;
        if (!(*hashBufferStart = malloc(hashBufferMaxSize)))
            return -1;

        computeHashes(fileFd, buffer, pieceSize, 0, *hashBufferStart, &hashBufferIndex, len, 1);
        close(fileFd);

        bencodeWriteString(fd, "length");
        bencodeWriteInt(fd, len);
    } else {
        assert(files);
        int rem = 0;
        bencodeWriteString(fd, "files");
        writeListStart(fd);
        hashBufferMaxSize = 1;
        for(unsigned int i = 0; i < numFiles && files[i]; i++) {
            if ((fileFd = open(files[i], O_RDONLY)) == -1 || fstat(fileFd, &fileStat) == -1) {
                FATAL_ERROR("Failed to open file or to get file length");
                return -1;
            }
            hashBufferMaxSize += HASH_SIZE * (fileStat.st_size + pieceSize - 1 ) / pieceSize;
            *hashBufferStart = realloc(*hashBufferStart, hashBufferMaxSize);
            if (!*hashBufferStart)
                return -1;

#ifndef NDEBUG
            static int lastInode;
            if (i && rem == 0 && fileStat.st_size % pieceSize == 0 && lastInode == fileStat.st_ino) {
                unsigned long lenOfHashesForFile = HASH_SIZE * (fileStat.st_size / pieceSize);
                memcpy(*hashBufferStart + hashBufferIndex, *hashBufferStart + hashBufferIndex - lenOfHashesForFile, lenOfHashesForFile);
                hashBufferIndex += lenOfHashesForFile;
            } else {
#endif
            rem = computeHashes(fileFd, buffer, pieceSize, rem, *hashBufferStart, &hashBufferIndex, fileStat.st_size, i == numFiles -1 || !files[i+1]);

#ifndef NDEBUG
            }
            lastInode = fileStat.st_ino;
#endif

            close(fileFd);
            writeDictStart(fd);
                bencodeWriteString(fd, "path");
                writeListStart(fd);
                    const char* component = files[i];
                    while(1) {
                        const char* next = strchr(component, '/');
                        if(!next) {
                            bencodeWriteString(fd, component);
                            break;
                        }
                        if(component != next)
                            bencodeWriteStringN(fd, component, next - component);
                        component = next + 1;
                    };
                writeListEnd(fd);

                bencodeWriteString(fd, "length");
                bencodeWriteInt(fd, fileStat.st_size);
                len += fileStat.st_size;
            writeDictEnd(fd);
        }
        writeListEnd(fd);
    }
    free(buffer);
    return len;
}
static const char* writeAnnounceToken(int fd, const char* str, char* c) {
    assert(str);
    const char* s = str;
    while (*s && !(*s == ' ' || *s == '\n')) {s++;}
    assert(s != str);
    bencodeWriteStringN(fd, str, s - str);
    *c = *s;
    while (*s && *s == *c) { s++; }
    return s;
}
static void writeAnnounceUrl(int fd,  const char* announceUrl) {
    bencodeWriteString(fd, "announce");
    char c;
    if (!writeAnnounceToken(fd, announceUrl, &c))
        return;

    bencodeWriteString(fd, "announce-list");
    writeListStart(fd);
    const char* next = announceUrl;
    while (*next) {
        writeListStart(fd);
        do {
            next = writeAnnounceToken(fd, next, &c);
        } while(*next && c == ' ');
        writeListEnd(fd);
    };
    writeListEnd(fd);
}

int createTorrentFile(int fd,  const char* announceUrl, const char* name, const char** files, unsigned int numFiles, unsigned int pieceSize) {
    assert(announceUrl);
    unsigned long len = 0;

    char *hashBuffer = NULL;

    if (pieceSize > MAX_BLOCK_SIZE * sizeof(long) * 8) {
        DIE(-1, "Piece size is too big 0x%X; max 0x%X\n", pieceSize);
        return -1;
    }

    writeDictStart(fd);
        writeAnnounceUrl(fd, announceUrl);
        bencodeWriteString(fd, "info");
        writeDictStart(fd);
            len = writeFileInfo(fd, name, files, numFiles, &hashBuffer, pieceSize);
            if (len == -1)
                return -1;
            bencodeWriteString(fd, "name");
            bencodeWriteString(fd, name);
            bencodeWriteString(fd, "piece length");
            bencodeWriteInt(fd, len < pieceSize ? len : pieceSize);
            bencodeWriteString(fd, "pieces");
            unsigned long numPieces = (len + pieceSize - 1) / pieceSize;
            dprintf(fd,"%ld:", HASH_SIZE * numPieces);
            safeWrite(fd, hashBuffer, HASH_SIZE * numPieces);
        writeDictEnd(fd);
    writeDictEnd(fd);
    free(hashBuffer);
    return len;
}

const char* findNthFlattenedElement(bencodeState *state, int n, int* slen, const char** listHead) {
    int i = 0;
    bencodeListEnter(state);
    while(bencodeHasNext(state)) {
        bencodeListEnter(state);
        const char* head = state->str;
        while(bencodeHasNext(state)) {
            if (i == n) {
                if (listHead) {
                    *listHead = head;
                }
                const char* key;
                bencodeReadString(state, &key, slen);
                return key;
            } else {
                bencodeSkipNext(state);
                i++;
            }
        }
        bencodeExit(state);
    }
    bencodeExit(state);
    return NULL;
}

const char* getAnnounceUrl(const TorrentMetadata * torrentMetadata, int n, int* slen) {
    const char* key;
    if (!torrentMetadata->announceUrlList) {
        bencodeState state = {torrentMetadata->announceUrlStr, torrentMetadata->end};
        bencodeReadString(&state, &key, slen);
        return key;
    }
    bencodeState state = {torrentMetadata->announceUrlList, torrentMetadata->end};
    return findNthFlattenedElement(&state, n, slen, NULL);
}
void announceSucceeded(const TorrentMetadata * torrentMetadata, int * n, int success) {
    // TODO on success move url to head of list
    if(success)
        *n = 0;
    else
        n[0]++;
}

int readAndParseTorrentFile(TorrentMetadata * torrentMetadata, int fd){
    struct stat fileStat;
    fstat(fd, &fileStat);
    if((torrentMetadata->data = mmap(NULL, fileStat.st_size, PROT_READ, MAP_SHARED, fd, 0)) == MAP_FAILED) {
        FATAL_ERROR("Could not load torrent metadata");
    }
    torrentMetadata->end = torrentMetadata->data + fileStat.st_size;
    bencodeState _state = {torrentMetadata->data, torrentMetadata->end};
    bencodeState *state = &_state;
    bencodeDictEnter(state);

    while(bencodeHasNext(state)) {
        const char* key;
        int klen;
        bencodeReadString(state, &key, &klen);
        LOG_ALL("Found key: '%.*s'\n", klen, key);
        if(strncmp("info", key, klen) == 0) {
            int dictLen = getBencodeLen(state);
            if(dictLen == -1) {
                LOG_ALL("Could not read len: '%.*s'\n", klen, key);
                return -1;
            }
            LOG_ALL("Computing info hash\n");
            if(getHash(state->str, dictLen, torrentMetadata->infoHash) == -1)
                return -1;

            for(int i = 0; i < HASH_SIZE; i++) {
                char buffer[4];
                // snprintf always adds a NULL to the end so just use a temp array
                snprintf(buffer, 4, "%%%02hhX", torrentMetadata->infoHash[i]);
                memcpy(torrentMetadata->encodedInfoHash + i * 3, buffer, 3);

            }
            bencodeDictEnter(state);

            while(bencodeHasNext(state)) {
                bencodeReadString(state, &key, &klen);
                LOG_ALL("Found info key: '%.*s'\n", klen, key);
                if(strncmp("piece length", key, klen) == 0) {
                    bencodeReadInt(state, &torrentMetadata->pieceLength);
                } else if(strncmp("pieces", key, klen) == 0) {
                    bencodeReadString(state, &torrentMetadata->pieces, &klen);
                    if(klen % HASH_SIZE || !klen ) {
                        DIE(EXIT_MALFORMED, "Size of pieces string isn't a multiple of %d or is 0: %d\n", HASH_SIZE, klen);
                        return -1;
                    }
                    torrentMetadata->numPieces = klen / HASH_SIZE;
                } else if(strncmp("name", key, klen) == 0) {
                    bencodeReadString(state, &torrentMetadata->name, &torrentMetadata->nameLen);
                } else if(strncmp("length", key, klen) == 0) {
                    bencodeReadInt(state, &torrentMetadata->totalLength);
                } else if(strncmp("md5sum", key, klen) == 0) {
                    LOG_ALL("ignoring key\n");
                    bencodeSkipNext(state);
                } else if(strncmp("files", key, klen) == 0) {
                    VERBOSE("Detected multi file torrent\n");
                    bencodeListEnter(state);
                    torrentMetadata->pathList = state->str;
                    while(bencodeHasNext(state)) {
                        bencodeDictEnter(state);
                        while(bencodeHasNext(state)) {
                            bencodeReadString(state, &key, &klen);
                            if(strncmp("length", key, klen) == 0) {
                                unsigned long length = 0;
                                bencodeReadInt(state, &length);
                                torrentMetadata->totalLength += length;
                            } else if(strncmp("md5sum", key, klen) == 0) {
                                LOG_ALL("ignoring key\n");
                                bencodeSkipNext(state);
                            } else if(strncmp("path", key, klen) == 0) {
                                bencodeSkipNext(state);
                            } else {
                                LOG_ALL("Unknown info key %.*s\n", klen, key);
                                bencodeSkipNext(state);
                            }
                        }
                        bencodeExit(state);
                        torrentMetadata->fileCount++;
                    }
                    bencodeExit(state);
                } else {
                    LOG_ALL("Unknown info key %.*s\n", klen, key);
                    bencodeSkipNext(state);
                }
            }

            bencodeExit(state);
        } else if(strncmp("announce", key, klen) == 0) {
            if (!torrentMetadata->announceUrlList)
                torrentMetadata->announceUrlStr = state->str;
            bencodeSkipNext(state);
        } else if(strncmp("announce-list", key, klen) == 0) {
            torrentMetadata->announceUrlStr = NULL;
            torrentMetadata->announceUrlList = state->str;
            bencodeSkipNext(state);
        } else {
            const char* start = state->str;
            bencodeSkipNext(state);
            LOG_ALL("Unknown key %.*s; Value %.*s\n", klen, key, state->str - start, start);
        }
    }
    if(torrentMetadata->fileCount == 0)
        torrentMetadata->fileCount++;

    if(hasBencodeError(state))
        return -1;

    assert(torrentMetadata->numPieces == (torrentMetadata->totalLength + torrentMetadata->pieceLength - 1 ) / torrentMetadata->pieceLength);
    return 0;
}

int openAndParseTorrentFile(TorrentMetadata * torrentFileInfo, const char* name){
    TRACE("Attempting to open %s\n", name);
    int fd = open(name, O_RDONLY);
    if(fd == -1) {
        FATAL_ERROR("Could not open file");
    }
    int ret = readAndParseTorrentFile(torrentFileInfo, fd);
    close(fd);
    return ret;
}

static char pathBuffer[MAX_ALLOWED_PATH];
static void getNextPathInfo(bencodeState* state,char**path, unsigned long*length) {
    assert(length);

    bencodeDictEnter(state);
    *length = 0;
    int currentLen = 0;
    pathBuffer[0] = 0;
    while(bencodeHasNext(state)) {
        const char * key;
        int klen;
        bencodeReadString(state, &key, &klen);
        if(strncmp("length", key, klen) == 0) {
            bencodeReadInt(state, length);
        } else if(strncmp("path", key, klen) == 0) {
            bencodeListEnter(state);
            while(bencodeHasNext(state)) {
                bencodeReadString(state, &key, &klen);
                if(currentLen + klen + 1 < sizeof(pathBuffer) - 1) {
                    if(currentLen) {
                        strcat(pathBuffer, "/");
                    }
                    strncat(pathBuffer, key, klen);
                    currentLen += klen + 1;
                }
            }
            bencodeExit(state);
        }
    }
    bencodeExit(state);
    *path = pathBuffer;
}

char* getPieceRange(const TorrentMetadata* torrentMetadata, int fileIndex, const char*targetPath, int* startingPieceIndex, int*endingPieceIndex, unsigned long*length, unsigned long* runningLength) {
    assert((fileIndex == -1) ^ !targetPath);
    if(targetPath && targetPath[0] == '/') {
        assert(fileIndex == -1);
        char* end = NULL;
        fileIndex = strtol(targetPath + 1, &end, 10);
        if(!*end && fileIndex >=0) {
            VERBOSE("Loading file %d since %s was given '%s'\n", fileIndex , targetPath, end);
            targetPath = NULL;
        }
        else {
            fileIndex = -1;
            targetPath++;
        }
    }

    if (!torrentMetadata->pathList) {
        assert(torrentMetadata->fileCount == 1);
        if(startingPieceIndex)
            *startingPieceIndex = 0;
        if(endingPieceIndex)
            *endingPieceIndex = torrentMetadata->numPieces;
        strncpy(pathBuffer, torrentMetadata->name, torrentMetadata->nameLen);
        if(length)
            *length = torrentMetadata->totalLength;
        if(runningLength)
            *runningLength= 0;
        char* path = pathBuffer + (pathBuffer[0] == '/');
        if(!targetPath || strcmp(path, targetPath) == 0)
            return path;
        return NULL;
    }
    bencodeState state = {torrentMetadata->pathList, torrentMetadata->end};
    char* path;
    int i = 0;
    unsigned long runningLen = 0;
    unsigned long len = 0;
    while(bencodeHasNext(&state)) {
        getNextPathInfo(&state, &path, &len);
        if( i == fileIndex || (targetPath && strcmp(path, targetPath) == 0)) {
            if(startingPieceIndex)
                *startingPieceIndex = runningLen / torrentMetadata->pieceLength;
            if(endingPieceIndex)
                *endingPieceIndex = (runningLen + len + torrentMetadata->pieceLength - 1) / torrentMetadata->pieceLength;
            if(length)
                *length = len;
            if(runningLength)
                *runningLength= runningLen;
            return path;
        }
        runningLen += len;
        i++;
    }
    return NULL;
}

void dumpTorrentInfo(const TorrentMetadata* torrentMetadata) {
        printf("Hash:\t%.*s\n", 3 * HASH_SIZE, torrentMetadata->encodedInfoHash);
        printf("Name:\t%.*s\n", torrentMetadata->nameLen, torrentMetadata->name);
        printf("AnnounceUrls:\n");
        const char* announceUrl;
        for (int i = 0, slen; (announceUrl = getAnnounceUrl(torrentMetadata, i++, &slen)); i++) {
            printf("\t%.*s\n", slen, announceUrl);
        }
        unsigned long length;
        for(int i = 0; i < torrentMetadata->fileCount; i++) {
            char* path = getPieceRange(torrentMetadata, i, NULL, NULL, NULL, &length, NULL);
            printf("File_%04d_PATH:\t%s\n", i, path);
            printf("File_%04d_SIZE:\t%ld\n", i, length);
        }
}
