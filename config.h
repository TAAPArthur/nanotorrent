#ifndef CONFIG_H
#define CONFIG_H
#include "extensions.h"

#define MAX_NUM_PEERS                  32


// Spec
#define CLIENT_ID_LEN 20
#define HASH_SIZE 20

// Recommendation
#define DEFAULT_BLOCK_SHIFT            14
#define MAX_BLOCK_SHIFT                17

#define MIN_BLOCK_SIZE                 (1 << DEFAULT_BLOCK_SHIFT)
#define MAX_BLOCK_SIZE                 (1 << MAX_BLOCK_SHIFT)

#define PIECE_SHIFT                    19

// The range of ports to attempt is [MIN_PORT, MAX_PORT]
#define MIN_PORT 6881
#define MAX_PORT 6889


#define PSTR "BitTorrent protocol"

#define EXTENSION_BYTES (EXTENSION_PROTOCOL_BIT)

#define DEFAULT_CONNECTION_TIMEOUT_MS     20000
#define PIECE_TIMEOUT_MS                  10000
#define MAX_NUM_CONSECUTIVE_TIMEOUTS      3
#define MAX_PEER_PEER_OUTSTANDING_PIECES  2
#define MAX_PEER_PEER_OUTSTANDING_BLOCKS  32
#define SEND_QUEUE_LEN                    256

#ifndef DEBUG
#define STATS_WINDOW_MS                 1024
#else
#define STATS_WINDOW_MS                 128
#endif
#define STATS_WINDOWS                   8

#define RANDOM_PIECE_WINDOW             4

#define DEFAULT_CACHE_SIZE              (100 << 20)


#define ENABLE_SOCKET_REUSE

#define MAX_ALLOWED_PATH                4096

#define GOOD_DOWNLOAD_RATE              (1<<17)
#define SLOW_DOWNLOAD_RATE              MAX_BLOCK_SIZE

#ifndef DEBUG
#define MIN_KEEP_ALIVE_TIME             2000
#else
#define MIN_KEEP_ALIVE_TIME             0
#endif

#endif
