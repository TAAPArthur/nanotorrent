# Nano Torrent
## torrent client for streaming media
The goal of nt is to provide a portable BitTorrent client that supports streaming media.

## Requirements
- c99 complier
- picohash.h (included in this repo)

## Install
```
make && make install
```

## Usage

### Steaming
Download the specified torrent file and pipe directly to mpv without saving to disk
`nt "$TORRENT_FILE" | mpv -`

Nothing is saved, so it will start from the beginning each time. If you want to remember progress
use `-F` or `-d`

`nt -F cache_file "$TORRENT_FILE" | mpv -`
`nt -d download_dir "$TORRENT_FILE" | mpv -`
The former will save the data as one large file. The former will save each file separately in download_dir

Note that in the above examples, nt and mpv will never exit unless an error occurs.
Use '-q' to have nt quit immediately when it finishes.
`nt -q "$TORRENT_FILE" | mpv -`

If nt should die when mpv exists, use `-s` or `-S`.
`nt -s "mpv -" "$TORRENT_FILE"`
`nt -S "mpv -" "$TORRENT_FILE"`
`-s` leaves nt in the foreground while `-S` puts the specified program in the foreground.

### Daemon
If you want to use nt like a regular torrent client that runs in the background
`nt -n "$TORRENT_FILE"`
And you probably want to use `-F` or `-d` to actually save the data somewhere

### Status
Status can be obtained by sending signals `SIGUSR1` and `SIGUSR2`

### Config
`-B` disables creation and loading of bitmap files
`-eN`  Allows the next N pieces (not files) to be additionally downloaded but not output
In memory cache can be controlled with

### MISC
To list files in the torrent
`nt -l "$TORRENT_FILE"`

To list misc info about the torrent
`nt -I "$TORRENT_FILE"`

To Create a torrent file
`nt -C announce_url "$TORRENT_FILE" files...`

See `nt --help` for a list of all commands


# Other work
* [btpd](https://github.com/btpd/btpd) - This project was born out of frustation with btpd. It was relatively large and complicated and would undergo period of no transfers even when peers could be found. In addition it wasn't suited to streaming and required a daemon to be present so it was hard to integrate in other projects
* [amt](https://codeberg.org/TAAPArthur/amt) - nt was designed for use with amt to enable streaming and downloading from torrent files.

# Acknowledgements
https://wiki.theory.org/BitTorrentSpecification
