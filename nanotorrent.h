#ifndef NANO_TORRENT_H
#define NANO_TORRENT_H

#include "bitmap.h"
#include "config.h"

#define NAME "NanoTorrent"
#define VERSION "0.01"

typedef enum {
    // Message type defined by the spec
    // These indicate how to parse a given message
    CHOKE,
    UNCHOKE,
    INTERESTED,
    UNINTERESTED,
    HAVE,
    BITFIELD,
    REQUEST,
    PIECE,
    CANCEL,

    SUGGEST      = 0xD,
    HAVE_ALL     = 0xE,
    HAVE_NONE    = 0xF,
    REJECT       = 0x10,
    ALLOWED_FAST = 0x11,

    EXTENSION = 20,
    DONT_HAVE = 21,
    MAX_NUM_EXTENSION,

    KEEP_ALIVE = 31,
    NUM_MESSAGE_TYPES,
} MessageType;

typedef struct {
    unsigned pieceIndex;
    unsigned pieceOffset;
    unsigned len;
} BlockInfo;

typedef enum {
    UPLOAD, DOWNLOAD, NUM_TRANSFER_TYPES
} TransferType;

typedef struct {
    unsigned long transferCounts[STATS_WINDOWS];
    unsigned long totals;
} Statistics;

typedef struct {
    unsigned int ip;
    unsigned short port;
    unsigned short localPort;
    int fd;
    unsigned short flags;
    unsigned short numOutstandingPieces;
    struct {
        MessageType type;
        BlockInfo blockInfo;
    } blocksToSend[SEND_QUEUE_LEN];
    short outstandingPieces[MAX_PEER_PEER_OUTSTANDING_PIECES];
    BitMap requestedBlocksBitmap[MAX_PEER_PEER_OUTSTANDING_PIECES];

    char clientId[CLIENT_ID_LEN];
    char errCount;
    unsigned char sendIndexStart, sendIndexEnd;
    struct {
        int read;
        int len;
        char header[13];
        void* destBuffer;
        char skip;
    } currentReceiveMsg;

    struct {
        int sent;
        int len;
        char type;
        const void* payload;
        union {
            BlockInfo blockInfo;
            struct {
                unsigned char subType;
                char extensionPayload[7];
            };
        };
    } currentSendMsg;
    void* scratchBuffer;

    long long extensionBytes;
    union {
        char extensions[MAX_NUM_EXTENSION];
        char handshakeBuffer[20];
    };
    unsigned lastReceiveMessageTimeMS;
    unsigned lastSentMessageTimeMS;
    unsigned firstRequestTimeMS;

#ifndef NDEBUG
    unsigned int fastPieceSetOutgoing[16];
#endif

    unsigned int fastPieceSet[16];
    unsigned int suggestedPiece;

    BitMap bitmap;
    Statistics stats[NUM_TRANSFER_TYPES];
    unsigned char statBucket;
} Peer;

typedef enum {
  STARTED, IN_PROGRESS, STOPPED, COMPLETED, NUM_ANNOUNCE_STATUS
} AnnounceStatus;

typedef struct {
    char trackerId[64];
    union {
        struct { long uploaded, downloaded; };
        long transfers[2];
    };
    unsigned long left;
    unsigned long announceInterval;
    unsigned long announceMinInterval;
    unsigned long lastAnnounceTimeMS;
    AnnounceStatus nextStatus;
    int fd;
    int sending;
    union {
        unsigned int sent;
        unsigned int read;
    };
    char buffer[1024];
    int announceUrlIndex;
} TorrentTrackerInfo;

typedef struct {
    const char* data;
    const char* end;
    // number of bytes in each piece
    unsigned long pieceLength;

    int numPieces;
    // string consisting of the concatenation of all 20-byte SHA1 hash values, one per piece
    const char* pieces;
    // the file or parent directory name; advisory
    const char* name;
    int nameLen;
    const char* pathList;

    unsigned long totalLength;

    // These aren't null terminated
    char infoHash[HASH_SIZE];
    char encodedInfoHash[HASH_SIZE * 3];

    const char* announceUrlStr;
    const char* announceUrlList;

    int fileCount;
} TorrentMetadata;

typedef struct {
    unsigned int pieceIndex;
    unsigned int oldPieceIndex;
    unsigned long requestTime;
    BitMap blockBitmap;
    char claimCount;
    char wasShared : 1;
    char invalidDownload : 1;
    char *data;
} PieceMetadata;

typedef struct TorrentSettings {
    const char** paths;
    char* bitmapFilename;
    long cacheSize;
    const char* cacheFilename;
    const char* peerList;
    const char* childCmd;
    char doNotCreateBitmapFiles : 1;
    char forkToBackground : 1;
    char quick : 1;
    char daemon : 1;
    char disableRandomization : 1;
    int dirfd;
    short minPort;
    short maxPort;
    unsigned maxUploadRate;
    unsigned int keepAlive;
    int extraPieces[2];
    union {
        unsigned int blockSize;
        unsigned int pieceSize;
    };
    long long extensions;
} TorrentSettings;

typedef struct TorrentInfo {
    const TorrentMetadata* metadata;
    TorrentTrackerInfo trackerInfo;
    unsigned short port;
    int listenFd;
    int outputFd;
    unsigned char numPeers;
    Peer peers[MAX_NUM_PEERS];

    char clientId[CLIENT_ID_LEN];
    unsigned int cacheSlots;

    unsigned int startingPieceIndex;
    unsigned int endingPieceIndex;
    unsigned int nextPieceToOutput;
    unsigned int nextPieceToOutputOffset;
    unsigned int firstPieceToOutput;
    unsigned int offsetIntoStartingPiece;
    unsigned int lastPieceToOutput;
    unsigned int lenOfLastPieceToOutput;

    Statistics outputStats;
    unsigned char statBucket;

    TorrentSettings settings;
    unsigned char permFlags;
#ifndef NDEBUG
    char pedantic;
#endif

    BitMap downloadedBitmap;
    BitMap requested;
    BitMap ondisk;
    BitMap cacheBitmap;
    PieceMetadata* pieceMetadata;
    const char* extensionHandshakeMsg;
    void* data;
} TorrentInfo;

int isPieceValid(const TorrentMetadata* torrentMetadata, int pieceIndex, const void* data, int len);
/**
 * Returns the length of the specified piece. All pieces except for maybe the last have the same length
 */
static int inline getPieceLen(const TorrentMetadata* metadata, int pieceIndex) {
    assert(metadata->numPieces > pieceIndex);
    if(metadata->numPieces == pieceIndex + 1) {
        int sizeOfLastPiece = metadata->totalLength == metadata->pieceLength ? metadata->totalLength : metadata->totalLength % metadata->pieceLength;
        if(sizeOfLastPiece == 0)
            sizeOfLastPiece = metadata->pieceLength;
        return sizeOfLastPiece;
    } else {
        return metadata->pieceLength;
    }
}

// torrent_file.c
/**
 * Reads and parses the file denoted by fd and populates torrentMetadata
 */
int readAndParseTorrentFile(TorrentMetadata* torrentMetadata, int fd);
/**
 * Opens the file denoted by name and calls readAndParseTorrentFile
 */
int openAndParseTorrentFile(TorrentMetadata * torrentMetadata, const char* name);
/*
 * This method is used to various infomation about a specified. If fileIndex is
 * not -1, the info for the file at that index is returned. Otherwise the file
 * matching targetPath is returned. targetPath can only be non-null if
 * fileIndex is -1.
 * Most information is stored in the corresponding pointer if it is non-null.
 * The file name is returned.
 */
char* getPieceRange(const TorrentMetadata* torrentMetadata, int fileIndex, const char*targetPath, int* startingPieceIndex, int*endingPieceIndex, unsigned long*length, unsigned long* runningLength) ;

const char* getAnnounceUrl(const TorrentMetadata * torrentMetadata, int n, int* slen);
void announceSucceeded(const TorrentMetadata * torrentMetadata, int * n, int success);


void dumpTorrentInfo(const TorrentMetadata* torrentMetadata);

/**
 * Creates a torrent file with the specified parameters and writes it to fd
 */
int createTorrentFile(int fd,  const char* announceUrl, const char* name, const char** files, unsigned int numFiles, unsigned int pieceSize);


// connect.c
int addPeer(TorrentInfo* torrentInfo, unsigned int ip, unsigned short port, int fd);
void removePeer(TorrentInfo* torrentInfo, int index);

int acceptPeerConnection(TorrentInfo* torrentInfo, int socketFd);
int addPeersFromPeerList(TorrentInfo* torrentInfo);
int listenOnPort(int port);
int connectToTracker(TorrentInfo* torrentFileInfo);
void announceToTrackerQuick(TorrentInfo* torrentInfo, AnnounceStatus announceStatus, int quick);
int processAnnounceResponse(TorrentInfo* torrentInfo, const char*buffer, int ret, int addPeers);
int listenForClients(TorrentInfo* torrentFileInfo);
void prepareMessageToSendToTracker(TorrentInfo* torrentInfo, AnnounceStatus announceStatus);


// stats.c
void dumpStats(const TorrentInfo* torrentInfo);
void dumpPeers(const TorrentInfo* torrentInfo);
unsigned long getPeerTransferRate(const Peer* peer, TransferType type);
void incrementTransferCounts(Statistics * stats, int bucket, int numBytes);
const char* getFlagString(unsigned int flags);
unsigned long getTransferRate(const Statistics * stats, int bucket);

void moveToNextStatsWindow(TorrentInfo* torrentInfo);

// disk.c
void allocateBuffersAndLoadBitmaps(TorrentInfo* torrentInfo) ;
void freeBuffers(TorrentInfo* torrentInfo) ;
int loadBitmapFromDisk(int dirfd, const TorrentMetadata* torrentMetadata, BitMap* bitmap) ;
int readFromDisk(int dirfd, const TorrentMetadata* metadata, char*buffer, int pieceIndex, int pieceOffset, int len);
int writeToDisk(int dirfd, const TorrentMetadata* metadata, char*buffer, int pieceIndex, int pieceOffset, int len);

int safeRead(int fd, void* buffer, int buffer_len);
int safeWrite(int fd, void* buffer, int buffer_len);

/*
 * Sync the bitmap and data to disk.
 * if block is true, then this function will block until everything is persisted.
 * Otherwise the data and bitmap will be synced asynchronously
 */
void syncChanges(TorrentInfo* torrentInfo, int block);

// extensions.c
const char* allocExtensionMessage();
void receiveExtensionHandshake(const char* start, int len, Peer* peer);
int translateExtensionToPeerFormat(const Peer* peer, MessageType ext);
int doesPeerSupportExtension(const Peer* peer, MessageType ext);
int isExtensionProtocalMessage(MessageType type);

#endif
